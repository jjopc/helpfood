package helpfood.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "listas_usuarios")
@IdClass(CompositeKey_ListasDeRecetasDeUnUsuario.class)
public class ListasDeRecetasDeUnUsuarioEntity {
	
	@Id
	String titulo;
	@Id
	String nick;
	
	public ListasDeRecetasDeUnUsuarioEntity() {}
	
	public ListasDeRecetasDeUnUsuarioEntity(String titulo, String nick) {
		this.titulo = titulo;
		this.nick = nick;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
    
}