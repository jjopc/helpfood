package helpfood.response;

public class AlergenosIngredientesResponse {
	
	private int id_ingrediente;
	private int id_alergeno;
	
	public AlergenosIngredientesResponse() {}
	

	public int getIdIngrediente() {
		return this.id_ingrediente;
	}

	public void setIdIngrediente(int id) {
		this.id_ingrediente = id;
	}
	
	//@JsonIgnore //con esto hago que este campo no vaya en el json resultante, porque no lo necesitamos
	//@JsonProperty(value = "user_password")
	public int getIdAlergeno() {
		return this.id_alergeno;
	}

	public void setIdAlergeno(int id) {
		this.id_alergeno = id;
	}
	
	
}