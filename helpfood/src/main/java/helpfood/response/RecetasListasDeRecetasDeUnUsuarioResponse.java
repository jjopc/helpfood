package helpfood.response;

public class RecetasListasDeRecetasDeUnUsuarioResponse {
    
    private int idReceta;
    private String tituloLista;
    private String nick;

    public RecetasListasDeRecetasDeUnUsuarioResponse() {
    }

    public RecetasListasDeRecetasDeUnUsuarioResponse(int idReceta, String tituloLista, String nick) {
        this.idReceta = idReceta;
        this.tituloLista = tituloLista;
        this.nick = nick;
    }

    public int getIdReceta() {
        return idReceta;
    }

    public void setIdReceta(int idReceta) {
        this.idReceta = idReceta;
    }

    public String getTituloLista() {
        return tituloLista;
    }

    public void setTituloLista(String tituloLista) {
        this.tituloLista = tituloLista;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    
}