package helpfood.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import helpfood.request.NewComentarioRequest;
import helpfood.response.ComentarioResponse;
import helpfood.response.ListaComentariosResponse;
import helpfood.service.ComentarioService;

@RestController
public class ComentariosRestController {
	@Autowired
	private ComentarioService service;
	
	@RequestMapping(path = "/comentarios/receta/{id_receta}", method = RequestMethod.GET)
	public ResponseEntity<ListaComentariosResponse> getComentariosFromReceta(@PathVariable int id_receta){
		return ResponseEntity.ok(service.getComentariosFromReceta(id_receta));
	}
	@RequestMapping(path = "/comentarios/usuario/{nick}", method = RequestMethod.GET)
	public ResponseEntity<ListaComentariosResponse> getComentariosFromUsuario(@PathVariable String nick){
		return ResponseEntity.ok(service.getComentariosFromUsuario(nick));
	}
	
	@RequestMapping(path = "/comentarios", method = RequestMethod.POST)
	public ResponseEntity<ComentarioResponse> createNewComentario(@Valid @RequestBody NewComentarioRequest comentarioRequest) {
		return ResponseEntity.ok(service.createComentario(comentarioRequest));
	}
	
	@RequestMapping(path = "/comentarios/delete/{nick}", method = RequestMethod.DELETE)
	public Optional<ComentarioResponse> borrarValoraciones(@PathVariable String nick) {
		Optional<ComentarioResponse> response = service.borrarValoraciones(nick);
		return response;
	}
	
	@RequestMapping(path = "/comentarios/delete/receta/{idreceta}", method = RequestMethod.DELETE)
	public Optional<ComentarioResponse> borrarValoracionesReceta(@PathVariable int idreceta) {
		Optional<ComentarioResponse> response = service.borrarValoracionesReceta(idreceta);
		return response;
	}
}