package helpfood.service;

import java.util.Optional;

import helpfood.request.NewComentarioRequest;
import helpfood.response.ComentarioResponse;
import helpfood.response.ListaComentariosResponse;

public interface ComentarioService {
	public ListaComentariosResponse getComentariosFromReceta(int id_receta);
	public ListaComentariosResponse getComentariosFromUsuario(String nick);
	public ComentarioResponse createComentario(NewComentarioRequest comentarioRequest);
	public Optional<ComentarioResponse> borrarValoraciones(String nick);
	public Optional<ComentarioResponse> borrarValoracionesReceta(int idreceta);
}