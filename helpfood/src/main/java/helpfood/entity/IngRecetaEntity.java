package helpfood.entity;

import javax.persistence.*;

@Entity
@IdClass(CompositeKey.class)
@Table(name = "ingredientes_recetas")
public class IngRecetaEntity {
	
	@Id
	@Column(name="id_receta")
	private int idreceta;
	
	@Id
	@Column(name="id_ingrediente")
	private int id_ingrediente;
	
	@Column(name="cantidad")
	private int cantidad;
	
	public IngRecetaEntity() {};
	
	public IngRecetaEntity(int idreceta, int id_ingrediente, int cantidad) {
		this.idreceta = idreceta;
		this.id_ingrediente = id_ingrediente;
		this.cantidad = cantidad;
	}

	public int getIdReceta() {
		return this.idreceta;
	}
	
	public void setIdReceta(int id) {
		this.idreceta = id;
	}
	
	public int getIdIngrediente() {
		return this.id_ingrediente;
	}
	
	public void setIdIngrediente(int id) {
		this.id_ingrediente = id;
	}
	
	public int getCantidad() {
		return this.cantidad;
	}
	
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
}