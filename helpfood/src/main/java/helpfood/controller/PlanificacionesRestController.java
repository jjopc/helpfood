package helpfood.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import helpfood.entity.PlanificacionEntity;
import helpfood.request.NewPlanificacionRequest;
import helpfood.response.ListaPlanificacionesResponse;
import helpfood.response.PlanificacionResponse;
import helpfood.service.PlanificacionService;

@RestController
//@RequestMapping(path = "/planificaciones", method = RequestMethod.GET)
public class PlanificacionesRestController {
	
	@Autowired
	private PlanificacionService service;
	
	
	@RequestMapping(path = "/planificaciones",  method = RequestMethod.GET)
	public ResponseEntity<ListaPlanificacionesResponse> getAllPlanificaciones() {
		return ResponseEntity.ok(service.getAllPlanificaciones());
	}
	
	@RequestMapping(path = "planificaciones/{nick}/{id_receta}/{fecha_hora}", method = RequestMethod.GET)
	public ResponseEntity<Optional<PlanificacionEntity>> getPlanificacion(@PathVariable String nick, @PathVariable int id_receta,
			@PathVariable String fecha_hora){
		//Date date = Date.valueOf(fecha_hora);
		return ResponseEntity.ok(service.findPlanificacion(nick, id_receta, fecha_hora));
	}
	
	@RequestMapping(path = "/planificaciones", method = RequestMethod.POST)
	@PostMapping("/date")
	public ResponseEntity<PlanificacionResponse> createNewPlanificacion(@Valid @RequestBody NewPlanificacionRequest planificacionRequest) {
		
		PlanificacionResponse response = service.createPlanificacion(planificacionRequest);
		
		String url = "/planificaciones/{nick}/{id_receta}/{fecha_hora}";
		
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
		 
		Map<String, String> urlParams = new HashMap<>();
		urlParams.put("nick", planificacionRequest.getnick());
		urlParams.put("id_receta", Integer.toString(planificacionRequest.getid_receta()));
		urlParams.put("fecha_hora", planificacionRequest.getfecha_hora());
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
		URI miURI = builder.buildAndExpand(urlParams).toUri();
		return ResponseEntity.created(miURI).body(response);
		
	}
	
	/*
	@PostMapping
	public ResponseEntity<PersonResource> post(@RequestBody final Person personFromRequest) {
	  final Person person = new Person(personFromRequest);
	  personRepository.save(person);
	  final URI uri =
	      MvcUriComponentsBuilder.fromController(getClass())
	          .path("/{id}")
	          .buildAndExpand(person.getId())
	          .toUri();
	  return ResponseEntity.created(uri).body(new PersonResource(person));
	}
	*/
	
	@RequestMapping(path = "/planificaciones/delete/{nick}/{id_receta}/{fecha_hora}", method = RequestMethod.DELETE)
	public Optional<PlanificacionResponse> borrarPlanificacion(@PathVariable String nick, @PathVariable int id_receta, @PathVariable String fecha_hora) {
		Optional<PlanificacionResponse> response = service.borrarPlanificacion(nick, id_receta, fecha_hora);
		return response;
	}

	@RequestMapping(path = "/planificaciones/delete/{nick}/all", method = RequestMethod.DELETE)
	public Optional<PlanificacionResponse> borrarPlanificacion(@PathVariable String nick) {
		Optional<PlanificacionResponse> response = service.borrarTodasPlanificaciones(nick);
		return response;
	}
	
	@RequestMapping(path = "/planificaciones/delete/{idreceta}", method = RequestMethod.DELETE)
	public Optional<PlanificacionResponse> borrarPlanificacionesReceta(@PathVariable int idreceta) {
		Optional<PlanificacionResponse> response = service.borrarPlanificacionesReceta(idreceta);
		return response;
	}

} 




