package helpfood.response;

public class ListasDeRecetasDeUnUsuarioResponse {

	String nick;
	String titulo;
	
	public ListasDeRecetasDeUnUsuarioResponse() {}
	
	public ListasDeRecetasDeUnUsuarioResponse(String nick, String titulo) {
		this.nick = nick;
		this.titulo = titulo;
	}
	
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
}