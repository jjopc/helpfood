package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.ListasDeRecetasDeUnUsuarioEntityToResponseConverter;
import helpfood.converter.NewListasDeRecetasDeUnUsuarioRequestToListasDeRecetasDeUnUsuarioEntityConverter;
import helpfood.entity.ListasDeRecetasDeUnUsuarioEntity;
import helpfood.repository.ListasDeRecetasDeUnUsuarioRepository;
import helpfood.request.NewListasDeRecetasDeUnUsuarioRequest;
import helpfood.response.Lista_listasDeRecetasDeUnUsuarioResponse;
import helpfood.response.ListasDeRecetasDeUnUsuarioResponse;

@Service
public class ListasDeRecetasDeUnUsuarioServiceImpl implements ListasDeRecetasDeUnUsuarioService {
	
	@Autowired
	ListasDeRecetasDeUnUsuarioRepository repository;
	
	ListasDeRecetasDeUnUsuarioEntityToResponseConverter entityToResponseConverter = new ListasDeRecetasDeUnUsuarioEntityToResponseConverter();
	NewListasDeRecetasDeUnUsuarioRequestToListasDeRecetasDeUnUsuarioEntityConverter requestToEntityConverter = new NewListasDeRecetasDeUnUsuarioRequestToListasDeRecetasDeUnUsuarioEntityConverter();
	
    public Optional<ListasDeRecetasDeUnUsuarioResponse> getListaFromUsuario(String nick, String titulo) {
        
    	Optional<ListasDeRecetasDeUnUsuarioResponse> res;
    	Optional<ListasDeRecetasDeUnUsuarioEntity> entity = repository.findByNickAndTitulo(nick, titulo);
    	res = entity.map(ListasUsuariosEntity -> entityToResponseConverter.convert(ListasUsuariosEntity));
        return res;
    }

    public Lista_listasDeRecetasDeUnUsuarioResponse getAllListasUsuariosFromUsuario(String nick) {
        
    	Lista_listasDeRecetasDeUnUsuarioResponse res = new Lista_listasDeRecetasDeUnUsuarioResponse();
		List<ListasDeRecetasDeUnUsuarioEntity> entities = repository.findBynick(nick);
		List<ListasDeRecetasDeUnUsuarioResponse> responses = entities.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		res.setListaListasUsuario(responses);
    	
        return res;
    }
    
    public ListasDeRecetasDeUnUsuarioResponse createListasDeRecetasDeUnUsuario(NewListasDeRecetasDeUnUsuarioRequest listaRequest) {
    	
    	ListasDeRecetasDeUnUsuarioResponse response;
		Optional<ListasDeRecetasDeUnUsuarioEntity> oldEntity = repository.findByNickAndTitulo(listaRequest.getNick(), listaRequest.getTitulo());
		
		if (oldEntity.isPresent()) {
			response = entityToResponseConverter.convert(oldEntity.get());
		} else {
			
			ListasDeRecetasDeUnUsuarioEntity newEntity = requestToEntityConverter.convert(listaRequest);
			newEntity = repository.save(newEntity);
			response = entityToResponseConverter.convert(newEntity);
		}
		return response;
    }
    
	public Optional<ListasDeRecetasDeUnUsuarioResponse> borrarListasDeRecetasDeUnUsuario(String nick, String titulo){
		Optional<ListasDeRecetasDeUnUsuarioResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		repository.deleteListasDeRecetasDeUnUsuario(nick, titulo);
		return response;
	}

	@Override
	public Optional<ListasDeRecetasDeUnUsuarioResponse> borrarListas(String nick) {
		Optional<ListasDeRecetasDeUnUsuarioResponse> response = null;
		repository.deleteListas(nick);
		return response;
	}
}