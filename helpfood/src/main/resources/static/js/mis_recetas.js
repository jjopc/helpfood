 $(document).ready(function(){
	getRecetas(getCookie("username")); //asi pide las recetas por el nick del usuario logueado en este momento
	
	var idABorrar;

	$(document).on("click", ".borrar", function () {
		idABorrar = $(this).data('id');
		//$(".modal-body #bookId").val( myBookId );
		// As pointed out in comments, 
		// it is unnecessary to have to manually call the modal.
		// $('#addBookDialog').modal('show');
	});

	$("#borrarMiReceta").click(function() {
		//agregarEvento(idReceta);
		//prueba(idReceta);
		//$('#agregarEvento').modal('hide');
		//var inputVal = document.getElementById("myInput").value;
		//var inputVal = document.getElementById("queTeBorres").value;
		//console.log(idABorrar);
		borraRecetaConcreta(idABorrar);
		$('#borraReceta').modal('hide');
		window.location.assign("/mis_recetas.html");
	});
}); 

/*
 function getRecetasORIGINAL(nick) {
 	$.ajax({
         url: "/recetas/nick/" + nick,
         type: "GET",
         success: function (data) {
        	 //TODO Con este for, se verían bien los estilos, pero no funciona el botón de eliminar
        	/*for (var i = 0; i < data.listaRecetas.length; i++) {
         		$("#contenedor_recetas").append('<a href="receta_concreta.html?id=' + data.listaRecetas[i].id + '" <div class="card">'
 				+ ' <img class="card-img-top" src="' + "img/ejemplo.jpg" 
 				+ '"><div class="card-body" style="color:black"><h5 class="card-title">' + data.listaRecetas[i].titulo 
 				+ '</h5><p class="card-text"><i class="fas fa-clock"></i> Tiempo:  <label>' + data.listaRecetas[i].tiempoPreparacion + " '"
 				+ '</label><i class="fas fa-exclamation ml-3"></i> Dificultad: <label>' + data.listaRecetas[i].dificultad + '</label>'
 				+ '<button style="background-color: Transparent" type="button" data-toggle="modal" data-target="#borraReceta"><i class="fa fa-trash ml-3"></i></button></p></div></div></a>');
         	}
         	for (var i = 0; i < data.listaRecetas.length; i++) {         		
        		$("#contenedor_recetas").append('<a href="receta_concreta.html?id=' + data.listaRecetas[i].id + '"><div class="card">' 
        		+ '<img class="card-img-top" src="' + "img/ejemplo.jpg" 
				+ '"><div class="card-body style="color:black""><h5 class="card-title">' + data.listaRecetas[i].titulo 
				+ '</h5><p class="card-text"><i class="fas fa-clock"></i> Tiempo:  <label>' + data.listaRecetas[i].tiempoPreparacion  + " '" + 
				'</label><i class="fas fa-exclamation ml-3"></i> Dificultad: <label>' + data.listaRecetas[i].dificultad + '</label><a type="button" data-toggle="modal" data-target="#borraReceta"><i  class="fa fa-trash ml-3"></i><a></p></div></div></a>');
         	}
         }
 	});
 }
 */

  function getRecetas(nick) {
 	$.ajax({
         url: "/recetas/nick/"+nick,
         type: "GET",
         success: function (data) {        	 
         	for (var i = 0; i < data.listaRecetas.length; i++) {         		
        		$("#contenedor_recetas").append('<a href="receta_concreta.html?id=' + data.listaRecetas[i].id + '" style="apearance:none;" class=""><div class="card m-2" id="unaReceta"><img class="card-img-top" src="../img/img_recetas/' +  data.listaRecetas[i].id + '.jpg"'
        				+ '"><div class="card-body"><h4 class="card-title">' + data.listaRecetas[i].titulo 
        				+ '</h4><p class="card-text"><i class="fas fa-clock"></i> Tiempo:  <label>' + data.listaRecetas[i].tiempoPreparacion  + " min" + 
        				'</label><i class="fas fa-exclamation ml-3"></i> Dificultad: <label>' + data.listaRecetas[i].dificultad + '</label><a type="button" class="borrar" data-toggle="modal" data-id="' + data.listaRecetas[i].id + '" value=' + data.listaRecetas[i].id + ' data-target="#borraReceta"><i  class="fa fa-trash ml-3"></i><a></p></div></div></a>');
         	}
         }
 	});
 }

 function borraRecetaConcreta(idreceta){

	//falta borrar lo relativo a las listas, que todavía no están acabadas
	borrarValoraciones(idreceta);
	borrarPlanificaciones(idreceta);
	borrarIngRecetas(idreceta);
	borrarAparicionesEnListas(idreceta);
	
	//Ya borrado todo lo relativo a esta receta concreta, se borrará la receta en sí
	var miUrl = "/recetas/delete/" + idreceta;

	$.ajax({
		async: false,
		url : miUrl,
		type : "DELETE",
		success : function() {
			
			alert("Receta borrada con éxito");
		}
	});
 }

  function borrarValoraciones(idreceta){

	
	var miUrl = "/comentarios/delete/receta/" + idreceta;

	$.ajax({
		async: false,
		url : miUrl,
		type : "DELETE",
		success : function() {
			
			console.log("ok valoraciones");
		}
	});
	
}

 function borrarPlanificaciones(idreceta){

	
	var miUrl = "/planificaciones/delete/" + idreceta;

	$.ajax({
		async: false,
		url : miUrl,
		type : "DELETE",
		success : function() {
			
			console.log("ok planificaciones");
		}
	});
	
}

 function borrarIngRecetas(idreceta){

	
	var miUrl = "/ingrecetas/delete/" + idreceta;

	$.ajax({
		async: false,
		url : miUrl,
		type : "DELETE",
		success : function() {
			
			//console.log("ok ingrecetas");
		}
	});
	
}
 
function borrarAparicionesEnListas(idreceta){
	$.ajax({
		async: false,
		url : "/listas_recetas/delete/apariciones/" + idreceta,
		type : "DELETE",
		success : function() {
			console.log("ok borrar apariciones en listas");
		},
        error: function (data, errorThrown) {
            alert('Algo ha ido mal intentando borrar apariciones en las listas');
        }
	});
}

 function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}