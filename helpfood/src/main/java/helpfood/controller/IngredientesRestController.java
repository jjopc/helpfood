package helpfood.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import helpfood.entity.IngredienteEntity;
import helpfood.response.ListaIngredientesResponse;
import helpfood.service.IngredienteService;

@RestController
@RequestMapping(path = "/ingredientes", method = RequestMethod.GET)
public class IngredientesRestController {
	@Autowired
	private IngredienteService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ListaIngredientesResponse> getAllIngredientes() {
		return ResponseEntity.ok(service.getAllIngredientes());
	}
	
	@RequestMapping(path = "/{id_ingrediente}", method = RequestMethod.GET)
	public ResponseEntity<Optional<IngredienteEntity>> getIngrediente(@PathVariable int id_ingrediente) {
		return ResponseEntity.ok(service.findById(id_ingrediente));
	}
	
}