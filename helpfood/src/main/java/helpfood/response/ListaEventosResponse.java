package helpfood.response;

import java.util.List;

public class ListaEventosResponse {
	private List<EventoResponse> listaEventos;

	public ListaEventosResponse(List<EventoResponse> listaEventos) {
		this.listaEventos = listaEventos;
	}

	public ListaEventosResponse() {
	}

	public List<EventoResponse> getListaEventos() {
		return listaEventos;
	}

	public void setListaEventos(List<EventoResponse> listaEventos) {
		this.listaEventos = listaEventos; 
	}
}