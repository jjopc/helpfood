package helpfood.response;

import java.util.List;

public class ListaUsuariosResponse {
	private List<UsuarioResponse> listaUsuarios;

	public ListaUsuariosResponse(List<UsuarioResponse> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public ListaUsuariosResponse() {
	}

	public List<UsuarioResponse> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuarioResponse> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
}