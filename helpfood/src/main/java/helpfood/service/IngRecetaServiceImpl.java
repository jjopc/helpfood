package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.IngRecetaEntityToIngRecetaResponseConverter;
import helpfood.entity.IngRecetaEntity;
import helpfood.repository.IngRecetaRepository;
import helpfood.request.NewIngRecetaRequest;
import helpfood.response.IngRecetaResponse;
import helpfood.response.ListaIngRecetasResponse;

@Service
public class IngRecetaServiceImpl implements IngRecetaService {
	
	private IngRecetaEntityToIngRecetaResponseConverter entityToResponseConverter = new IngRecetaEntityToIngRecetaResponseConverter();
	//private NewIngRecetaRequestToIngRecetaEntityConverter requestToEntityConverter = new NewIngRecetaRequestToIngRecetaEntityConverter();
	
	@Autowired
	private IngRecetaRepository ingRecetaRepository;

	@Override
	public ListaIngRecetasResponse getAllIngRecetas() {
		ListaIngRecetasResponse response = new ListaIngRecetasResponse();
		List<IngRecetaEntity> allIngRecetas = ingRecetaRepository.findAll();
		List<IngRecetaResponse> allResponses = allIngRecetas.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaIngRecetas(allResponses);
		return response;
	}
	
	
	@Override
	public ListaIngRecetasResponse findByIdRecetaAndIdIngrediente(int idreceta, int idingrediente) {
		ListaIngRecetasResponse response = new ListaIngRecetasResponse();
		List<IngRecetaEntity> ingReceta = ingRecetaRepository.findByIdRecetaAndIdIngrediente(idreceta, idingrediente);
		List<IngRecetaResponse> allResponses = ingReceta.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaIngRecetas(allResponses);
		return response;
	}
	
	@Override
	public ListaIngRecetasResponse findByIdReceta(int idreceta) {
		ListaIngRecetasResponse response = new ListaIngRecetasResponse();
		List<IngRecetaEntity> ingReceta = ingRecetaRepository.findByIdReceta(idreceta);
		List<IngRecetaResponse> allResponses = ingReceta.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaIngRecetas(allResponses);
		return response;
	}
	
	@Override
	public IngRecetaResponse createIngReceta(NewIngRecetaRequest ingRecetaRequest) {
		IngRecetaResponse response = null;
		//Optional<RecetaEntity> oldEntity = ingRecetaRepository.findByIdReceta(ingRecetaRequest.getId());
		List<IngRecetaEntity> oldEntity = ingRecetaRepository.findByIdReceta(ingRecetaRequest.getId());
		
		//if (oldEntity.isPresent()) {
		//	response = entityToResponseConverter.convert(oldEntity.get());
		//} else {
			/*IngRecetaEntity newEntity = requestToEntityConverter.convert(ingRecetaRequest);
			//IngRecetaEntity newEntity2 = requestToEntity2Converter.convert(recetaRequest);
			
			newEntity = ingRecetaRepository.save(newEntity);
			//newEntity2 = ingRecetaRepository.save(newEntity2);
			
			response = entityToResponseConverter.convert(newEntity);*/
		//}
		return response;
	}
	
	//@Override
	public Optional<IngRecetaResponse> borrarIngReceta(int idreceta){
		Optional<IngRecetaResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		ingRecetaRepository.deleteIngReceta(idreceta);
		return response;
	}
}