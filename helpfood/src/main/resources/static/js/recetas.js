$(document).ready(function(){
	
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	inicializacion();

	$("#select").change(function(){
		$(this).attr("selected", true);
		inicializacion();
		var valA = $(this).val();
		for (var i = 0; i < valA.length; i++) {
			if(valA[i] == "1") $("#i_creator").show();
			if(valA[i] == "2") $("#i_time").show();
			if(valA[i] == "3") $("#i_culture").show();
			if(valA[i] == "4"){
				$("#i_caloriesMin").show();
				$("#i_caloriesMax").show();
			}
			if(valA[i] == "5") $("#i_difficulty").show();
		}
	});

	$("#buscar").click(function() {
		var quiereExcluirAlergenos = document.getElementById("select")[6].selected;

		//Si cualquiera de los valores al final de su procesamiento vale null
		//querrá decir que está invalidado y por tanto no se tiene en cuenta para la búsqueda (será "cualquiera"),
		//o bien porque el usuario no lo ha añadido a su búsqueda avanzada, 
		//o bien porque el valor no era correcto para usarlo en la búsqueda (por descuido, o por trolleo del usuario, o porque no ha metido nada)

		var titulo = null;
		//Si el campo del titulo tenía contenido útil 
		//(no contenia una sucesión de espacios ni era string vacío), titulo dejará de valer null
		titulo = document.getElementById('i_title').value;
		titulo = titulo.trim(); //quito espacios iniciales y/o finales
		
		if (titulo.replace(/\s/g, '').length) { //titulo no tenia un contenido todo en blanco (espacios, tabulacion o saltos de linea) ni era vacío
			//console.log("titulo OK: " + titulo);
		}
		else titulo = null;
		

		var creador = null;
		//Si el campo del creador estaba visible al darle a buscar y además tenía contenido útil 
		//(no contenia una sucesión de espacios ni era string vacío), creador dejará de valer null
		if(document.getElementById("i_creator").style.display == "none" == false){

			creador = document.getElementById('i_creator').value;
			creador = creador.trim(); //quito espacios iniciales y/o finales
			
			if (creador.replace(/\s/g, '').length) { //creador no tenia un contenido todo en blanco (espacios, tabulacion o saltos de linea) ni era vacío
				//console.log("creador OK: " + creador);
			}
			else creador=null;
		}

		var tiempoMinimo = null;
		var tiempoMaximo = null;
		const variacionTiempoAprox = 10; 
		//Si el usuario especifica un tiempo aproximado de X minutos, 
		//se buscarán recetas cuyo tiempo de preparacion valga [X-variacionTiempoAprox, X+variacionTiempoAprox], sin permitir en ningún caso valores negativos en el intervalo
		//Si el campo del tiempo estaba visible al darle a buscar y además tenía contenido útil 
		//(no lo ha dejado vacío ni ha metido a mano algo negativo), tiempoMinimo y tiempoMaximo dejarán de valer null
		if(document.getElementById("i_time").style.display == "none" == false){

			var tiempo = document.getElementById('i_time').value;
			
			if(tiempo != "" && tiempo>=0){
				tiempo = parseInt(tiempo, 10); //por si el muy troll quiere meter numeros reales con comas o puntos, sólo cogerá la parte entera de esos
				//console.log("tiempo OK: " + tiempo);
				tiempoMinimo = tiempo-variacionTiempoAprox;
				if(tiempoMinimo < 0) tiempoMinimo = 0;
				tiempoMaximo = tiempo+variacionTiempoAprox;
				
				//console.log("tiempoMinimo OK: " + tiempoMinimo);
				//console.log("tiempoMaximo OK: " + tiempoMaximo);
			} 
		}

		
		var cultura = null;
		//Si el campo de cultura estaba visible al darle a buscar y además tenía 
		//como valor algo distinto de "Cualquiera" o "", cultura dejará de valer null
		if(document.getElementById("i_culture").style.display == "none" == false && 
			document.getElementById('i_culture').value != "Cualquiera" &&
			document.getElementById('i_culture').value != ""){

			cultura = document.getElementById('i_culture').value;
			//console.log("cultura OK: " + cultura);
		}

		
		var minCal = null;
		var maxCal = null;
		//Si los campos de mínimo y máximo estaban visibles al darle a buscar y además tenían contenido útil 
		//(al menos uno tenía un entero positivo), y también si minCal<=maxCal,
		//entonces minCal y/o maxCal dejarán de ser null
		if(document.getElementById("i_caloriesMin").style.display == "none" == false){

			var minCal = document.getElementById('i_caloriesMin').value;
			var maxCal = document.getElementById('i_caloriesMax').value;
			
			if(minCal != "" && minCal>=0) minCal = parseInt(minCal, 10); //por si el muy troll quiere meter numeros reales con comas o puntos, sólo cogerá la parte entera de esos
			else minCal = null;

			if(maxCal != "" && maxCal>=0) maxCal = parseInt(maxCal, 10); 
			else maxCal = null;

			if(minCal != null && maxCal != null && minCal>maxCal){
				minCal = null;
				maxCal = null;
			}

			//if(minCal != null) console.log("minCal OK: " + minCal);
			//if(maxCal != null) console.log("maxCal OK: " + maxCal);
		}

		var dificultad = null;//document.getElementById('i_difficulty').value;
		//Si el campo de dificultad estaba visible al darle a buscar y además tenía 
		//como valor algo distinto de "Cualquiera" o "", dificultad dejará de valer null
		if(document.getElementById("i_difficulty").style.display == "none" == false && 
			document.getElementById('i_difficulty').value != "Cualquiera" &&
			document.getElementById('i_difficulty').value != ""){

			dificultad = document.getElementById('i_difficulty').value;
		}
			$("#info_filtros_usados").remove();
		if(titulo == null && creador == null && tiempoMinimo == null && tiempoMaximo == null && cultura == null && minCal == null 
			&& maxCal == null && dificultad == null && !quiereExcluirAlergenos)
			$("#divfiltrosusados").append('<p id="info_filtros_usados" style="color:white;">¡Especifica algo primero para tu búsqueda!<br></p>').hide().fadeIn(300);
		else{ //si cualquiera tiene algo distinto a null, es decir, al menos se puede intentar buscar algo

			$("#divfiltrosusados").append('<p id="info_filtros_usados" style="color:white;">Buscando recetas que cumplan a la vez tus requisitos:<br></p>').hide().fadeIn(300);
			if(titulo != null) $("#info_filtros_usados").append('Contienen en el título: "' + titulo + '"<br>').hide().fadeIn(300);
			if(creador != null) $("#info_filtros_usados").append('Creadas por: "' + creador + '"<br>').hide().fadeIn(300);
			if(tiempoMinimo != null || tiempoMaximo != null){
				if(tiempoMinimo != null && tiempoMaximo == null) $("#info_filtros_usados").append('Tiempo de preparación a partir de: ' + tiempoMinimo + ' minutos<br>').hide().fadeIn(300);
				if(tiempoMinimo == null && tiempoMaximo != null) $("#info_filtros_usados").append('Tiempo de preparación máximo: ' + tiempoMaximo + ' minutos<br>').hide().fadeIn(300);
				if(tiempoMinimo != null && tiempoMaximo != null) $("#info_filtros_usados").append('Tiempo de preparación entre: ' + tiempoMinimo + ' y ' + tiempoMaximo + ' minutos<br>').hide().fadeIn(300);
				
				//$("#info_filtros_usados").append('Contiene en el título: "' + titulo + '"<br>').hide().fadeIn(300);
			}
			if(cultura != null) $("#info_filtros_usados").append('Cultura: "' + cultura + '"<br>').hide().fadeIn(300);
			else if(document.getElementById("i_culture").style.display == "none" == false) $("#info_filtros_usados").append('Cultura: "Cualquiera"<br>').hide().fadeIn(300);
			if(minCal != null || maxCal != null){
				if(minCal != null && maxCal == null) $("#info_filtros_usados").append('Con un mínimo de: ' + minCal + ' kcal<br>').hide().fadeIn(300);
				if(minCal == null && maxCal != null) $("#info_filtros_usados").append('Con un máximo de: ' + maxCal + ' kcal<br>').hide().fadeIn(300);
				if(minCal != null && maxCal != null) $("#info_filtros_usados").append('Que tengan entre: ' + minCal + ' y ' + maxCal + ' kcal<br>').hide().fadeIn(300);
			}
			if(dificultad != null) $("#info_filtros_usados").append('Nivel de dificultad: "' + dificultad + '"<br>').hide().fadeIn(300);
			else if(document.getElementById("i_difficulty").style.display == "none" == false) $("#info_filtros_usados").append('Dificultad: "Cualquiera"<br>').hide().fadeIn(300);
			if(quiereExcluirAlergenos) $("#info_filtros_usados").append('Excluyendo recetas con ingredientes a los que tengas alergia o intolerancia.<br>').hide().fadeIn(300);
			
			//$("#info_filtros_usados").append('zeeeeeip').hide().fadeIn(300);
			
			$('[data-id="tarjeta_receta"]').remove();
			//$("#info_filtros_usados").append('holiii');
			getRecetasBusqueda(titulo, creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad, quiereExcluirAlergenos);
		} 

		//$("#info_filtros_usados").append('holiii');
		//$("#info_filtros_usados").remove();
		

	});

	const creadorParam = urlParams.get('creador');
	if(creadorParam == null) getRecetas(); //muestra todas las recetas de la tabla
	else getRecetasCreador(creadorParam);
	
});

function inicializacion(){
	$("#i_creator").hide();
	$("#i_time").hide();
	$("#i_culture").hide();
	$("#i_difficulty").hide();
	$("#i_caloriesMin").hide();
	$("#i_caloriesMax").hide();
}

function getRecetas() {
	$.ajax({
        url: "/recetas",
        type: "GET",
        success: function (data) {
			if(data.listaRecetas.length){
				$("#info_filtros_usados").append('Aquí tienes todas las recetas de HelpFood. ¡Prueba a filtrar algunas!<br>').hide().fadeIn(300);
				$("#info_filtros_usados").append('Se están mostrando un total de ' + data.listaRecetas.length + ' recetas.').hide().fadeIn(300);
			}
			else $("#info_filtros_usados").append('¡Vaya! Parece que no hay ninguna receta en HelpFood todavía.').hide().fadeIn(300);
        	for (var i = 0; i < data.listaRecetas.length; i++) {
        		$("#contenedor_recetas").append('<a href="receta_concreta.html?id=' + data.listaRecetas[i].id + '" <div class="card" data-id="tarjeta_receta">'
				+ ' <img class="card-img-top" src="img/img_recetas/' + data.listaRecetas[i].id + '.jpg"'
				+ '"><div class="card-body" style="color:black"><h5 class="card-title">' + data.listaRecetas[i].titulo 
				+ '</h5><p class="card-text"><i class="fas fa-clock"></i> Tiempo:  <label>' + data.listaRecetas[i].tiempoPreparacion + " min" + 
				'</label><i class="fas fa-exclamation ml-3"></i> Dificultad: <label>' + data.listaRecetas[i].dificultad + '</label></p></div></div></a>').hide().fadeIn(300);
        	}
        }
	});
}

function getRecetasCreador(creador) {
	$.ajax({
        url: "/recetas/nick/" + creador,
        type: "GET",
        success: function (data) {
			if(data.listaRecetas.length){
				$("#info_filtros_usados").append('Aquí tienes todas las recetas creadas por ' + creador + '.<br>').hide().fadeIn(300);
				$("#info_filtros_usados").append('Se han encontrado ' + data.listaRecetas.length + ' recetas.').hide().fadeIn(300);
			}
			else $("#info_filtros_usados").append('¡Vaya! Parece que ' + creador + ' no ha creado todavía ninguna receta.').hide().fadeIn(300);
			//$("#divfiltrosusados").append('<p id="info_filtros_usados" style="color:white;">Se han encontrado ' + data.listaRecetas.length + ' recetas.<br></p>');
			//$("#info_filtros_usados").append('<br>Aquíii tienes todas las recetas creadas por ' + creador + '.');
			for (var i = 0; i < data.listaRecetas.length; i++) {
        		$("#contenedor_recetas").append('<a href="receta_concreta.html?id=' + data.listaRecetas[i].id + '" <div class="card" data-id="tarjeta_receta">'
				+ '  <img class="card-img-top" src="img/img_recetas/' + data.listaRecetas[i].id + '.jpg"'
				+ '"><div class="card-body" style="color:black"><h5 class="card-title">' + data.listaRecetas[i].titulo 
				+ '</h4><p class="card-text"><i class="fas fa-clock"></i> Tiempo:  <label>' + data.listaRecetas[i].tiempoPreparacion + " min" + 
				'</label><i class="fas fa-exclamation ml-3"></i> Dificultad: <label>' + data.listaRecetas[i].dificultad + '</label></p></div></div></a>').hide().fadeIn(300);
        	}
        }
	});
}

function getRecetasBusqueda(titulo, creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad, quiereExcluirAlergenos){

	//Preparo los parametros para que lleguen todo bien mapeados a RecetasRestController.
	//Si algo a partir de aquí está a null, no irá siquiera en la URL de petición, y por tanto en getRecetasBuscadas(...) de RecetasRestController
	//aquellos no incluidos también serán null allí y no se tendrán en cuenta para las querys.
	/*
	if(titulo == null) titulo = "null";
	if(creador == null) creador = "null";
	if(tiempoMinimo == null) tiempoMinimo = -1;
	if(tiempoMaximo == null) tiempoMaximo = -1;
	if(cultura == null) cultura = "null";
	if(minCal == null) minCal = -1;
	if(maxCal == null) maxCal = -1;
	if(dificultad == null) dificultad = "null";
	*/

	var miURL = "/recetas/busqueda?";//titulo=" + titulo + "&creador=" + creador + "&tiempoMinimo=" + tiempoMinimo 
				//+ "&tiempoMaximo=" + tiempoMaximo + "&cultura=" + cultura + "&minCal=" + minCal + 
				//"&maxCal=" + maxCal + "&dificultad=" + dificultad;
	if(titulo != null) miURL += "&titulo=" + titulo;
	if(creador != null) miURL += "&creador=" + creador;
	if(tiempoMinimo != null) miURL += "&tiempoMinimo=" + tiempoMinimo;
	if(tiempoMaximo != null) miURL += "&tiempoMaximo=" + tiempoMaximo;
	if(cultura != null) miURL += "&cultura=" + cultura;
	if(minCal != null) miURL += "&minCal=" + minCal;
	if(maxCal != null) miURL += "&maxCal=" + maxCal;
	if(dificultad != null) miURL += "&dificultad=" + dificultad;
		
	$.ajax({
        url: miURL,
        type: "GET",
        success: function (data) {
			
			var cantidadFinal = data.listaRecetas.length;
			if(quiereExcluirAlergenos){
				
				//Primero: me quedo con los ids de los alérgenos del usuario que está buscando en este momento
				var idsAlergenosUsuario;
				$.ajax({
					async: false,
					url : "alergiasusuarios/" + getCookie("username"),
					type : "GET",
					success : function(data) {
						idsAlergenosUsuario = data.listaAlergiasUsuarios; //OJO, idsAlergenosUsuario.length puede ser tambien 0 si el que busca no tiene alergias
					}
				}); 

				//Por cada receta, decido sobre la marcha si la dejo o no en el array data.listaRecetas

				for (var indice = 0; indice < data.listaRecetas.length; indice++){
					//Segundo: por cada receta obtengo los ids de sus ingredientes

					var ingsReceta;
					$.ajax({
						async: false,
						url : "ingrecetas/" + data.listaRecetas[indice].id,
						type : "GET",
						success : function(data2) {
							
							ingsReceta = data2.listaIngRecetas;
						}
					});

					//Si se detecta al menos un ingrediente cuyo id está asociado con un alérgeno del usuario actual, la receta de la posición "indice" se elimina de data.listaRecetas
					var detectadoAlergeno = false;
					for (var i = 0; i < ingsReceta.length && !detectadoAlergeno; i++){

						$.ajax({
							async: false,
							url : "alergenosingredientes/" + ingsReceta[i].idIngrediente,
							type : "GET",
							success : function(data3) {
								
								//OJO, un ingrediente puede no tener ningun alergeno, por tanto data3 sería directamente null
								if(data3 != null){ //el ingrediente sí tiene alergeno asociado
									//miro si el id de ese alergeno está entre los alergenos del usuario
									for (var j = 0; j < idsAlergenosUsuario.length && !detectadoAlergeno; j++){
										if(idsAlergenosUsuario[j].idAlergeno == data3.idAlergeno){
											detectadoAlergeno = true;
											data.listaRecetas[indice] = null;
											cantidadFinal--;
										}
									}
								}
							}
						});

					}
					
				}

				

			}//if quiereExcluirAlergenos
			
			$("#info_filtros_usados").append('Se han encontrado ' + cantidadFinal + ' recetas.').hide().fadeIn(300);
			for (var i = 0; i < data.listaRecetas.length; i++) {
				if(data.listaRecetas[i] != null){
					$("#contenedor_recetas").append('<a href="receta_concreta.html?id=' + data.listaRecetas[i].id + '" <div class="card" data-id="tarjeta_receta">'
					+ ' <img class="card-img-top" src="img/img_recetas/' + data.listaRecetas[i].id + '.jpg"'
					+ '"><div class="card-body" style="color:black"><h5 class="card-title">' + data.listaRecetas[i].titulo 
					+ '</h5><p class="card-text"><i class="fas fa-clock"></i> Tiempo:  <label>' + data.listaRecetas[i].tiempoPreparacion + " min" + 
					'</label><i class="fas fa-exclamation ml-3"></i> Dificultad: <label>' + data.listaRecetas[i].dificultad + '</label></p></div></div></a>').hide().fadeIn(300);
				}
			}
			
		},//success
		error: function(textStatus, errorThrown) { 
				//$("#info_filtros_usados").append('Se han encontrado 0 recetas.').hide().fadeIn(300);
		} 
	});	
}
