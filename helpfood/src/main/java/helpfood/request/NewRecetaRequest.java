package helpfood.request;

import javax.validation.constraints.NotNull;

public class NewRecetaRequest {
	@NotNull
	private int id;
	@NotNull
	private String titulo;
	private String cultura;
	private String dificultad;
	private String utensilios;
	@NotNull
	private int tiempoPreparacion;
	@NotNull
	private String pasos;
	@NotNull
	private String fecha;
	//private int popularidad;
	@NotNull
	private String nick;
	private int caloriasTotales;
	private String descripcion;
	
	private String nombreArchivo;
	//Campos del ingReceta (idIngrediente, cantidad)
	private String ingredientes;
	
	public NewRecetaRequest() {};
	
	public NewRecetaRequest(String titulo) {
		this.titulo = titulo;
	}

	public NewRecetaRequest(@NotNull int id, @NotNull String titulo, String cultura, String dificultad,
			String utensilios, @NotNull int tiempoPreparacion, @NotNull String pasos, @NotNull String fecha,
			@NotNull String nick, int caloriasTotales, String descripcion, String nombreArchivo, String ingredientes) {
		this.id = id;
		this.titulo = titulo;
		this.cultura = cultura;
		this.dificultad = dificultad;
		this.utensilios = utensilios;
		this.tiempoPreparacion = tiempoPreparacion;
		this.nombreArchivo = nombreArchivo;
		this.pasos = pasos;
		this.fecha = fecha;
		this.nick = nick;
		this.caloriasTotales = caloriasTotales;
		this.descripcion = descripcion;
		this.ingredientes = ingredientes;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCultura() {
		return cultura;
	}

	public void setCultura(String cultura) {
		this.cultura = cultura;
	}

	public String getDificultad() {
		return dificultad;
	}

	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}

	public String getUtensilios() {
		return utensilios;
	}

	public void setUtensilios(String utensilios) {
		this.utensilios = utensilios;
	}

	public int getTiempoPreparacion() {
		return tiempoPreparacion;
	}

	public void setTiempoPreparacion(int tiempoPreparacion) {
		this.tiempoPreparacion = tiempoPreparacion;
	}

	public String getPasos() {
		return pasos;
	}

	public void setPasos(String pasos) {
		this.pasos = pasos;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getCaloriasTotales() {
		return caloriasTotales;
	}

	public void setCaloriasTotales(int caloriasTotales) {
		this.caloriasTotales = caloriasTotales;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(String ingredientes) {
		this.ingredientes = ingredientes;
	}
	
}