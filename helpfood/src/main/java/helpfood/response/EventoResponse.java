package helpfood.response;

/*
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Id;
*/

public class EventoResponse {
	
	private int id_receta;
	private String title;
	private String start;
	//private boolean allDay;
	private String nick;
	//private String url;
	
	public EventoResponse() {
		//this.allDay = false;
	}

	public int getIdReceta() {
		return this.id_receta; 
	}
	
	public void setid(int id) {
		this.id_receta = id;
	}
	
	
	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
	public String getStart() {
		return this.start;
	}
	
	public void setStart(String start) {
		this.start = start;
	}
	
	/*
	public boolean getAllDay() {
		return false;
	}
	
	public void setAllDay(boolean allDay) {
		this.allDay = false;
	}
	*/
	
	public String getNick() {
		return this.nick;
	}
	
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	/*
	public String geturl() {
		return this.url;
	}
	
	public void seturl(String url) {
		this.url = url;
	}
	*/

}