package helpfood.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import helpfood.service.SubirFotoImpl;

@RestController
public class SubirFotoController {
	@Autowired
    private SubirFotoImpl uploadFileService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<?>  uploadFile(@RequestParam("file") MultipartFile file) {
        try {
            uploadFileService.saveFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<Object>("<b>Cargando...</b><script>setTimeout(function() {" + 
        		"              window.location.assign('/mis_recetas.html');" + 
        		"        }, 1000);</script>", HttpStatus.OK);
    }
}
