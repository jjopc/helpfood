package helpfood.service;

import java.util.Optional;

import javax.validation.Valid;

import helpfood.request.NewUsuarioRequest;
import helpfood.response.ListaUsuariosResponse;
import helpfood.response.UsuarioResponse;

public interface UsuarioService {
	public Optional<UsuarioResponse> getUsuario(String nick);
	public ListaUsuariosResponse getAllUsuarios();
	public UsuarioResponse createUsuario(NewUsuarioRequest usuarioRequest);
	public Optional<UsuarioResponse> borrarUsuario(String nick);
	public UsuarioResponse editarUsuario(@Valid NewUsuarioRequest usuarioRequest);
}