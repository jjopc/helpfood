package helpfood.request;

import javax.validation.constraints.NotNull;

public class NewListasDeRecetasDeUnUsuarioRequest {
	
	@NotNull
	private String nick;
	@NotNull
	private String titulo;
	
	//TODO añadir una lista de recetas que haya en la lista al principio(a lo mejor no hace falta porque cuando se crean las listas estan vacias)

	public NewListasDeRecetasDeUnUsuarioRequest() {};
	
	public NewListasDeRecetasDeUnUsuarioRequest(@NotNull String titulo, @NotNull String nick) {
		this.titulo = titulo;
		this.nick = nick;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}
