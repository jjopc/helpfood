package helpfood.service;

import java.util.Optional;

import helpfood.response.ListaAlergenosResponse;
import helpfood.entity.AlergenoEntity;

public interface AlergenoService {
	public Optional<AlergenoEntity> getAlergeno(int idalergeno);
	public ListaAlergenosResponse getAllAlergenos();
}