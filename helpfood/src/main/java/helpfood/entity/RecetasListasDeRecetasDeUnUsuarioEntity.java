package helpfood.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(CompositeKeyRecetasListasDeRecetas.class)
@Table(name = "listas_recetas")
public class RecetasListasDeRecetasDeUnUsuarioEntity {
    
    @Id
    @Column(name = "id_receta")
    private int idReceta;

    @Id
    @Column(name = "titulo_lista")
    private String tituloLista;

    @Id
    @Column(name = "nick")
    private String nick;

    public RecetasListasDeRecetasDeUnUsuarioEntity() {
    }

    public RecetasListasDeRecetasDeUnUsuarioEntity(int idReceta, String tituloLista, String nick) {
        this.idReceta = idReceta;
        this.tituloLista = tituloLista;
        this.nick = nick;
    }

    public int getIdReceta() {
        return idReceta;
    }

    public void setIdReceta(int idReceta) {
        this.idReceta = idReceta;
    }

    public String getTituloLista() {
        return tituloLista;
    }

    public void setTituloLista(String tituloLista) {
        this.tituloLista = tituloLista;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    
}