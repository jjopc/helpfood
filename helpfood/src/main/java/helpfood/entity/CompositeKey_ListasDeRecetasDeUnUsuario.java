package helpfood.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CompositeKey_ListasDeRecetasDeUnUsuario implements Serializable{
    private String nick;
    private String titulo;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
}
