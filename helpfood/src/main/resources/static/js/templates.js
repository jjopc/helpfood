$(document).ready(function () {
	$('.mostrarEncabezado').load('templates/encabezado.html');
	$('.mostrarFooter').load('templates/footer.html');
	setNombre();
});

function setNombre() {
	$.ajax({
		success: function () {
			$("#dropdownMenuLink").append(getCookie("username"));
			$("#user_dropdown").append(getCookie("username"));
		}
	});
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function cerrarSesion() {
	document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
	window.location.assign("/login.html");
}