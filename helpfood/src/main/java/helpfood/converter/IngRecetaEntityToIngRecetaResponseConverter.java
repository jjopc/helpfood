package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.IngRecetaEntity;
import helpfood.response.IngRecetaResponse;

public class IngRecetaEntityToIngRecetaResponseConverter implements Converter<IngRecetaEntity, IngRecetaResponse> {
	@Override
	public IngRecetaResponse convert(IngRecetaEntity ingreceta) {
		IngRecetaResponse ingRecetaRes = new IngRecetaResponse();
		ingRecetaRes.setIdReceta(ingreceta.getIdReceta());
		ingRecetaRes.setIdIngrediente(ingreceta.getIdIngrediente());
		ingRecetaRes.setCantidad(ingreceta.getCantidad());
		return ingRecetaRes;
	}
}