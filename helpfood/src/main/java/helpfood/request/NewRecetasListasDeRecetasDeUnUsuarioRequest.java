package helpfood.request;

import javax.validation.constraints.NotNull;

public class NewRecetasListasDeRecetasDeUnUsuarioRequest {
    
    @NotNull
    private int id_receta;
    @NotNull
    private String titulo_lista;
    @NotNull
    private String nick;

    public NewRecetasListasDeRecetasDeUnUsuarioRequest() {
    }

    public NewRecetasListasDeRecetasDeUnUsuarioRequest(@NotNull int id_receta, @NotNull String titulo_lista,
            @NotNull String nick) {
        this.id_receta = id_receta;
        this.titulo_lista = titulo_lista;
        this.nick = nick;
    }

    public int getId_receta() {
        return id_receta;
    }

    public void setId_receta(int id_receta) {
        this.id_receta = id_receta;
    }

    public String getTitulo_lista() {
        return titulo_lista;
    }

    public void setTitulo_lista(String titulo_lista) {
        this.titulo_lista = titulo_lista;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    
    
}