package helpfood.entity;

import javax.persistence.*;

@Entity
@IdClass(CompositeKeyPlanificacionEntity.class)
@Table(name = "planificaciones")
public class PlanificacionEntity {
	
	@Id
	private String nick;
	
	@Id
	//@Column(name="id_receta")
	private int id_receta;
	
	@Id
	//@Column(name="fecha_hora")
	private String fecha_hora;
	
	public PlanificacionEntity() {
		 
	};

	public String getnick() {
		return this.nick;
	}
	
	public void setnick(String nick) {
		this.nick = nick;
	}
	
	public int getid_receta() {
		return this.id_receta;
	}
	
	public void setid_receta(int id) {
		this.id_receta = id;
	}
	
	public String getfecha_hora() {
		return this.fecha_hora;
	}
	
	public void setfecha_hora(String fecha_hora) {
		this.fecha_hora = fecha_hora;
	}
	
	
	
	
}