package helpfood.response;

import java.util.List;

public class ListaAlergenosResponse {
	private List<AlergenoResponse> listaAlergenos;

	public ListaAlergenosResponse(List<AlergenoResponse> listaAlergenos) {
		this.listaAlergenos = listaAlergenos;
	}

	public ListaAlergenosResponse() {
	}

	public List<AlergenoResponse> getListaAlergenos() {
		return listaAlergenos;
	}

	public void setListaAlergenos(List<AlergenoResponse> listaAlergenos) {
		this.listaAlergenos = listaAlergenos;
	}
}