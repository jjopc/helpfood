package helpfood.entity;

import java.util.ArrayList;

public class ListaEntity {
	private int id;
	private String titulo;
	private String nickUsuario; //Usuario que ha creado la receta
	private ArrayList<RecetaEntity> listaRecetas;
	
	public ListaEntity(int id, String titulo, String nickUsuario, ArrayList<RecetaEntity> listaRecetas) {
		this.id = id;
		this.titulo = titulo;
		this.nickUsuario = nickUsuario;
		this.listaRecetas = listaRecetas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getNickUsuario() {
		return nickUsuario;
	}

	public void setNickUsuario(String nickUsuario) {
		this.nickUsuario = nickUsuario;
	}

	public ArrayList<RecetaEntity> getListaRecetas() {
		return listaRecetas;
	}

	public void setListaRecetas(ArrayList<RecetaEntity> listaRecetas) {
		this.listaRecetas = listaRecetas;
	}
	
}