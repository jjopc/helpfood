package helpfood.service;

import java.util.Optional;

import javax.validation.Valid;

import helpfood.request.NewIngRecetaRequest;
import helpfood.response.IngRecetaResponse;
import helpfood.response.ListaIngRecetasResponse;

public interface IngRecetaService {
	public ListaIngRecetasResponse getAllIngRecetas();
	public ListaIngRecetasResponse findByIdRecetaAndIdIngrediente(int idreceta, int idingrediente);
	public ListaIngRecetasResponse findByIdReceta(int idreceta);
	public IngRecetaResponse createIngReceta(@Valid NewIngRecetaRequest ingRecetaRequest);
	public Optional<IngRecetaResponse> borrarIngReceta(int idreceta);
}