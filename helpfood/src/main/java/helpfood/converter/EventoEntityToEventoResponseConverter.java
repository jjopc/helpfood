package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.EventoEntity;
import helpfood.response.EventoResponse;

public class EventoEntityToEventoResponseConverter implements Converter<EventoEntity, EventoResponse> {
	@Override
	public EventoResponse convert(EventoEntity evento) {
		EventoResponse eventoRes = new EventoResponse(); 
		eventoRes.setid(evento.getid());
		eventoRes.setTitle(evento.getTitle());
		eventoRes.setStart(evento.getStart());
		eventoRes.setNick(evento.getNick());
		return eventoRes;
	}
}