package helpfood.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import helpfood.entity.RecetasListasDeRecetasDeUnUsuarioEntity;

@Repository
public interface RecetasListasDeRecetasDeUnUsuarioRepository extends CrudRepository<RecetasListasDeRecetasDeUnUsuarioEntity, String> {
    
    public List<RecetasListasDeRecetasDeUnUsuarioEntity> findAll();

    @Query ("SELECT e "
             + "FROM RecetasListasDeRecetasDeUnUsuarioEntity e "
             + "WHERE e.tituloLista = :titulo_lista "
             + "AND e.nick = :nick")
    public List<RecetasListasDeRecetasDeUnUsuarioEntity> findByTituloListaAndNick(@Param ("titulo_lista") String tituloLista, @Param ("nick") String nick);

    @Query("SELECT e "
            + "FROM RecetasListasDeRecetasDeUnUsuarioEntity e "
            + "WHERE e.idReceta = :id_receta")
    public List<RecetasListasDeRecetasDeUnUsuarioEntity> findByIdReceta(@Param ("id_receta") int idReceta);

    @Query ("SELECT e "
             + "FROM RecetasListasDeRecetasDeUnUsuarioEntity e "
             + "WHERE e.tituloLista = :titulo_lista "
             + "AND e.nick = :nick "
             + "AND e.idReceta = :id_receta")
    public RecetasListasDeRecetasDeUnUsuarioEntity findByTituloListaAndNickAndIdReceta(@Param ("titulo_lista") String tituloLista, @Param ("nick") String nick, @Param ("id_receta") int idReceta);
        

    @Modifying
    @Transactional
    @Query("DELETE FROM RecetasListasDeRecetasDeUnUsuarioEntity e "
            + "WHERE e.tituloLista = :titulo_lista "
            + "AND e.nick = :nick")
    public void deleteTodaLaListaDeRecetasListasDeRecetasDeUnUsuario(@Param ("titulo_lista") String tituloLista, @Param ("nick") String nick);

    @Modifying
    @Transactional
    @Query("DELETE FROM RecetasListasDeRecetasDeUnUsuarioEntity e "
            + "WHERE e.tituloLista = :titulo_lista "
            + "AND e.nick = :nick "
            + "AND e.idReceta = :id_receta")
    public void deleteUnaRecetaDeUnaListaDeRecetasListasDeRecetasDeUnUsuario(@Param ("titulo_lista") String tituloLista, @Param ("nick") String nick, @Param ("id_receta") int idReceta);
    
    @Modifying
    @Transactional
    @Query("DELETE FROM RecetasListasDeRecetasDeUnUsuarioEntity e "
            + "WHERE e.nick = :nick")
    public void deleteTodaLaListaDeRecetasListasDeRecetas(@Param ("nick") String nick);
    
    @Modifying
    @Transactional
    @Query("DELETE FROM RecetasListasDeRecetasDeUnUsuarioEntity e "
            + "WHERE e.idReceta = :id")
    public void deleteListaApareceReceta(@Param ("id") int id);

}