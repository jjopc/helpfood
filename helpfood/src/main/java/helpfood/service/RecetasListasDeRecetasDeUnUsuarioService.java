package helpfood.service;

import java.util.List;

import helpfood.request.NewRecetasListasDeRecetasDeUnUsuarioRequest;
import helpfood.response.RecetasListasDeRecetasDeUnUsuarioResponse;

public interface RecetasListasDeRecetasDeUnUsuarioService {
    public List<RecetasListasDeRecetasDeUnUsuarioResponse> findAllRecetasListasDeRecetasDeUnUsuario();
    public List<RecetasListasDeRecetasDeUnUsuarioResponse> findByTituloListaAndNick(String titulo, String nick);
    public List<RecetasListasDeRecetasDeUnUsuarioResponse> findByIdReceta(int id);
    public RecetasListasDeRecetasDeUnUsuarioResponse findByTituloListaAndNickAndIdReceta(String titulo, String nick, int id);
    public void deleteTodaLaListaDeRecetasListasDeRecetasDeUnUsuario(String titulo, String nick);
    public void deleteUnaRecetaDeUnaListaDeRecetasListasDeRecetasDeUnUsuario(String titulo, String nick, int id);
    public RecetasListasDeRecetasDeUnUsuarioResponse createRecetasListasDeUnUsuario(NewRecetasListasDeRecetasDeUnUsuarioRequest request); 
    public void deleteTodaLaListaDeRecetasListasDeRecetas(String nick);
    public void deleteListaApareceReceta(int id);
}