package helpfood.controller;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import helpfood.response.AlergiasUsuariosResponse;
import helpfood.response.ListaAlergiasUsuariosResponse;
import helpfood.service.AlergiasUsuariosService;


@RestController
@RequestMapping(path = "/alergiasusuarios")
public class AlergiasUsuariosRestController {
	
	@Autowired
	private AlergiasUsuariosService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ListaAlergiasUsuariosResponse> getAllAlergiasUsuarios() {
		return ResponseEntity.ok(service.getAllAlergiasUsuarios());
	}
	
	@RequestMapping(path = "/{nick}", method = RequestMethod.GET)
	public ResponseEntity<ListaAlergiasUsuariosResponse> getAlergiasUsuarioConcreto(@PathVariable String nick) {
		return ResponseEntity.ok(service.getAlergiasUsuarioConcreto(nick));
	}
	
	@RequestMapping(path = "/delete/{nick}", method = RequestMethod.DELETE)
	public Optional<AlergiasUsuariosResponse> borrarAlergiasUsuario(@PathVariable String nick) {
		Optional<AlergiasUsuariosResponse> response = service.borrarAlergias(nick);
		return response;
	}
	
}