$(document).ready(function () {

	$("#añadirReceta").hide();
	$("#crearLista").click(function () {
		$("#añadirReceta").show();
		crearNuevaLista($("#titulo").val());
		$("#crearLista").hide();
	});

});

function crearNuevaLista(titulo) {
	var username = getCookie('username');

	$.ajax({
		url: "listas_usuarios",
		type: "POST",
		data: {
			nick: username,
			titulo: titulo
		},
		success: function (data) {
			alert("Lista creada con éxito");
		},
		error: function (data, errorThrown) {
			alert("Ha ocudrrido un problema en la creacón de la lista.")
		}
	});
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
	  var c = ca[i];
	  while (c.charAt(0) == ' ') {
		c = c.substring(1);
	  }
	  if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
	  }
	}
	return "";
  }
