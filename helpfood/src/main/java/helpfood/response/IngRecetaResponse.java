package helpfood.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class IngRecetaResponse {
	
	private int id_receta;
	private int id_ingrediente;
	private int cantidad;
	
	public IngRecetaResponse() {}
	
	@JsonIgnore //con esto hago que este campo no vaya en el json resultante, porque no lo necesitamos
	//@JsonProperty(value = "user_password")
	public int getIdReceta() {
		return this.id_receta;
	}

	public void setIdReceta(int id) {
		this.id_receta = id;
	}

	public int getIdIngrediente() {
		return this.id_ingrediente;
	}

	public void setIdIngrediente(int id) {
		this.id_ingrediente = id;
	}
	
	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
}