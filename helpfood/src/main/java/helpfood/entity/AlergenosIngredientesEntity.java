package helpfood.entity;

import javax.persistence.*;

@Entity
@IdClass(CompositeKeyAlergenosIngredientes.class)
@Table(name = "alergenos_ingredientes")
public class AlergenosIngredientesEntity {
	
	@Id
	@Column(name="id_ingrediente")
	private int idingrediente;
	
	@Id
	@Column(name="id_alergeno")
	private int idalergeno;
	
	public AlergenosIngredientesEntity() {};
	
	public AlergenosIngredientesEntity(int idingrediente, int idalergeno) {
		
		this.idingrediente = idingrediente;
		this.idalergeno = idalergeno;
	}

	public int getIdIngrediente() {
		return this.idingrediente;
	}
	
	public void setIdIngrediente(int id) {
		this.idingrediente = id;
	}
	
	public int getIdAlergeno() {
		return this.idalergeno;
	}
	
	public void setIdAlergeno(int id) {
		this.idalergeno = id;
	}
	
}