package helpfood.repository;

import org.springframework.stereotype.Repository;

import helpfood.entity.UsuarioEntity;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, String>{
	public Optional<UsuarioEntity> findByNick(String nick);
	public List<UsuarioEntity> findAll();
}