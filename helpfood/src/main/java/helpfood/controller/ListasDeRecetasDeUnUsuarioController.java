package helpfood.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import helpfood.request.NewListasDeRecetasDeUnUsuarioRequest;
import helpfood.response.Lista_listasDeRecetasDeUnUsuarioResponse;
import helpfood.response.ListasDeRecetasDeUnUsuarioResponse;
import helpfood.service.ListasDeRecetasDeUnUsuarioService;


@RestController
public class ListasDeRecetasDeUnUsuarioController {
    
    @Autowired
    private ListasDeRecetasDeUnUsuarioService service;
    
    @RequestMapping(path = "/listas_usuarios/{nick}/{titulo}", method = RequestMethod.GET)
    public ResponseEntity<ListasDeRecetasDeUnUsuarioResponse> getListaDeRecetasXDeUnUsuario(@PathVariable String nick, @PathVariable String titulo) {
    	Optional<ListasDeRecetasDeUnUsuarioResponse> response = service.getListaFromUsuario(nick, titulo);
        return response.isPresent() ? ResponseEntity.ok(response.get()) : ResponseEntity.notFound().build();
    }
    
   @RequestMapping(path = "/listas_usuarios/{nick}", method = RequestMethod.GET)
    public ResponseEntity<Lista_listasDeRecetasDeUnUsuarioResponse> getAllListasDeRecetasDeUnUsuario(@PathVariable String nick) {
        return ResponseEntity.ok(service.getAllListasUsuariosFromUsuario(nick));
    }
    
    @RequestMapping(path = "/listas_usuarios", method = RequestMethod.POST)
    public ResponseEntity<ListasDeRecetasDeUnUsuarioResponse> createListaDeRecetasDeUnUsuario(@Valid NewListasDeRecetasDeUnUsuarioRequest listaRequest) {
    	
    	ListasDeRecetasDeUnUsuarioResponse response = service.createListasDeRecetasDeUnUsuario(listaRequest);
    	URI uriCreated = UriComponentsBuilder.fromPath("/listas_usuarios").buildAndExpand(listaRequest.getNick()).toUri();		
		return ResponseEntity.created(uriCreated).body(response);
    }
    
    
    @RequestMapping(path = "/listas_usuarios/delete/{nick}/{titulo}", method = RequestMethod.DELETE)
    public Optional<ListasDeRecetasDeUnUsuarioResponse> borrarListasDeRecetasDeUnUsuario(@PathVariable String nick, @PathVariable String titulo){
    	
    	Optional<ListasDeRecetasDeUnUsuarioResponse> res = service.borrarListasDeRecetasDeUnUsuario(nick, titulo);
    	return res;
    }
    
    @RequestMapping(path = "/listas_usuarios/delete/{nick}", method = RequestMethod.DELETE)
    public Optional<ListasDeRecetasDeUnUsuarioResponse> borrarListasDeRecetasDeUnUsuario(@PathVariable String nick){
    	
    	Optional<ListasDeRecetasDeUnUsuarioResponse> res = service.borrarListas(nick);
    	return res;
    }
    
    /*

    @RequestMapping(path = "/listas_usuarios/{nick}", method = RequestMethod.GET)
    public ResponseEntity<ListasRecetasUsuarioResponse> getAllListasUsuario(String nick) {
        return ResponseEntity.ok(service.getAllListasUsuario(nick));
    }

    @RequestMapping(path = "/listas_usuarios/{nick}/favoritas", method = RequestMethod.GET)
    public ResponseEntity<ListaRecetasFavoritasUsuarioResponse> getListaRecetasFavoritasUsuario(String nick) {
        Optional<ListaRecetasFavoritasUsuarioResponse> response = service.getListaRecetasFavoritasUsuario(nick);
        return response.isPresent() ? ResponseEntity.ok(response.get()) : ResponseEntity.notFound().build();
    }

    @RequestMapping(path = "/listas_usuarios/{nick}/{titulo}", method = RequestMethod.GET)
    public ResponseEntity<ListaRecetasXUsuarioResponse> getListaRecetasXUsuario(String nick, String titulo) {
        Optional<ListaRecetasXUsuarioResponse> response = service.getListaRecetasXUsuario(nick, titulo);
        return response.isPresent() ? ResponseEntity.ok(response.get()) : ResponseEntity.notFound().build();
    }

    @RequestMapping(path = "/listas_usuarios", method = RequestMethod.POST)
    public ResponseEntity<ListaRecetasXUsuarioResponse> createListaRecetasXUsuairo(NewListaRecetasXUsuarioRequest usuarioRequest) {
        // TODO createListaRecetasXUsuairo tengo que ver las composite key con JPA y RestController
        ListaRecetasXUsuarioResponse response = service.createListaRecetasXUsuairo(usuarioRequest);
        URI uriCreated = UriComponentsBuilder.fromPath("/listas_usuarios").buildAndExpand(usuarioRequest.getNick()).toUri();
        return ResponseEntity.created(uriCreated).body(response);
    }
    */
}