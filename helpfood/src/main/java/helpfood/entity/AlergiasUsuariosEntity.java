package helpfood.entity;

import javax.persistence.*;

@Entity
@IdClass(CompositeKeyAlergiasUsuarios.class)
@Table(name = "alergias_usuarios")
public class AlergiasUsuariosEntity {
	
	@Id
	@Column(name="id_alergeno")
	private int idalergeno;
	
	@Id
	@Column(name="nick")
	private String nick;
	
	public AlergiasUsuariosEntity() {};
	
	public AlergiasUsuariosEntity(int idalergeno, String nick) {
		this.idalergeno = idalergeno;
		this.nick = nick;
	}
	
	public int getIdAlergeno() {
		return this.idalergeno;
	}
	
	public void setIdAlergeno(int id) {
		this.idalergeno = id;
	}
	
	public String getNick() {
		return this.nick;
	}
	
	public void setNick(String nick) {
		this.nick = nick;
	}
	
}