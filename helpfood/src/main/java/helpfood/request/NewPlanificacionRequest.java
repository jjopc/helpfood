package helpfood.request;

import javax.validation.constraints.NotNull;

public class NewPlanificacionRequest {
	
	@NotNull
	private String nick;
	@NotNull
	private int id;
	@NotNull
	private String fecha_hora;
	
	public NewPlanificacionRequest() {
		super();
	}
	
	public NewPlanificacionRequest(@NotNull String nick, @NotNull int id, @NotNull String fecha_hora) {
		super();
		this.nick = nick;
		this.id = id;
		this.fecha_hora = fecha_hora;
	}
	
	/*
	public NewUsuarioRequest(@NotNull String nick, @NotNull String password, @NotNull String email, String nombre,
			String apellidos, Date fechaNacimiento, String codPais, String rol) {
		super();
		this.nick = nick;
		this.password = password;
		this.email = email;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechaNacimiento = fechaNacimiento;
		this.codPais = codPais;
		this.rol = rol;
	}
	*/

	public String getnick() {
		return nick;
	}

	public void setnick(String nick) {
		this.nick = nick;
	}

	public int getid_receta() {
		return id;
	}

	public void setid_receta(int id) {
		this.id = id;
	}

	public String getfecha_hora() {
		return fecha_hora;
	}

	public void setfecha_hora(String fecha_hora) {
		this.fecha_hora = fecha_hora;
	}

}