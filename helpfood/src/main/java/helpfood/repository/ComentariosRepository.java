package helpfood.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import helpfood.entity.ComentarioEntity; 

@Repository
public interface ComentariosRepository extends CrudRepository<ComentarioEntity, String>, JpaSpecificationExecutor<ComentarioEntity>{
	public List<ComentarioEntity> findByidreceta(int id);
	public List<ComentarioEntity> findBynickUsuario(String nick);
	
	@Query("SELECT c FROM ComentarioEntity c WHERE c.nickUsuario = :nickUsuario AND c.idreceta = :idreceta")
	public Optional<ComentarioEntity> findBynickUsuarioAndidreceta(@Param("nickUsuario") String nickUsuario, @Param("idreceta") int idreceta);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM ComentarioEntity e "
			+ "WHERE e.nickUsuario = :nick")
	void deleteValoraciones(@Param("nick") String nick);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM ComentarioEntity e "
			+ "WHERE e.idreceta = :idreceta")
	void deleteValoracionesReceta(@Param("idreceta") int idreceta);
}