package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.AlergenosIngredientesEntityToAlergenosIngredientesResponseConverter;
import helpfood.entity.AlergenosIngredientesEntity;
import helpfood.repository.AlergenosIngredientesRepository;
import helpfood.response.AlergenosIngredientesResponse;
import helpfood.response.ListaAlergenosIngredientesResponse;
@Service
public class AlergenosIngredientesServiceImpl implements AlergenosIngredientesService {
	
	private AlergenosIngredientesEntityToAlergenosIngredientesResponseConverter entityToResponseConverter = new AlergenosIngredientesEntityToAlergenosIngredientesResponseConverter();
	//private NewIngRecetaRequestToIngRecetaEntityConverter requestToEntityConverter = new NewIngRecetaRequestToIngRecetaEntityConverter();
	
	@Autowired
	private AlergenosIngredientesRepository alergenosIngredientesRepository;

	@Override
	public ListaAlergenosIngredientesResponse getAllAlergenosIngredientes() {
		ListaAlergenosIngredientesResponse response = new ListaAlergenosIngredientesResponse();
		List<AlergenosIngredientesEntity> allAlergenosIngredientes = alergenosIngredientesRepository.findAll();
		List<AlergenosIngredientesResponse> allResponses = allAlergenosIngredientes.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaAlergenosIngredientes(allResponses);
		return response;
	}
	
	@Override
	public Optional<AlergenosIngredientesEntity> getAlergenoIngrediente(int idingrediente) {
		return alergenosIngredientesRepository.findByidingrediente(idingrediente);
	}
	
	/*
	@Override
	public ListaIngRecetasResponse findByIdRecetaAndIdIngrediente(int idreceta, int idingrediente) {
		ListaIngRecetasResponse response = new ListaIngRecetasResponse();
		List<IngRecetaEntity> ingReceta = ingRecetaRepository.findByIdRecetaAndIdIngrediente(idreceta, idingrediente);
		List<IngRecetaResponse> allResponses = ingReceta.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaIngRecetas(allResponses);
		return response;
	}
	
	@Override
	public ListaIngRecetasResponse findByIdReceta(int idreceta) {
		ListaIngRecetasResponse response = new ListaIngRecetasResponse();
		List<IngRecetaEntity> ingReceta = ingRecetaRepository.findByIdReceta(idreceta);
		List<IngRecetaResponse> allResponses = ingReceta.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaIngRecetas(allResponses);
		return response;
	}
		
	
	//@Override
	public Optional<IngRecetaResponse> borrarIngReceta(int idreceta){
		Optional<IngRecetaResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		ingRecetaRepository.deleteIngReceta(idreceta);
		return response;
	}
	*/
}