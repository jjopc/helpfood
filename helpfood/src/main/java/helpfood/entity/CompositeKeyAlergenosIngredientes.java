package helpfood.entity;

import java.io.Serializable;

//Nos permite tener para IngRecetaEntity que las 2 columnas especificadas aqui sean ambas clave primaria
@SuppressWarnings("serial")
public class CompositeKeyAlergenosIngredientes implements Serializable {
   
    private int idingrediente;
    private int idalergeno;

    public int getIdingrediente() {
        return idingrediente;
    }

    public void setIdingrediente(int idingrediente) {
        this.idingrediente = idingrediente;
    }

    public int getIdalergeno() {
        return idalergeno;
    }

    public void setIdalergeno(int idalergeno) {
        this.idalergeno = idalergeno;
    }
    
}