$(document).ready(function () {
    obtenerListaConcretaUsuario();

    $("#borrar").click(function () {
        borrarLista();
    });

    $("#borrarRecetaDeLista").click(function () {
        borrarRecetaDeLista();
    });
});

function obtenerListaConcretaUsuario() {
    var username = getCookie('username');
    var titulo = window.location.search
        .split('=')[1];

    $.ajax({
        url: "/listas_recetas/" + username + '/' + titulo,
        type: "GET",
        success: function (recetas) {
            for (var i = 0; i < recetas.length; i++) {
                $.ajax({
                    url: "/recetas/" + recetas[i].idReceta,
                    type: "GET",
                    success: function (data) {
                        $("#contenedor_lista").append(
                            '<div class="card" data-id="tarjeta_receta">'
                            + '<a href="receta_concreta.html?id=' + data.id + '">'
                            + '<img class="card-img-top" src="img/img_recetas/' + data.id + '.jpg"></a>'
                            + '<div class="card-body" style="color:black">'
                            + '<a href="receta_concreta.html?id=' + data.id + '" style="apearance:none; color:black;" class=""><h5 class="card-title">' + data.titulo + '</h5></a>'
                            + '<p class="card-text"><i class="fas fa-clock"></i> Tiempo:  <label>' + data.tiempoPreparacion + " min"
                            + '</label><i class="fas fa-exclamation ml-3"></i> Dificultad: <label>' + data.dificultad + '</label></p>'
                            + '<button type="button" onclick=' + '\'document.cookie="recetaID=' + data.id + ';"\' data-toggle="modal" data-target="#eliminarRecetaDeLista" class="btn btn-danger">'
                            + '<i class="fa fa-trash"></i></div></div>'
                        ).hide().fadeIn(300);
                    },
                    error: function () {
                        alert('Algo ha ido mal al consultar la receta para mostrarla en la lista.');
                    }
                });
            }
        },
        error: function () {
            alert('Algo ha ido mal consultando las recetas de la lista.');
        }
    });
}

function borrarLista() {
    var username = getCookie('username');
    var titulo = window.location.search
        .split('=')[1];

    // Tengo que borrar primero en listas_recetas
    $.ajax({
        url: "/listas_recetas/delete/" + username + '/' + titulo,
        type: "DELETE",
        success: function (data) {
            console.log("Recetas borradas de la lista con éxito");
        },
        error: function () {
            alert('Algo ha ido mal intentando borrar las recetas de la lista de recetas.');
        }
    });

    // Después ya podré borrar en listas_usuarios
    $.ajax({
        url: "/listas_usuarios/delete/" + username + '/' + titulo,
        type: "DELETE",
        success: function (data) {
            alert("Lista borrada con éxito");
            window.location.assign('listas.html');
        },
        error: function () {
            alert('Algo ha ido mal intentando borrar la lista de recetas.');
        }
    });

}

function borrarRecetaDeLista() {
    var username = getCookie('username');
    var titulo = window.location.search.split('=')[1];
    var id = getCookie('recetaID');

    $.ajax({
        url: "/listas_recetas/delete/" + username + '/' + titulo + '/' + id,
        type: "DELETE",
        success: function (data) {
            document.cookie="recetaID=;expires=Thu; 01 Jan 1970";
            window.location.reload();
            console.log("Recetas borradas de la lista con éxito");
        },
        erro: function () {
            alert('Algo ha ido mal intentando borrar las recetas de la lista de recetas.');
        }
    });
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
