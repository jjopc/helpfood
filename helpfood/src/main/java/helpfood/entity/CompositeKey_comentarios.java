package helpfood.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CompositeKey_comentarios implements Serializable{
    private int idreceta;
    private String nickUsuario;

    public int getIdreceta() {
        return idreceta;
    }

    public void setIdreceta(int idreceta) {
        this.idreceta = idreceta;
    }

    public String getNickUsuario() {
        return nickUsuario;
    }

    public void setNickUsuario(String nickUsuario) {
        this.nickUsuario = nickUsuario;
    }

}
