package helpfood.entity;

import java.sql.Date;
import javax.persistence.*;

@Entity
@Table(name = "usuarios")
public class UsuarioEntity {
	@Id
	private String nick;
	
	@Column
	private String password;
	@Column
	private String email;
	@Column
	private String nombre;
	@Column
	private String apellidos;
	@Column
	private Date fechaNacimiento;
	@Column
	private String codPais;
	@Column
	private String rol;
	
	public UsuarioEntity() {};
	
	public UsuarioEntity(String nick) {
		this.nick = nick;
	}
	
	public UsuarioEntity(String nick, String password, String email) {
		this.nick = nick;
		this.password = password;
		this.email = email;
	}
	
	public UsuarioEntity(String nick, String password, String email, String nombre, String apellidos,
			Date fechaNacimiento, String codPais, String rol) {
		super();
		this.nick = nick;
		this.password = password;
		this.email = email;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechaNacimiento = fechaNacimiento;
		this.codPais = codPais;
		this.rol = rol;
	}

	public String getNick() {
		return nick;
	}
	
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public String getCodPais() {
		return codPais;
	}
	
	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}
	
	public String getRol() {
		return rol;
	}
	
	public void setRol(String rol) {
		this.rol = rol;
	}
}