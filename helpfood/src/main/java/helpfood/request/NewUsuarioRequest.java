package helpfood.request;

import java.sql.Date;

import javax.validation.constraints.NotNull;

public class NewUsuarioRequest {
	
	@NotNull
	private String nick;
	@NotNull
	private String password;
	@NotNull
	private String email;
	
	private String nombre;
	private String apellidos;
	private Date fechaNacimiento;
	private String codPais;
	private String rol;
	private String alergias;
	
	public NewUsuarioRequest() {
		super();
	}
	
	public NewUsuarioRequest(@NotNull String nick, @NotNull String password, @NotNull String email) {
		super();
		this.nick = nick;
		this.password = password;
		this.email = email;
	}
	
	public NewUsuarioRequest(@NotNull String nick, @NotNull String password, @NotNull String email, String nombre,
			String apellidos, Date fechaNacimiento, String codPais, String rol, String alergias) {
		super();
		this.nick = nick;
		this.password = password;
		this.email = email;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechaNacimiento = fechaNacimiento;
		this.codPais = codPais;
		this.rol = rol;
		this.alergias = alergias;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCodPais() {
		return codPais;
	}

	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getAlergias() {
		return alergias;
	}

	public void setAlergias(String alergias) {
		this.alergias = alergias;
	}
}