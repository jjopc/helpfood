package helpfood.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

import helpfood.entity.AlergenosIngredientesEntity;
import java.util.List;
import java.util.Optional;


@Repository
public interface AlergenosIngredientesRepository extends CrudRepository<AlergenosIngredientesEntity, String>{
	
	public List<AlergenosIngredientesEntity> findAll();
	public Optional<AlergenosIngredientesEntity> findByidingrediente(int idingrediente);
	
	//public List<IngRecetaEntity> findByidreceta(int id_receta);
	
	/*
	@Query("SELECT e "
			+ "FROM IngRecetaEntity e "
			+ "WHERE e.id_ingrediente = :idreceta AND e.id_ingrediente = :idingrediente")
	public List<IngRecetaEntity> findByIdRecetaAndIdIngrediente(@Param("idreceta") int idreceta, @Param("idingrediente") int idingrediente);
	
	@Query("SELECT e "
			+ "FROM IngRecetaEntity e "
			+ "WHERE e.idreceta = :idreceta")
	public List<IngRecetaEntity> findByIdReceta(@Param("idreceta") int idreceta);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM IngRecetaEntity e "
			+ "WHERE e.idreceta = :idreceta")
	void deleteIngReceta(@Param("idreceta") int idreceta);
	*/
	
}