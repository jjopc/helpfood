package helpfood.response;

import java.util.List;

public class ListaAlergiasUsuariosResponse {
	private List<AlergiasUsuariosResponse> listaAlergiasUsuarios;

	public ListaAlergiasUsuariosResponse(List<AlergiasUsuariosResponse> listaAlergiasUsuarios) {
		this.listaAlergiasUsuarios = listaAlergiasUsuarios;
	}

	public ListaAlergiasUsuariosResponse() {
	}

	public List<AlergiasUsuariosResponse> getListaAlergiasUsuarios() {
		return listaAlergiasUsuarios;
	}

	public void setListaAlergiasUsuarios(List<AlergiasUsuariosResponse> listaAlergiasUsuarios) {
		this.listaAlergiasUsuarios = listaAlergiasUsuarios;
	}
}