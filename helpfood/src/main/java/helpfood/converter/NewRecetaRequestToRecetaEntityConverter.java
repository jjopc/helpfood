package helpfood.converter;

import helpfood.entity.RecetaEntity;
import helpfood.request.NewRecetaRequest;

public class NewRecetaRequestToRecetaEntityConverter {
	public RecetaEntity convert(NewRecetaRequest recetaRequest) {
		RecetaEntity recetaEntity = new RecetaEntity();
		recetaEntity.setId(recetaRequest.getId());
		recetaEntity.setTitulo(recetaRequest.getTitulo());
		recetaEntity.setCultura(recetaRequest.getCultura());
		recetaEntity.setDificultad(recetaRequest.getDificultad());
		recetaEntity.setUtensilios(recetaRequest.getUtensilios());
		recetaEntity.setTiempoPreparacion(recetaRequest.getTiempoPreparacion());
		recetaEntity.setPasos(recetaRequest.getPasos());
		recetaEntity.setFecha(recetaRequest.getFecha());
		recetaEntity.setNick(recetaRequest.getNick());
		recetaEntity.setCaloriasTotales(recetaRequest.getCaloriasTotales());
		recetaEntity.setDescripcion(recetaRequest.getDescripcion());
		return recetaEntity;
	}
}