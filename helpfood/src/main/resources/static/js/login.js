$(document).ready(function(){	
	//Comprobamos si tiene la cookie ya establecida
	checkCookie();
	//Hacemos el login
	 $("#btn_submit").click(function() {
		 buscaUsuario();
	 });

});

function buscaUsuario() {
	$.ajax({
        url: "/usuarios/" + $("#user").val(),
        type: "GET",
        success: function (response) {
        	if (response.password == $("#password").val()) {
				hacerLogin();
			}
			else {
				alert("Error en el login: contraseña incorrecta");
				$("#password").focus();
			}
        },
        error: function() {
        	alert("El usuario no existe, registrate en nuestra página");
        	window.location.assign("/registro.html");
        }
	});
}

function hacerLogin() {
	//Si se pone a 0 no guarda la cookie de sesión, así dura un día. 
	var exdays = 1;
	// Comprueba si el usuario quiere que le recuerde la cookie de sesión, durará un año
	if(document.getElementById("rememberMe").checked)
		exdays = 365;
	setCookie("username",$("#user").val(),exdays);
	alert("¡Bienvenid@! " + $("#user").val());
	window.location.assign("/index.html");
}

function setCookie(cname,cvalue,exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires=" + d.toGMTString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function checkCookie() {
	var user = getCookie("username");
	//Si tiene la cookie ya guardada (marcó la casilla recordarme) va directamente a index.html
	if (user != "") {
		alert("Bienvenid@ de nuevo " + user);
		window.location.assign("/index.html");
	} 
}