package helpfood.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CompositeKeyPlanificacionEntity implements Serializable {
	private String nick;
    private int id_receta;
    private String fecha_hora;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getId_receta() {
        return id_receta;
    }

    public void setId_receta(int id_receta) {
        this.id_receta = id_receta;
    }

    public String getFecha_hora() {
        return fecha_hora;
    }

    public void setFecha_hora(String fecha_hora) {
        this.fecha_hora = fecha_hora;
    }
    
}