package helpfood.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import helpfood.entity.PlanificacionEntity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

@Repository
public interface PlanificacionRepository extends CrudRepository<PlanificacionEntity, String>, JpaSpecificationExecutor<PlanificacionEntity> {
	
	//public List<EventoEntity> findByNick(String nick);
	public List<PlanificacionEntity> findAll(); 
	
	@Query("SELECT e "
			+ "FROM PlanificacionEntity e "
			+ "WHERE e.nick = :nick AND e.id_receta = :id_receta AND e.fecha_hora = :fecha_hora")
	public Optional<PlanificacionEntity> findPlanificacion(@Param("nick") String nick, @Param("id_receta") int id_receta, @Param("fecha_hora") String fecha_hora);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM PlanificacionEntity e "
			+ "WHERE e.nick = :nick AND e.id_receta = :id_receta AND e.fecha_hora = :fecha_hora")
	void deletePlanificacion(@Param("nick") String nick, @Param("id_receta") int id_receta, @Param("fecha_hora") String fecha_hora);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM PlanificacionEntity e "
			+ "WHERE e.nick = :nick")
	void deleteTodasPlanificaciones(@Param("nick") String nick);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM PlanificacionEntity e "
			+ "WHERE e.id_receta = :idreceta")
	void deletePlanificacionesReceta(@Param("idreceta") int idreceta);
	
	/*
	@Query("SELECT e "
			+ "FROM EventoEntity e "
			+ "WHERE e.nick = :nick AND e.start BETWEEN CURRENT_TIMESTAMP() and :fechamax")
	public List<EventoEntity> findByNickAndFechamax(@Param("nick") String nick, @Param("fechamax") String fechamax);
	//public List<Person> find(@Param("lastName") String lastName);
	
	
	@Query("SELECT e "
			+ "FROM EventoEntity e "
			+ "WHERE e.nick = :nick AND e.id_receta = :id_receta")
	public List<EventoEntity> findEventoEntityByNickAndId(@Param("nick") String nick, @Param("id_receta") int id_receta);
	*/
	
}