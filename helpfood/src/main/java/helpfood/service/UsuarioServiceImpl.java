package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.NewUsuarioRequestToUsuarioEntityConverter;
//import helpfood.converter.UsuarioEntityToUsuarioCreatedEventConverter;
import helpfood.converter.UsuarioEntityToUsuarioResponseConverter;
import helpfood.entity.AlergiasUsuariosEntity;
import helpfood.entity.UsuarioEntity;
import helpfood.repository.AlergiasUsuariosRepository;
import helpfood.repository.UsuarioRepository;
import helpfood.request.NewUsuarioRequest;
import helpfood.response.ListaUsuariosResponse;
import helpfood.response.UsuarioResponse;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	private UsuarioEntityToUsuarioResponseConverter entityToResponseConverter = new UsuarioEntityToUsuarioResponseConverter();
	private NewUsuarioRequestToUsuarioEntityConverter requestToEntityConverter = new NewUsuarioRequestToUsuarioEntityConverter();
	//private UsuarioEntityToUsuarioCreatedEventConverter entityToEventConverter = new UsuarioEntityToUsuarioCreatedEventConverter(); 
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	AlergiasUsuariosRepository alergiasUsuariosRepository;
	
	@Override
	public Optional<UsuarioResponse> getUsuario(String nick) {
		Optional<UsuarioEntity> entity = usuarioRepository.findByNick(nick);
		Optional<UsuarioResponse> response;
		response = entity.map(usuarioEntity -> entityToResponseConverter.convert(usuarioEntity));
		return response;
	}

	@Override
	public ListaUsuariosResponse getAllUsuarios() {
		ListaUsuariosResponse response = new ListaUsuariosResponse();
		List<UsuarioEntity> allUsuarios = usuarioRepository.findAll();
		List<UsuarioResponse> allResponses = allUsuarios.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaUsuarios(allResponses);
		return response;
	}

	@Override
	public UsuarioResponse createUsuario(NewUsuarioRequest usuarioRequest) {
		UsuarioResponse response;
		Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(usuarioRequest.getNick());
		if (oldEntity.isPresent()) {
			response = entityToResponseConverter.convert(oldEntity.get());
		} else {
			UsuarioEntity newEntity = requestToEntityConverter.convert(usuarioRequest);
			newEntity = usuarioRepository.save(newEntity);
			String alergiasFront = usuarioRequest.getAlergias();
			String alergiasFrontMod = alergiasFront.substring(1, alergiasFront.length()-1);
			String[] alergias = alergiasFrontMod.split(",");
			
			for (int i = 0; i < alergias.length; i++) {
				AlergiasUsuariosEntity newAlergiasUsuariosEntity = new AlergiasUsuariosEntity(Integer.parseInt(alergias[i]), usuarioRequest.getNick());
				alergiasUsuariosRepository.save(newAlergiasUsuariosEntity);
			}
			response = entityToResponseConverter.convert(newEntity);
		}
		return response;
	}
	
	public UsuarioResponse editarUsuario(NewUsuarioRequest usuarioRequest) {
		UsuarioResponse response;
		Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(usuarioRequest.getNick());
		if(oldEntity.isPresent()) {
			UsuarioEntity newEntity = requestToEntityConverter.convert(usuarioRequest);
			newEntity = usuarioRepository.save(newEntity);
			response = entityToResponseConverter.convert(newEntity);
		}
		else {response=null;}
		return response;
	}
	
	public Optional<UsuarioResponse> borrarUsuario(String nick) {
		Optional<UsuarioResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		usuarioRepository.deleteById(nick);
		return response;
	}
}