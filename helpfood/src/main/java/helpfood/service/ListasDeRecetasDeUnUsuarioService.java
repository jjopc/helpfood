package helpfood.service;

import java.util.Optional;

import helpfood.request.NewListasDeRecetasDeUnUsuarioRequest;
import helpfood.response.Lista_listasDeRecetasDeUnUsuarioResponse;
import helpfood.response.ListasDeRecetasDeUnUsuarioResponse;

public interface ListasDeRecetasDeUnUsuarioService {
	public Optional<ListasDeRecetasDeUnUsuarioResponse> getListaFromUsuario(String nick, String titulo);
    public Lista_listasDeRecetasDeUnUsuarioResponse getAllListasUsuariosFromUsuario(String nick);
    public ListasDeRecetasDeUnUsuarioResponse createListasDeRecetasDeUnUsuario(NewListasDeRecetasDeUnUsuarioRequest listaRequest);
    public Optional<ListasDeRecetasDeUnUsuarioResponse> borrarListasDeRecetasDeUnUsuario(String nick, String titulo);
    public Optional<ListasDeRecetasDeUnUsuarioResponse> borrarListas(String nick);
    /*public Optional<ListaRecetasFavoritasUsuarioResponse> getListaRecetasFavoritasUsuario(String nick);
    public Optional<ListaRecetasXUsuarioResponse> getListaRecetasXUsuario(String nick, String titulo);
    public ListaRecetasFavoritasUsuarioResponse createListaRecetasFavoritasUsurio(NewListaRecetasFavoritasUsuarioRequest usuarioRequest);
    public ListaRecetasXUsuarioResponse createListaRecetasXUsuairo(NewListaRecetasXUsuarioRequest usuarioRequest);*/
}