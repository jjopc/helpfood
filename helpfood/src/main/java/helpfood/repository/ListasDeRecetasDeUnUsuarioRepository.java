package helpfood.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import helpfood.entity.ListasDeRecetasDeUnUsuarioEntity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

@Repository
public interface ListasDeRecetasDeUnUsuarioRepository extends CrudRepository<ListasDeRecetasDeUnUsuarioEntity, String>{
	
	/*@Query("SELECT l "
			+ "FROM ListasUsuariosEntity l "
			+ "WHERE l.nick = :nick AND l.titulo = :titulo")*/
	public Optional<ListasDeRecetasDeUnUsuarioEntity> findByNickAndTitulo(@Param("nick") String nick, @Param("titulo") String titulo);
    
	public List<ListasDeRecetasDeUnUsuarioEntity> findBynick(String nick);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM ListasDeRecetasDeUnUsuarioEntity e " 
			+ "WHERE e.nick = :nick AND e.titulo = :titulo")
	public void deleteListasDeRecetasDeUnUsuario(@Param("nick") String nick, @Param("titulo") String titulo);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM ListasDeRecetasDeUnUsuarioEntity e " 
			+ "WHERE e.nick = :nick")
	public void deleteListas(@Param("nick") String nick);
}