package helpfood.response;

import java.util.List;

public class ListaAlergenosIngredientesResponse {
	private List<AlergenosIngredientesResponse> listaAlergenosIngredientes;

	public ListaAlergenosIngredientesResponse(List<AlergenosIngredientesResponse> listaAlergenosIngredientes) {
		this.listaAlergenosIngredientes = listaAlergenosIngredientes;
	}

	public ListaAlergenosIngredientesResponse() {
	}

	public List<AlergenosIngredientesResponse> getListaAlergenosIngredientes() {
		return listaAlergenosIngredientes;
	}

	public void setListaAlergenosIngredientes(List<AlergenosIngredientesResponse> listaAlergenosIngredientes) {
		this.listaAlergenosIngredientes = listaAlergenosIngredientes;
	}
}