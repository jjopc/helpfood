package helpfood.service;

import java.util.List;
import java.util.Optional;

import helpfood.entity.EventoEntity;
import helpfood.response.ListaEventosResponse;
import helpfood.specifications.SearchCriteria;

public interface EventoService {
	public ListaEventosResponse getEventosUsuario(String nick);
	public ListaEventosResponse getAllEventos();
	List<EventoEntity> searchEvento(List<SearchCriteria> params); //filtros 
	//public EventoResponse createEvento(NewEventoRequest eventoRequest);
	public Optional<EventoEntity> findByFecha(String start);
	public ListaEventosResponse findByNickAndFechamax(String nick, String fechamax);
	public ListaEventosResponse findEventoEntityByNickAndId(String nick, int id_receta);
}