package helpfood.entity;

import java.io.Serializable;

//Nos permite tener para IngRecetaEntity que las 2 columnas especificadas aqui sean ambas clave primaria
@SuppressWarnings("serial")
public class CompositeKey implements Serializable {
    private int idreceta;
    private int id_ingrediente;
   // private int column3;
   
    public int getIdreceta() {
        return idreceta;
    }

    public void setIdreceta(int idreceta) {
        this.idreceta = idreceta;
    }

    public int getId_ingrediente() {
        return id_ingrediente;
    }

    public void setId_ingrediente(int id_ingrediente) {
        this.id_ingrediente = id_ingrediente;
    }
   
}