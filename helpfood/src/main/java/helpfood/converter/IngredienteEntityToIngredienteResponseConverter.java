package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.IngredienteEntity;
import helpfood.response.IngredienteResponse;

public class IngredienteEntityToIngredienteResponseConverter implements Converter<IngredienteEntity, IngredienteResponse> {
	@Override
	public IngredienteResponse convert(IngredienteEntity ingrediente) {
		IngredienteResponse ingredienteRes = new IngredienteResponse();
		ingredienteRes.setNombre(ingrediente.getNombre());
		ingredienteRes.setId(ingrediente.getId());
		return ingredienteRes;
	}
}