package helpfood.converter;

import helpfood.entity.UsuarioEntity;
import helpfood.request.NewUsuarioRequest;

public class NewUsuarioRequestToUsuarioEntityConverter {

	public UsuarioEntity convert(NewUsuarioRequest usuarioRequest) {
		UsuarioEntity usuarioEntity = new UsuarioEntity();
		usuarioEntity.setNick(usuarioRequest.getNick());
		usuarioEntity.setPassword(usuarioRequest.getPassword());
		usuarioEntity.setEmail(usuarioRequest.getEmail());
		usuarioEntity.setNombre(usuarioRequest.getNombre());
		usuarioEntity.setApellidos(usuarioRequest.getApellidos());
		usuarioEntity.setFechaNacimiento(usuarioRequest.getFechaNacimiento());
		usuarioEntity.setCodPais(usuarioRequest.getCodPais());
		usuarioEntity.setRol("normal");
		return usuarioEntity;
	}

}
