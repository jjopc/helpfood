$(document).ready(function() {
	wEnabled();
	var nickUser = getCookie("username");
	mostrarInfo(nickUser);
	wDisabled();

	editarPerfil();
	$("#guardar").click(function() {
		guardarCambios()
	});
	borrarCuenta(nickUser);
});

// funciones
function wEnabled() {
	$("#nick").prop("disabled", false);
	$("#password").prop("disabled", false);
	$("#nombre").prop("disabled", false);
	$("#apellidos").prop("disabled", false);
	$("#email").prop("disabled", false);
	$("#fecha").prop("disabled", false);
	$("#pais").prop("disabled", false);
	$("#rol").prop("disabled", false);
}
function wDisabled() {
	$("#nick").prop("disabled", true);
	$("#password").prop("disabled", true);
	$("#nombre").prop("disabled", true);
	$("#apellidos").prop("disabled", true);
	$("#email").prop("disabled", true);
	$("#fecha").prop("disabled", true);
	$("#pais").prop("disabled", true);
	$("#rol").prop("disabled", true);
	$("#guardar").hide();
	$("#check-box").hide();
}

// mostrar info usuario
function mostrarInfo(nickUser) {
	$.ajax({
		async: false,
		url : "/usuarios/" + nickUser,
		type : "GET",
		success : function(data) {
			$("#nick").val(data.nick);
			$("#password").val(data.password);
			$("#email").val(data.email);
			$("#nombre").val(data.nombre);
			$("#apellidos").val(data.apellidos);
			$("#fecha").val(data.fechaNacimiento);
			$("#pais").val(data.codPais);
			$("#rol").val(data.rol);
		}
	});

	var idsAlergenosUsuario;
	$.ajax({
		async: false,
		url : "alergiasusuarios/" + nickUser,
		type : "GET",
		success : function(data) {
			idsAlergenosUsuario = data.listaAlergiasUsuarios;
		}
	});

	for (var i = 0; i < idsAlergenosUsuario.length; i++) {
		$.ajax({
			async: false,
			url : "alergenos/" + idsAlergenosUsuario[i].idAlergeno,
			type : "GET",
			success : function(data) {
				$("#alergias").append('<option>' + data.nombre + '</option>');
			}
		});
	}
}

// borrado de todos los datos del usuario en orden para que no haya conflictos con las tablas
function borrarCuenta(nickUser) {
	var empty = $("#nick").empty();
	$("#borrar").click(function() {
		borradoCascadaManual(nickUser);
		borrarUsuario(nickUser);
	});
}

function borraCookies() {
    var cookies = document.cookie.split("; ");
    for (var c = 0; c < cookies.length; c++) {
        var d = window.location.hostname.split(".");
        while (d.length > 0) {
            var cookieBase = encodeURIComponent(cookies[c].split(";")[0].split("=")[0]) + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=' + d.join('.') + ' ;path=';
            var p = location.pathname.split('/');
            document.cookie = cookieBase + '/';
            while (p.length > 0) {
                document.cookie = cookieBase + p.join('/');
                p.pop();
            };
            d.shift();
        }
    }
}

// editar datos
function editarPerfil() {
	$("#editar").click(function() {
		$("#password").prop("disabled", false);
		$("#nombre").prop("disabled", false);
		$("#apellidos").prop("disabled", false);
		$("#email").prop("disabled", false);
		$("#fecha").prop("disabled", false);
		$("#pais").prop("disabled", false);
		$("#editar").hide();
		$("#guardar").show();
		$("#check-box").show();
		$("#mostrarClave").click(function() {
			mostrarClave();
		});
	});
}

function mostrarClave() {
	var x = document.getElementById("password");
	if (x.type === "password") {
		x.type = "text";
	} else {
		x.type = "password";
	}
}

function guardarCambios() {
	$.ajax({
		url : "/usuarios/editar",
		type : "PUT",
		dataType : "text",
		async : false,
		data : JSON.stringify({
			nick: $("#nick").val(),
			email: $("#email").val(),
			password: $("#password").val(),
			rol: $("#rol").val(),
			apellidos: $("#apellidos").val(),
			fechaNacimiento: $("#fecha").val(),
			codPais: $("#pais").val(),
			nombre: $("#nombre").val()
		}),
		contentType : "application/json; charset=utf-8",
		success : function() {
			alert("Usuario editado con éxito.");
			window.location.assign("/perfil_usuario.html");
		}
	});
}

 function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function borradoCascadaManual(nick){

	borrarValoraciones(nick);
	//borrarRespuestas(nick); //no estan sus entities ni nada implementado
	borrarPlanificaciones(nick); //OK!!!!!

	borrarLista(nick);
	borrarRecetas(nick); //OK!!!!
	borrarAlergias(nick); //OK!!

	//y al salir de borradoCascadaManual(), se borrara al usuario con el nick "nick" de la tabla de usuarios
}


function chuleta(){

	var miUrl = "/planificaciones/delete/" + nick + "/" + event.idReceta + "/" + event.start._i; //pa debuguear

	$.ajax({
		
		url : miUrl,
		async: false,
		type : "DELETE",
		success : function() {
			
			calendar.fullCalendar('removeEvents');
			var eventos = getJsonEventos(nick);
			calendar.fullCalendar('addEventSource', eventos);
			calendar.fullCalendar('refetchEvents');
		}
	});
}


function borrarValoraciones(nick){

	var miUrl = "/comentarios/delete/" + nick;

	$.ajax({
		async: false,
		url : miUrl,
		type : "DELETE",
		success : function() {
			
			console.log("ok valoraciones");
		}
	});
	
}

function borrarValoracionesReceta(id){

	var miUrl = "/comentarios/delete/receta/" + id;

	$.ajax({
		async: false,
		url : miUrl,
		type : "DELETE",
		success : function() {
			
			console.log("ok valoraciones receta");
		}
	});
	
}



function borrarPlanificaciones(nick){

	var miUrl = "/planificaciones/delete/" + nick + "/all";

	$.ajax({
		async: false,
		url : miUrl,
		type : "DELETE",
		success : function() {
			
			console.log("ok planificaciones");
		}
	});
	
}

function borrarRecetas(nick){
	
	//Primero voy a obtener todos los ids de recetas asociadas a este usuario con el nick "nick"
	var recetasUsuario;
	$.ajax({
		url : "/recetas/nick/" + nick,
		type : "GET",
		async: false,
		success : function(data) {

			recetasUsuario = data.listaRecetas;
			for (var i=0; i < recetasUsuario.length; i++) {             
				//console.log(recetasUsuario[i].id);
				borrarIngRecetas(recetasUsuario[i].id);
				borrarValoracionesReceta(recetasUsuario[i].id);
				
				//Ya borrados todos los ingredientes con sus cantidades para esa receta concreta, borro la receta en sí
				var miUrl = "/recetas/delete/" + recetasUsuario[i].id;

				$.ajax({
					async: false,
					url : miUrl,
					type : "DELETE",
					success : function() {
						
						console.log("ok recetas");
					},
			        error: function (data, errorThrown) {
			            console.log(data);
			        }
				});
			}
			
		}//success
	});
}

function borrarLista(nick) {
    var titulo = window.location.search
        .split('=')[1];

    // Tengo que borrar primero en listas_recetas
    $.ajax({
        url: "/listas_recetas/delete/" + nick,
        type: "DELETE",
        async: false,
        success: function (data) {
        	console.log("ok recetas de la lista");
        },
        error: function () {
            alert('Algo ha ido mal intentando borrar las recetas de la lista de recetas.');
        }
    });

    // Después ya podré borrar en listas_usuarios
    $.ajax({
        url: "/listas_usuarios/delete/" + nick,
        type: "DELETE",
        async: false,
        success: function (data) {
        	console.log("ok listas");
        },
        error: function () {
            alert('Algo ha ido mal intentando borrar la lista de recetas.');
        }
    });

}

function borrarIngRecetas(idreceta){

	
	var miUrl = "/ingrecetas/delete/" + idreceta;

	$.ajax({
		async: false,
		url : miUrl,
		type : "DELETE",
		success : function() {
			
			console.log("ok ingrecetas");
		}
	});
	
	
}

function borrarAlergias(nick){
	$.ajax({
		url : "/alergiasusuarios/delete/" + nick,
		async: false,
		type : "DELETE",
		success : function() {
			console.log("ok alergias");
		}
	});
}

function borrarUsuario(nick){
	$.ajax({
		url : "/usuarios/delete/" + nick,
		async: false,
		type : "DELETE",
		success : function() {
			alert("Cuenta borrada con éxito. ¡Te echaremos de menos! :(");
			borraCookies();
			window.location.assign("/login.html");
		}
	});
}