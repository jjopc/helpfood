package helpfood.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.NewRecetasListasDeUnUsuarioRequestToEntityConverter;
import helpfood.converter.RecetasListasDerecetasDeUnUsuarioEntityToResponseConverter;
import helpfood.entity.RecetasListasDeRecetasDeUnUsuarioEntity;
import helpfood.repository.RecetasListasDeRecetasDeUnUsuarioRepository;
import helpfood.request.NewRecetasListasDeRecetasDeUnUsuarioRequest;
import helpfood.response.RecetasListasDeRecetasDeUnUsuarioResponse;

@Service
public class RecetasListasDeRecetasDeUnUsuarioServiceImpl implements RecetasListasDeRecetasDeUnUsuarioService {

    private RecetasListasDerecetasDeUnUsuarioEntityToResponseConverter converter = new RecetasListasDerecetasDeUnUsuarioEntityToResponseConverter();
    private NewRecetasListasDeUnUsuarioRequestToEntityConverter requestToEntityconverter = new NewRecetasListasDeUnUsuarioRequestToEntityConverter();
    
    @Autowired
    private RecetasListasDeRecetasDeUnUsuarioRepository repository;

    @Override
    public List<RecetasListasDeRecetasDeUnUsuarioResponse> findAllRecetasListasDeRecetasDeUnUsuario() {
        List<RecetasListasDeRecetasDeUnUsuarioEntity> entities = repository.findAll();
        List<RecetasListasDeRecetasDeUnUsuarioResponse> allResponses = entities.stream().map(e -> converter.convert(e))
                .collect(Collectors.toList());
        return allResponses;
    }

    @Override
    public List<RecetasListasDeRecetasDeUnUsuarioResponse> findByTituloListaAndNick(String titulo, String nick) {
        List<RecetasListasDeRecetasDeUnUsuarioEntity> entities = repository.findByTituloListaAndNick(titulo, nick);
        List<RecetasListasDeRecetasDeUnUsuarioResponse> allResponses = entities.stream().map(e -> converter.convert(e))
                .collect(Collectors.toList());
        return allResponses;
    }

    @Override
    public List<RecetasListasDeRecetasDeUnUsuarioResponse> findByIdReceta(int id) {
        List<RecetasListasDeRecetasDeUnUsuarioEntity> entities = repository.findByIdReceta(id);
        List<RecetasListasDeRecetasDeUnUsuarioResponse> allResponses = entities.stream().map(e -> converter.convert(e))
                .collect(Collectors.toList());
        return allResponses;
    }

    @Override
    public RecetasListasDeRecetasDeUnUsuarioResponse findByTituloListaAndNickAndIdReceta(String titulo, String nick,
            int id) {
        RecetasListasDeRecetasDeUnUsuarioEntity entity = repository.findByTituloListaAndNickAndIdReceta(titulo, nick,
                id);
        RecetasListasDeRecetasDeUnUsuarioResponse response = converter.convert(entity);
        return response;
    }

    @Override
    public void deleteTodaLaListaDeRecetasListasDeRecetasDeUnUsuario(String titulo, String nick) {
        repository.deleteTodaLaListaDeRecetasListasDeRecetasDeUnUsuario(titulo, nick);
    }

    @Override
    public void deleteUnaRecetaDeUnaListaDeRecetasListasDeRecetasDeUnUsuario(String titulo, String nick, int id) {
        repository.deleteUnaRecetaDeUnaListaDeRecetasListasDeRecetasDeUnUsuario(titulo, nick, id);
    }

    @Override
    public RecetasListasDeRecetasDeUnUsuarioResponse createRecetasListasDeUnUsuario(
            NewRecetasListasDeRecetasDeUnUsuarioRequest request) {
        RecetasListasDeRecetasDeUnUsuarioResponse response;
        RecetasListasDeRecetasDeUnUsuarioEntity oldEntity = repository.findByTituloListaAndNickAndIdReceta(
                request.getTitulo_lista(), request.getNick(), request.getId_receta());
        if (oldEntity != null)
            response = converter.convert(oldEntity);
        else {
            RecetasListasDeRecetasDeUnUsuarioEntity newEntity = requestToEntityconverter.convert(request);
            newEntity = repository.save(newEntity);
            response = converter.convert(newEntity);
        }

        return response;
    }

	@Override
	public void deleteTodaLaListaDeRecetasListasDeRecetas(String nick) {
		repository.deleteTodaLaListaDeRecetasListasDeRecetas(nick);
	}

	@Override
	public void deleteListaApareceReceta(int id) {
		repository.deleteListaApareceReceta(id);
	}

}