package helpfood.service;

import java.util.Optional;

import helpfood.entity.AlergenosIngredientesEntity;
import helpfood.response.ListaAlergenosIngredientesResponse;

public interface AlergenosIngredientesService {
	public ListaAlergenosIngredientesResponse getAllAlergenosIngredientes();
	public Optional<AlergenosIngredientesEntity> getAlergenoIngrediente(int idingrediente);

	/*
	public ListaIngRecetasResponse findByIdRecetaAndIdIngrediente(int idreceta, int idingrediente);
	public ListaIngRecetasResponse findByIdReceta(int idreceta);
	public IngRecetaResponse createIngReceta(@Valid NewIngRecetaRequest ingRecetaRequest);
	public Optional<IngRecetaResponse> borrarIngReceta(int idreceta);
	*/
}