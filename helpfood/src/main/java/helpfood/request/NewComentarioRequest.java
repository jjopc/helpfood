package helpfood.request;

import javax.validation.constraints.NotNull;

public class NewComentarioRequest {

	@NotNull
	private int id_receta;
	@NotNull
	private String nick;
	@NotNull
	private String texto;
	@NotNull
	private int nota;
	@NotNull
	private String fecha;
	
	public NewComentarioRequest(@NotNull int id_receta, @NotNull String nick, @NotNull String texto, @NotNull int nota, @NotNull String fecha){
		super();
		this.id_receta = id_receta;
		this.nick = nick;
		this.texto = texto;
		this.nota = nota;
		this.fecha = fecha;
	}
	
	public NewComentarioRequest() {
		super();
	}

	public int getId_receta() {
		return id_receta;
	}

	public void setId_receta(int id_receta) {
		this.id_receta = id_receta;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
