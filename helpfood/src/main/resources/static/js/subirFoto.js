$(document).ready(function(){	
  	
  	var nombreArchivo = "";
  	$(document).on('change', 'input[type=file]', function() {
  		
  		//nombreArchivo = $('#file-selector').val();			//fake path
  		nombreArchivo = $('#file-selector')[0].files[0].name;	//name solo
  		
  		$("#subirFoto").prop("disabled", this.files.length == 0);
  		
  	});  	 
		
  
  	$("#subirFoto").click(function() {
  		setTimeout(function() {
  			window.location.assign("/mis_recetas.html");
		}, 1000);  		
	});	
});