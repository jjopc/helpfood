package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import helpfood.converter.IngredienteEntityToIngredienteResponseConverter;
import helpfood.entity.IngredienteEntity;
import helpfood.repository.IngredienteRepository;
import helpfood.response.ListaIngredientesResponse;
import helpfood.response.IngredienteResponse;

@Service
public class IngredienteServiceImpl implements IngredienteService {
	private IngredienteEntityToIngredienteResponseConverter entityToResponseConverter = new IngredienteEntityToIngredienteResponseConverter();
	@Autowired
	private IngredienteRepository ingredienteRepository;
	
	@Override
	public Optional<IngredienteResponse> getIngrediente(String nombre) {
		return null;
		/*Optional<UsuarioEntity> entity = usuarioRepository.findByNick(nick);
		Optional<UsuarioResponse> response;
		response = entity.map(usuarioEntity -> entityToResponseConverter.convert(usuarioEntity));
		return response;*/
	}
	
	@Override
	public Optional<IngredienteEntity> findById(int id_ingrediente){
		return ingredienteRepository.findById(id_ingrediente);
	}
	
	/*
	@Override
	public Optional<IngredienteResponse> findById(int id_ingrediente) {
	
		Optional<IngredienteEntity> entity = ingredienteRepository.findById(id_ingrediente);
		Optional<IngredienteResponse> response;
		response = entity.map(usuarioEntity -> entityToResponseConverter.convert(usuarioEntity));
		return response;
	}*/

	@Override
	public ListaIngredientesResponse getAllIngredientes() {
		ListaIngredientesResponse response = new ListaIngredientesResponse();
		List<IngredienteEntity> allIngredientes = ingredienteRepository.findAll();
		List<IngredienteResponse> allResponses = allIngredientes.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaIngredientes(allResponses);
		return response;
	}
}