package helpfood.controller;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import helpfood.entity.AlergenosIngredientesEntity;
import helpfood.response.ListaAlergenosIngredientesResponse;

import helpfood.service.AlergenosIngredientesService;


@RestController
@RequestMapping(path = "/alergenosingredientes")
public class AlergenosIngredientesRestController {
	
	@Autowired
	private AlergenosIngredientesService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ListaAlergenosIngredientesResponse> getAllAlergenosIngredientes() {
		return ResponseEntity.ok(service.getAllAlergenosIngredientes());
	}
	
	@RequestMapping(path = "/{idingrediente}", method = RequestMethod.GET)
	public ResponseEntity<Optional<AlergenosIngredientesEntity>> getAlergenoIngrediente(@PathVariable int idingrediente){
		return ResponseEntity.ok(service.getAlergenoIngrediente(idingrediente));
	}
	
}