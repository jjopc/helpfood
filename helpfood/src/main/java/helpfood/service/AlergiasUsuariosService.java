package helpfood.service;

import java.util.Optional;

import helpfood.response.AlergiasUsuariosResponse;
import helpfood.response.ListaAlergiasUsuariosResponse;

public interface AlergiasUsuariosService {
	public ListaAlergiasUsuariosResponse getAllAlergiasUsuarios();
	public ListaAlergiasUsuariosResponse getAlergiasUsuarioConcreto(String nick);
	public Optional<AlergiasUsuariosResponse> borrarAlergias(String nick);
	/*
	public ListaIngRecetasResponse findByIdRecetaAndIdIngrediente(int idreceta, int idingrediente);
	public ListaIngRecetasResponse findByIdReceta(int idreceta);
	public IngRecetaResponse createIngReceta(@Valid NewIngRecetaRequest ingRecetaRequest);
	public Optional<IngRecetaResponse> borrarIngReceta(int idreceta);
	*/
}