package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.AlergenosIngredientesEntity;
import helpfood.response.AlergenosIngredientesResponse;

public class AlergenosIngredientesEntityToAlergenosIngredientesResponseConverter implements Converter<AlergenosIngredientesEntity, AlergenosIngredientesResponse> {
	@Override
	public AlergenosIngredientesResponse convert(AlergenosIngredientesEntity alergenosingredientes) {
		AlergenosIngredientesResponse alergenosIngredientesRes = new AlergenosIngredientesResponse();
		alergenosIngredientesRes.setIdIngrediente(alergenosingredientes.getIdIngrediente());
		alergenosIngredientesRes.setIdAlergeno(alergenosingredientes.getIdAlergeno());
		return alergenosIngredientesRes;
	}
}