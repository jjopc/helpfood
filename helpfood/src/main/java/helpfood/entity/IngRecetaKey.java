package helpfood.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@SuppressWarnings("serial")
public class IngRecetaKey implements Serializable {
 
    @Column(name = "id_receta")
    Integer idreceta;
 
    @Column(name = "id_ingrediente")
    Integer idingrediente;
 
    public IngRecetaKey() {}
    
    public IngRecetaKey(int idreceta, int idingrediente) {
    	this.idreceta = idreceta;
    	this.idingrediente = idingrediente;
    }
    
    // standard constructors, getters, and setters
    // hashcode and equals implementation
    /*
    public IngRecetaKey() {
    }
    */

    public int getidreceta() {
        return idreceta;
    }
    
    public void setidreceta(int idreceta) {
        this.idreceta = idreceta;
    }

    public int getidingrediente() {
        return idingrediente;
    }
    
    public void setidingrediente(int idingrediente) {
        this.idingrediente = idingrediente;
    }
    
    
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idingrediente == null) ? 0 : idingrediente.hashCode());
        result = prime * result + ((idreceta == null) ? 0 : idreceta.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IngRecetaKey other = (IngRecetaKey) obj;
        if (idingrediente == null) {
            if (other.idingrediente != null)
                return false;
        } else if (!idingrediente.equals(other.idingrediente))
            return false;
        if (idreceta == null) {
            if (other.idreceta != null)
                return false;
        } else if (!idreceta.equals(other.idreceta))
            return false;
        return true;
    }
    
}