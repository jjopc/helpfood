$(document).ready(function(){
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const idReceta = urlParams.get('id');
	
 	$("#bttnMasComentarios").on("click", function() {
 		mostrarMasComentarios();
   	});
 	 $("form.publicarComentario").submit(function() {
  		crearComentario(idReceta, getCookie("username"));
	 });
 	 
 	$("#userSigned").text("Signed as: " + getCookie("username"));
 	
	cargarComentarios(idReceta, getCookie("username"));
	$("#bttnMasComentarios").hide();
});

var numComentarios = 0;
var pos = 0;
var listadoComentarios;

/*
function crearComentario2(receta, userNick){
	$.post(
		 "/comentarios",
		 {
			 id_receta: receta,
			 nick: userNick,
			 texto: $("#textoComentario").val(),
			 nota: $("#notaComentario").val(),
			 fecha: moment().format("YYYY-MM-DD HH:mm:ss")
		 },
		 function (data, status) {
			 alert("¡Comentario creado correctamente!");
			 window.location.assign("/receta_concreta.html?id="+receta);
	     }
	);
}
*/

function crearComentario(receta, userNick){
	
	$.ajax({
        type: "POST",
        url: "/comentarios",
        dataType: "text",
        async: false,
        data: JSON.stringify({ id_receta: receta, nick: userNick, texto: $("#textoComentario").val(), nota: $("#notaComentario").val(), fecha: moment().format("YYYY-MM-DD HH:mm:ss") }),
        contentType: "application/json; charset=utf-8",
        success: function (data, status) {
			window.location.assign("/receta_concreta.html?id=" + receta);
			alert("¡Comentario publicado!");
			//window.location.assign("/login.html");
        },
        error: function (textStatus, errorThrown) {
            Success = false;
        }
    });
}

function cargarComentarios(idReceta, idUsuario){
	$.ajax({
        url: "/comentarios/receta/" + idReceta,
        type: "GET",    
        success: function (data) {
        	listadoComentarios = data;
        	numComentarios = data.listaComentarios.length;
        	ordenarComentarios();
        	mostrarComentarios();
        	deshabilitarValoracion(idUsuario);
        }
	});	
}

function ordenarComentarios(){
	var i;
	var j;
	var aux;
	for (i = 0; i < numComentarios; i++) {
		for(j = 1; j < numComentarios; ++j){
			if(listadoComentarios.listaComentarios[j - 1].fecha < listadoComentarios.listaComentarios[j].fecha){
				aux = listadoComentarios.listaComentarios[j - 1]
				listadoComentarios.listaComentarios[j - 1] = listadoComentarios.listaComentarios[j];
				listadoComentarios.listaComentarios[j] = aux;
			}
		}
	}
}

function mostrarComentarios(){
	if(numComentarios > 6)
		pos = 5;
	else
		pos = numComentarios;
	for (var i = 0; i < pos; i++) {
		$("#listadoComentarios").append('<div class="d-flex flex-row bd-highlight mx-3 flex-grow-1">' +
        						        	'<div class="mx-3 flex-grow-1">' +
	        						        	'<div class="d-flex justify-content-between mb-4">' +
	        						        		'<span class="titulo_menor">Valoración:</span>' +
	        					    	    		'<div style="color:white"> Puntuacion: ' + listadoComentarios.listaComentarios[i].nota + '</div>' +
	        					        		'</div>' +
	        					        		'<div style="color:white" class="mx-4 mb-4 mt-2">' + listadoComentarios.listaComentarios[i].texto + '</div>' +
	        					        		'<div class="d-flex fledx-row justify-content-end h-25">' +
	        					        			'<i style="color:darkgrey" class="mr-4">' + listadoComentarios.listaComentarios[i].fecha + '</i>' + 
	        					       				'<i style="color:darkgrey">Commented by: ' + listadoComentarios.listaComentarios[i].nick + '</i>' +
	        					       			'</div>' +
	        					       		'</div>' +
        					        	'</div>' +
	        					      	'<hr width=80%  size=10   noshade="noshade" style="color:darkgrey">');
        		
	}
    if(pos < numComentarios)
        $("#bttnMasComentarios").show();
}

function mostrarMasComentarios(){
	var aux = 0;
	if(pos + 5 < numComentarios)
		aux = pos + 5;
	else
		aux = numComentarios;
	for (var i = pos; i < aux; i++) {
		$("#listadoComentarios").append('<div class="d-flex flex-row bd-highlight mx-3 flex-grow-1">' +
						        			'<div class="mx-3 flex-grow-1">' +
    						        			'<div class="d-flex justify-content-between mb-4">' +
    						        				'<span class="titulo_menor">Valoración:</span>' +
    					    	    				'<div style="color:white"> Puntuacion: ' + listadoComentarios.listaComentarios[i].nota + '</div>' +
    					        				'</div>' +
    					        				'<div style="color:white" class="mx-4 mb-4 mt-2">' + listadoComentarios.listaComentarios[i].texto + '</div>' +
    					        				'<div class="d-flex fledx-row justify-content-end h-25">' +
    					        					'<i style="color:darkgrey" class="mr-4">' + listadoComentarios.listaComentarios[i].fecha + '</i>' + 
    					        					'<i style="color:darkgrey">Commented by: ' + listadoComentarios.listaComentarios[i].nick + '</i>' +
    					        				'</div>' +
    					        			'</div>' +
					        			'</div>' +
    					        	'	<hr width=80%  size=10   noshade="noshade" style="color:darkgrey">');
	}
	
	pos = aux;
	if(pos = numComentarios)
		$("#bttnMasComentarios").hide();
}

function deshabilitarValoracion(nick){
	var existeValoracion = false;
	var i = 0;
	while(!existeValoracion && i < numComentarios){
		if(listadoComentarios.listaComentarios[i].nick.toString() == nick)
			existeValoracion = true;
		++i;
	}

	if(existeValoracion)
		$("#crearValoracion").hide();
}
