-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: helpfood
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alergenos`
--

DROP TABLE IF EXISTS `alergenos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alergenos` (
  `id_alergeno` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL COMMENT 'Nombre específico (científico) del alergeno.',
  `observaciones` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_alergeno`),
  UNIQUE KEY `alergenos_nombre_UN` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Tabla que contiene los posibles alergenos presentes en los alimentos.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alergenos`
--

LOCK TABLES `alergenos` WRITE;
/*!40000 ALTER TABLE `alergenos` DISABLE KEYS */;
INSERT INTO `alergenos` VALUES (1,'gluten','El gluten es una proteína que se encuentra en algunas gramíneas, de las cuales proceden las semillas de muchos cereales como son el trigo, cebada, centeno, triticale, espelta, algunas variedades de avena, así como sus híbridos y derivados.'),(2,'huevo','El principal alérgeno de la clara es el ovomucoide, parcialmente estable al calor, seguido por la ovoalbúmina y la conalbúmina. El principal alérgeno de la yema es la albúmina sérica presente en las plumas de las aves, la carne y el huevo de gallina.'),(3,'lácteos','La leche de vaca contiene proteínas divididas entre caseínas (80%) y del suero/séricas (20%). Las caseínas son los alérgenos más potentes, y entre las proteínas séricas los más importantes son la betalactoglobulina (BLG) y la alfalactoalbúmina (ALA).'),(4,'pescado','La alergia al pescado puede ser debida a su ingesta, inhalación de sus vapores o por mero contacto. Sus alérgenos principales son proteínas altamente resistentes al calor (no se modifican al cocinarlas) y a la digestión enzimática intestinal.'),(5,'moluscos','La principal proteína responsable de las reacciones alérgicas a los moluscos es resistente al calor y la cocción. Alguien alérgico a éstos mostrará síntomas si los come crudos o cocidos. Su alergia se asocia a patología respiratoria (rinitis y asma).'),(6,'crustáceos','La principal proteína responsable de las reacciones alérgicas a los crustáceos es resistente al calor y la cocción (producirán síntomas tanto crudos como cocidos). Es muy probable que un alérgico a la gamba lo sea también a otros tipos de crustáceos.'),(7,'cacahuete','La alergia al cacahuete es distinta a la de los frutos secos, pues éste pertenece a la familia de las legumbres. Produce una reacción exagerada del sistema inmunitario y es reconocida como una de las más severas debido a su persistencia.'),(8,'soja','La soja es de la familia de las leguminosas y puede tener reactividad cruzada con otras legumbres (lenteja, cacahuete...). La mayoría de soja que se comercializa en masa corresponde a brotes de judía mungo, que no tiene ninguna relación con la soja.'),(9,'frutos secos','Los alérgicos a una fruta seca oleaginosa suelen reaccionar a diferentes frutas aunque sean de especies diferentes (sobre todo en el caso de los adultos) aunque puede haber excepciones. Las reacciones alérgicas de este grupo pueden ser graves.'),(10,'sésamo','La alergia a la semilla de sésamo puede ocasionar reacciones alérgicas muy graves (anafilaxia) en adultos y niños, así como reacciones asmáticas, y suele acompañar a alergias a los frutos secos (nuez, anacardo...) y legumbres (lenteja, soja...).'),(11,'apio','El apio es la hortaliza más frecuentemente implicada en la anafilaxia asociada a vegetales. Un alérgico al apio es probable que lo sea a la zanahoria y pepino (misma familia). La alergia se suele producir más tras su ingesta crudo que ya cocinado.'),(12,'mostaza','La alergia a la semilla de mostaza es más frecuente y grave en adultos. En niños tiende a ser reacción cutánea. Suele ir con alergias a alimentos de la misma familia (rábano, col), otras semillas (lino, colza) y vegetales (frutos secos, legumbres).'),(13,'altramuz','Se puede desarrollar alergia al altramuz con el tiempo. Un alérgico a otras leguminosas, tras consumir ésta podría tener una reacción al primer contacto. Los alérgicos al cacahuete en particular parecen tener mayor probabilidad de serlo al altramuz.'),(14,'sulfitos','Los sulfitos y bisulfitos son derivados del azufre y utilizadas como antioxidantes o conservantes. Pueden agudizar el asma y ocasionar molestias digestivas y/o cutáneas. Presente en el vino, vinagre, bebidas envasadas, conservas y ultracongelados.'),(15,'legumbres','Los alérgenos de las leguminosas son más potentes cocinados. En muchos casos las legumbres sometidas a cocción aumentan su alergenicidad. Los síntomas abarcan desde desde alergia oral y urticaria hasta anafilaxia. Destacan las lentejas y guisantes.');
/*!40000 ALTER TABLE `alergenos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alergenos_ingredientes`
--

DROP TABLE IF EXISTS `alergenos_ingredientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alergenos_ingredientes` (
  `id_ingrediente` int unsigned NOT NULL,
  `id_alergeno` int unsigned NOT NULL,
  PRIMARY KEY (`id_ingrediente`,`id_alergeno`),
  KEY `alergenos_ingredientes_FK` (`id_alergeno`),
  CONSTRAINT `alergenos_ingredientes_FK` FOREIGN KEY (`id_alergeno`) REFERENCES `alergenos` (`id_alergeno`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `alergenos_ingredientes_FK_1` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingredientes` (`id_ingrediente`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla contiene los alergenos de cada ingrediente.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alergenos_ingredientes`
--

LOCK TABLES `alergenos_ingredientes` WRITE;
/*!40000 ALTER TABLE `alergenos_ingredientes` DISABLE KEYS */;
INSERT INTO `alergenos_ingredientes` VALUES (2,1),(3,1),(55,1),(1,2),(7,3),(8,3),(23,7),(22,9),(24,9),(30,11),(35,14),(25,15),(26,15),(27,15),(34,15);
/*!40000 ALTER TABLE `alergenos_ingredientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alergias_usuarios`
--

DROP TABLE IF EXISTS `alergias_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alergias_usuarios` (
  `nick` varchar(25) NOT NULL,
  `id_alergeno` int unsigned NOT NULL,
  PRIMARY KEY (`nick`,`id_alergeno`),
  KEY `alergias_usuarios_FK` (`id_alergeno`),
  CONSTRAINT `alergias_usuarios_FK` FOREIGN KEY (`id_alergeno`) REFERENCES `alergenos` (`id_alergeno`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `alergias_usuarios_FK_1` FOREIGN KEY (`nick`) REFERENCES `usuarios` (`nick`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla contiene las alergias de los usuarios.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alergias_usuarios`
--

LOCK TABLES `alergias_usuarios` WRITE;
/*!40000 ALTER TABLE `alergias_usuarios` DISABLE KEYS */;
INSERT INTO `alergias_usuarios` VALUES ('Pablo',1),('Pablo',2),('Juanjo',3),('Pablo',3),('Juanjo',4),('Juanjo',5),('Cesar',7),('Cesar',8),('Pablo',9),('Pablo',10),('Juanjo',11),('Cesar',13),('Cesar',14),('Cesar',15);
/*!40000 ALTER TABLE `alergias_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredientes`
--

DROP TABLE IF EXISTS `ingredientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ingredientes` (
  `nombre` varchar(150) NOT NULL,
  `calorias` smallint unsigned NOT NULL COMMENT 'Calorías del ingrediente expresadas en kcal por 100g o 100ml de producto.',
  `tipo` tinyint(1) NOT NULL COMMENT 'Sólido (0) o líquido (1). Este campo nos sirve para expresar las cantidades en g o ml.',
  `id_ingrediente` int unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_ingrediente`),
  UNIQUE KEY `ingredientes_nombre_UN` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='Tabla que contiene los ingredientes de las recetas. ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredientes`
--

LOCK TABLES `ingredientes` WRITE;
/*!40000 ALTER TABLE `ingredientes` DISABLE KEYS */;
INSERT INTO `ingredientes` VALUES ('Huevos de gallina',155,1,1),('Harina de trigo',364,0,2),('Pan',308,0,3),('Aceite de oliva',884,1,4),('Sal',0,0,5),('Levadura de panaderia',105,0,6),('Mantequilla',717,0,7),('Leche de vaquita',42,1,8),('Azúcar',387,0,9),('Agua',0,1,10),('Solomillo de ternera',182,0,11),('Filete de ternera',189,0,12),('Lomo de ternera',117,0,13),('Hamburguesas de ternera',247,0,14),('Carne picada de ternera',241,0,15),('Alitas de pollo',266,0,16),('Chuletas de pavo',189,0,17),('Pechuga de pavo',135,0,18),('Pechuga de pollo',172,0,19),('Muslo de pavo',208,0,20),('Muslo de pollo',174,0,21),('Calabaza',14,0,22),('Cacahuetes',567,0,23),('Castañas',210,0,24),('Garbanzos',210,0,25),('Judias blancas',336,0,26),('Judias pintas',347,0,27),('Calabazin',17,0,28),('Puerro',61,0,29),('Zanahoria',41,0,30),('Cebolla',40,0,31),('Patata',77,0,32),('Ajo',149,0,33),('Judias verdes',31,0,34),('Vino blanco',82,1,35),('Oregano',308,0,36),('Almendras peladas',90,0,37),('caldo de pollo',36,0,38),('Salsa de soja',53,0,39),('Harina de maiz',370,0,40),('Aceite de girasol',884,1,41),('Arroz',130,0,42),('Gambas',84,0,43),('Guisantes',81,0,44),('Jamon Cocido',108,0,45),('Tomates',18,0,46),('Galletas María',436,0,47),('Queso crema (tipo Philadelphia)',83,0,48),('Nata para cocinar',163,1,49),('Queso fresco',46,0,50),('Queso parmesano',431,0,51),('Piñones',673,0,52),('Albahaca',22,0,53),('Perejil',36,0,54),('Espaguetis',158,0,55),('Limon',29,0,56);
/*!40000 ALTER TABLE `ingredientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredientes_recetas`
--

DROP TABLE IF EXISTS `ingredientes_recetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ingredientes_recetas` (
  `id_receta` int unsigned NOT NULL,
  `id_ingrediente` int unsigned NOT NULL,
  `cantidad` smallint unsigned NOT NULL,
  PRIMARY KEY (`id_receta`,`id_ingrediente`),
  KEY `ingredientes_recetas_FK_1` (`id_ingrediente`),
  CONSTRAINT `ingredientes_recetas_FK` FOREIGN KEY (`id_receta`) REFERENCES `recetas` (`id_receta`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `ingredientes_recetas_FK_1` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingredientes` (`id_ingrediente`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla guarda los ingredientes de cada receta.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredientes_recetas`
--

LOCK TABLES `ingredientes_recetas` WRITE;
/*!40000 ALTER TABLE `ingredientes_recetas` DISABLE KEYS */;
INSERT INTO `ingredientes_recetas` VALUES (1,1,1),(1,2,30),(1,6,1),(1,7,30),(1,9,30),(2,2,200),(2,5,2),(2,6,1),(2,10,50),(3,1,3),(3,2,200),(3,5,3),(3,6,10),(3,7,140),(3,8,3),(3,9,250),(4,1,2),(4,2,1),(4,3,150),(4,4,1),(4,5,1),(4,8,50),(4,15,750),(4,30,2),(4,31,1),(4,32,400),(4,33,2),(5,1,15),(5,4,1),(5,5,1),(5,31,1),(5,32,500),(6,1,3),(6,2,120),(6,4,150),(6,5,10),(6,7,25),(6,9,15),(6,19,130),(6,33,15),(7,2,150),(7,4,10),(7,5,5),(7,19,100),(7,30,40),(7,31,30),(7,33,2),(7,35,150),(8,1,3),(8,30,250),(8,42,200),(8,43,500),(8,44,3),(8,45,80),(9,8,10),(9,18,3),(9,28,1),(9,30,200),(9,31,2),(9,37,100),(9,38,250),(9,39,100),(9,40,30),(9,41,50),(10,4,1),(10,5,1),(10,10,1000),(10,13,100),(10,26,100),(10,30,2),(10,31,1),(10,32,2),(10,33,1),(11,4,1),(11,5,1),(11,10,10),(11,11,300),(11,31,10),(11,33,1),(11,35,500),(12,3,35),(12,4,30),(12,5,1),(12,33,1),(12,46,150),(13,1,8),(13,7,120),(13,9,300),(13,47,200),(13,49,800),(13,50,250),(14,4,160),(14,5,3),(14,33,10),(14,51,200),(14,52,75),(14,53,100),(15,4,50),(15,5,3),(15,33,2),(15,54,3),(15,55,1),(15,56,250);
/*!40000 ALTER TABLE `ingredientes_recetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listas_recetas`
--

DROP TABLE IF EXISTS `listas_recetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `listas_recetas` (
  `id_receta` int unsigned NOT NULL,
  `titulo_lista` varchar(150) NOT NULL,
  `nick` varchar(25) NOT NULL,
  PRIMARY KEY (`id_receta`,`titulo_lista`,`nick`),
  KEY `listas_recetas_FK` (`titulo_lista`,`nick`),
  CONSTRAINT `listas_recetas_FK` FOREIGN KEY (`titulo_lista`, `nick`) REFERENCES `listas_usuarios` (`titulo`, `nick`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `listas_recetas_FK_1` FOREIGN KEY (`id_receta`) REFERENCES `recetas` (`id_receta`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listas_recetas`
--

LOCK TABLES `listas_recetas` WRITE;
/*!40000 ALTER TABLE `listas_recetas` DISABLE KEYS */;
INSERT INTO `listas_recetas` VALUES (5,'favoritas','Pablo'),(7,'favoritas','Pablo'),(8,'favoritas','Pablo'),(11,'favoritas','Pablo'),(1,'Postres','Pablo'),(3,'Postres','Pablo'),(13,'Postres','Pablo'),(1,'Recetas francesas','Pablo'),(2,'Recetas francesas','Pablo'),(3,'Recetas francesas','Pablo');
/*!40000 ALTER TABLE `listas_recetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listas_usuarios`
--

DROP TABLE IF EXISTS `listas_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `listas_usuarios` (
  `titulo` varchar(150) NOT NULL,
  `nick` varchar(25) NOT NULL,
  PRIMARY KEY (`titulo`,`nick`),
  KEY `listas_usuarios_FK` (`nick`),
  CONSTRAINT `listas_usuarios_FK` FOREIGN KEY (`nick`) REFERENCES `usuarios` (`nick`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listas_usuarios`
--

LOCK TABLES `listas_usuarios` WRITE;
/*!40000 ALTER TABLE `listas_usuarios` DISABLE KEYS */;
INSERT INTO `listas_usuarios` VALUES ('favoritas','Adri'),('favoritas','Cesar'),('favoritas','Juanjo'),('favoritas','Pablo'),('Postres','Pablo'),('Recetas francesas','Pablo');
/*!40000 ALTER TABLE `listas_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paises`
--

DROP TABLE IF EXISTS `paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paises` (
  `cod_pais` varchar(10) NOT NULL COMMENT 'Código breve descriptivo del país de origen del usuario.',
  `nombre_pais` varchar(50) DEFAULT NULL,
  `medidas` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Sistema Internacional (0) o Imperial (1).',
  PRIMARY KEY (`cod_pais`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Países de origen de los usuarios.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paises`
--

LOCK TABLES `paises` WRITE;
/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
INSERT INTO `paises` VALUES ('AD','Andorra',0),('AE','United Arab Emirates',0),('AF','Afghanistan',0),('AG','Antigua and Barbuda',0),('AI','Anguilla',0),('AL','Albania',0),('AM','Armenia',0),('AN','Antillas Holandesas',0),('AO','Angola',0),('AP','Asia/Pacific Region',0),('AQ','Antarctica',0),('AR','Argentina',0),('AS','American Samoa',0),('AT','Austria',0),('AU','Australia',0),('AW','Aruba',0),('AX','Aland Islands',0),('AZ','Azerbaijan',0),('BA','Bosnia and Herzegovina',0),('BB','Barbados',0),('BD','Bangladesh',0),('BE','Belgium',0),('BF','Burkina Faso',0),('BG','Bulgaria',0),('BH','Bahrain',0),('BI','Burundi',0),('BJ','Benin',0),('BL','Saint Barthelemy',0),('BM','Bermuda',0),('BN','Brunei Darussalam',0),('BO','Bolivia',0),('BQ','Bonair',0),('BR','Brazil',0),('BS','Bahamas',0),('BT','Bhutan',0),('BW','Botswana',0),('BY','Belarus',0),('BZ','Belize',0),('CA','Canada',0),('CAN','Canadá',0),('CC','Cocos (Keeling) Islands',0),('CD','Cong',0),('CF','Central African Republic',0),('CG','Congo',0),('CH','Switzerland',0),('CI','Cote D\'Ivoire',0),('CK','Cook Islands',0),('CL','Chile',0),('CM','Cameroon',0),('CN','China',0),('CO','Colombia',0),('CR','Costa Rica',0),('CU','Cuba',0),('CV','Cape Verde',0),('CW','Curacao',0),('CX','Christmas Island',0),('CY','Cyprus',0),('CZ','Czech Republic',0),('DE','Germany',0),('DJ','Djibouti',0),('DK','Denmark',0),('DM','Dominica',0),('DO','Dominican Republic',0),('DZ','Algeria',0),('EC','Ecuador',0),('EE','Estonia',0),('EG','Egypt',0),('EH','Western Sahara',0),('ER','Eritrea',0),('ES','España',0),('ET','Ethiopia',0),('EU','Europe',0),('FI','Finland',0),('FJ','Fiji',0),('FK','Falkland Islands (Malvinas)',0),('FM','Micronesi',0),('FO','Faroe Islands',0),('FR','France',0),('GA','Gabon',0),('GB','United Kingdom',1),('GD','Grenada',0),('GE','Georgia',0),('GF','French Guiana',0),('GG','Guernsey',0),('GH','Ghana',0),('GI','Gibraltar',0),('GL','Greenland',0),('GM','Gambia',0),('GN','Guinea',0),('GP','Guadeloupe',0),('GQ','Equatorial Guinea',0),('GR','Greece',0),('GS','South Georgia and the South Sandwich Islands',0),('GT','Guatemala',0),('GU','Guam',0),('GW','Guinea-Bissau',0),('GY','Guyana',0),('HK','Hong Kong',0),('HN','Honduras',0),('HR','Croatia',0),('HT','Haiti',0),('HU','Hungary',0),('ID','Indonesia',0),('IE','Ireland',0),('IL','Israel',0),('IM','Isle of Man',0),('IN','India',0),('IO','British Indian Ocean Territory',0),('IQ','Iraq',0),('IR','Ira',0),('IS','Iceland',0),('IT','Italy',0),('JE','Jersey',0),('JM','Jamaica',0),('JO','Jordan',0),('JP','Japan',0),('KE','Kenya',0),('KG','Kyrgyzstan',0),('KH','Cambodia',0),('KI','Kiribati',0),('KM','Comoros',0),('KN','Saint Kitts and Nevis',0),('KP','Kore',0),('KR','Kore',0),('KW','Kuwait',0),('KY','Cayman Islands',0),('KZ','Kazakhstan',0),('LA','Lao People\'s Democratic Republic',0),('LB','Lebanon',0),('LC','Saint Lucia',0),('LI','Liechtenstein',0),('LK','Sri Lanka',0),('LR','Liberia',0),('LS','Lesotho',0),('LT','Lithuania',0),('LU','Luxembourg',0),('LV','Latvia',0),('LY','Libya',0),('MA','Morocco',0),('MC','Monaco',0),('MD','Moldov',0),('ME','Montenegro',0),('MF','Saint Martin',0),('MG','Madagascar',0),('MH','Marshall Islands',0),('MK','Macedonia',0),('ML','Mali',0),('MM','Myanmar',0),('MN','Mongolia',0),('MO','Macau',0),('MP','Northern Mariana Islands',0),('MQ','Martinique',0),('MR','Mauritania',0),('MS','Montserrat',0),('MT','Malta',0),('MU','Mauritius',0),('MV','Maldives',0),('MW','Malawi',0),('MX','Mexico',0),('MY','Malaysia',0),('MZ','Mozambique',0),('NA','Namibia',0),('NC','New Caledonia',0),('NE','Niger',0),('NF','Norfolk Island',0),('NG','Nigeria',0),('NI','Nicaragua',0),('NL','Netherlands',0),('NO','Norway',0),('NP','Nepal',0),('NR','Nauru',0),('NU','Niue',0),('NZ','New Zealand',0),('OM','Oman',0),('PA','Panama',0),('PE','Peru',0),('PF','French Polynesia',0),('PG','Papua New Guinea',0),('PH','Philippines',0),('PK','Pakistan',0),('PL','Poland',0),('PM','Saint Pierre and Miquelon',0),('PN','Pitcairn Islands',0),('PR','Puerto Rico',0),('PS','Palestinian Territory',0),('PT','Portugal',0),('PW','Palau',0),('PY','Paraguay',0),('QA','Qatar',0),('RE','Reunion',0),('RO','Romania',0),('RS','Serbia',0),('RU','Russian Federation',0),('RW','Rwanda',0),('SA','Saudi Arabia',0),('SB','Solomon Islands',0),('SC','Seychelles',0),('SD','Sudan',0),('SE','Sweden',0),('SG','Singapore',0),('SH','Saint Helena',0),('SI','Slovenia',0),('SJ','Svalbard and Jan Mayen',0),('SK','Slovakia',0),('SL','Sierra Leone',0),('SM','San Marino',0),('SN','Senegal',0),('SO','Somalia',0),('SR','Suriname',0),('SS','South Sudan',0),('ST','Sao Tome and Principe',0),('SV','El Salvador',0),('SX','Sint Maarten (Dutch part)',0),('SY','Syrian Arab Republic',0),('SZ','Swaziland',0),('TC','Turks and Caicos Islands',0),('TD','Chad',0),('TF','French Southern Territories',0),('TG','Togo',0),('TH','Thailand',0),('TJ','Tajikistan',0),('TK','Tokelau',0),('TL','Timor-Leste',0),('TM','Turkmenistan',0),('TN','Tunisia',0),('TO','Tonga',0),('TR','Turkey',0),('TT','Trinidad and Tobago',0),('TV','Tuvalu',0),('TW','Taiwan',0),('TZ','Tanzani',0),('UA','Ukraine',0),('UG','Uganda',0),('UM','United States Minor Outlying Islands',0),('US','United States',1),('UY','Uruguay',0),('UZ','Uzbekistan',0),('VA','Holy See (Vatican City State)',0),('VC','Saint Vincent and the Grenadines',0),('VE','Venezuela',0),('VG','Virgin Island',0),('VI','Virgin Island',0),('VN','Vietnam',0),('VU','Vanuatu',0),('WF','Wallis and Futuna',0),('WS','Samoa',0),('YE','Yemen',0),('YT','Mayotte',0),('ZA','South Africa',0),('ZM','Zambia',0),('ZW','Zimbabwe',0);
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planificaciones`
--

DROP TABLE IF EXISTS `planificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `planificaciones` (
  `nick` varchar(25) NOT NULL,
  `id_receta` int unsigned NOT NULL,
  `fecha_hora` datetime NOT NULL,
  PRIMARY KEY (`nick`,`id_receta`,`fecha_hora`),
  KEY `planificaciones_FK` (`id_receta`),
  CONSTRAINT `planificaciones_FK` FOREIGN KEY (`id_receta`) REFERENCES `recetas` (`id_receta`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `planificaciones_FK_1` FOREIGN KEY (`nick`) REFERENCES `usuarios` (`nick`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='En esta tabla se guardan las recetas que un usuairo ha planificado.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planificaciones`
--

LOCK TABLES `planificaciones` WRITE;
/*!40000 ALTER TABLE `planificaciones` DISABLE KEYS */;
INSERT INTO `planificaciones` VALUES ('Pablo',1,'2020-06-19 22:00:00'),('Pablo',1,'2020-06-19 22:15:00'),('Pablo',2,'2020-06-19 21:30:00'),('Pablo',5,'2020-06-20 12:00:00'),('Pablo',9,'2020-06-11 17:41:00'),('Pablo',13,'2020-06-17 08:40:00');
/*!40000 ALTER TABLE `planificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recetas`
--

DROP TABLE IF EXISTS `recetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recetas` (
  `id_receta` int unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `cultura` varchar(100) DEFAULT NULL COMMENT 'Tipo de cultura culinaria al que pertenece la receta, por ejemplo cocina italiana, mediterránea, hindú, etc.',
  `dificultad` varchar(150) DEFAULT NULL COMMENT 'Vamos a establecer una escala de valores para clasificar la dificultad de las recetas, por ejemplo: principiante, me manejo un poco en la cocina, masterchef...',
  `utensilios` json DEFAULT NULL COMMENT 'El usuairo describirá una lista de utensilios útiles para la elaboración de la receta.',
  `t_preparacion` smallint unsigned NOT NULL COMMENT 'Tiempo estimado de preparación de la receta expresado en minutos.',
  `pasos` json NOT NULL COMMENT 'Descripción detallada de la preparación de la receta.',
  `fecha_receta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nick` varchar(25) NOT NULL COMMENT 'Establece el usuario que ha creado la receta.',
  `calorias` int DEFAULT NULL COMMENT 'Calorías totales de la receta calculadas a partir de los ingredientes y sus cantidades, representan las calorías de la ración calculada para cada persona.',
  `descripcion` varchar(250) DEFAULT NULL COMMENT 'Descripción breve de la receta',
  PRIMARY KEY (`id_receta`),
  KEY `recetas_FK` (`nick`),
  CONSTRAINT `recetas_FK` FOREIGN KEY (`nick`) REFERENCES `usuarios` (`nick`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Recetas que van a crear los usuarios.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recetas`
--

LOCK TABLES `recetas` WRITE;
/*!40000 ALTER TABLE `recetas` DISABLE KEYS */;
INSERT INTO `recetas` VALUES (1,'Magdalenas','Francesa/Española','Cocinillas','[\"Bol\", \"Batidora\", \"Molde\"]',25,'[\"En un recipiente hondo introducimos el azúcar, los huevos y la mantequilla en dados y blandita.\", \"Batimos con unas varillas eléctricas hasta obtener una masa uniforme, blanquecina y aireada.\", \"Habrá aumentado de volumen por el aire incorporado con el batido.\", \"Podemos añadir la mantequilla fundida, que es mucho más fácil de incorporar al resto de ingredientes. Lo dejamos a vuestra elección.\", \"A continuación añadimos la harina y la levadura química, pasando ambas por un tamiz.\", \"Removemos con una lengua o espátula con movimientos envolventes, de abajo hacia arriba, suavemente para no perder el aire que hemos incorporado con el batido de la masa.\", \"En caso de usar mantequilla sin sal, añadimos un pellizco de sal a la masa en el momento en que incorporamos la harina. Esta potenciará el sabor de las magdalenas.\", \"Cubrimos el recipiente con la masa con papel film y lo introducimos en la nevera.\"]','2020-06-09 00:52:26','Pablo',443,'Este dulce francés es adictivo y tiene un tamaño peligroso. Se come en dos bocados y es tan sencillo comer una tras otra sin darse cuenta que lo mismo vais a echar mano de una más y os encontráis con que el plato está vacío.'),(2,'Baguetes francesas','Francesa/Española','Noob','[\"Bol\", \"Molde\", \"Rodillo\"]',95,'[\"En un bol mezclamos todos los ingredientes del prefermento. Mezclamos bien todos los ingredientes hasta conseguir una bola de masa uniforme.\", \"Ponemos la masa en un bol y la tapamos con un paño húmedo. Dejamos que fermente durante no menos de 3 horas en un lugar templado.\", \"En un cuenco añadimos el resto de la harina, la sal y el agua. Mezclamos bien y amasamos durante 5 minutos, hasta conseguir una bola lisa.\", \"Tapamos la masa con un paño húmedo y dejamos que la masa repose durante 15 minutos antes de continuar con el proceso.\", \"Incorporamos el prefermento y la levadura. Amasamos durante 15 ó 20 minutos y dejamos que la masa repose de nuevo, en lugar cálido, tapada con un paño húmedo, durante 30 minutos.\", \"Una vez fermentada la masa, la volcamos sobre una superficie de trabajo previamente enharinada. Dividimos la masa en las porciones que queramos. En nuestro caso, la cantidad de masa es la idónea para 4 baguettes pequeñas.\", \"Boleamos cada una de las porciones para darle una forma redonda y las dejamos reposar de nuevo, tapadas con un paño húmedo en lugar cálido, durante 30 minutos más.\", \"Estiramos cada una de las porciones para darle una forma rectangular.\", \"Doblamos la masa hacia el centro tanto en su parte superior como inferior y esta sobre sí misma. Sellamos bien el borde y lo colocamos hacia abajo.\"]','2020-06-09 00:58:00','Pablo',729,'Dentro de la cocina francesa, el pan tiene un sitio reservado. Existe en especial un tipo de pan francés, que ha traspasado fronteras y que es representante de su país de origen en el resto del mundo. Estoy hablando de las baguettes francesas.'),(3,'Tarta de manzana','Francesa/Española','Noob','[\"Bol\", \"Molde\"]',95,'[\"En un bol mezclamos la harina tamizada, el gasificante, una cucharada de azúcar avainillado, la leche, 150 g. de azúcar, 2 huevos, 60 g. de la mantequilla derretida y una pizca de sal. Mezclamos bien con unas varillas hasta conseguir una crema hiomogénea. Reservamos.\", \"Pelamos las manzanas y les retiramos el corazón. Las cortamos en láminas y las añadimos a la mezcla que tenemos reservada.\", \"Mezclamos bien y vertemos la mezcla en un molde desmoldable de unos 18 o 20 cm. de diámetro.\", \"Debemos engrasar y enharinar el molde antes de verter la mezcla. También podemos engrasarlo y forrarlo con papel de horno o simplemente utilizar un molde de silicona que nos evitará ambos procesos.\"]','2020-06-09 01:03:16','Pablo',10862,'La tarta de manzana es un ejemplo de que, por un lado los postres con manzana están entre mis favoritos y, por otro lado, la manzana es uno de los ingredientes top de la repostería.'),(4,'Albóndigas en salsa','Española','Principiante','[\"Bol\", \"Pelador de patatas\", \"Cuenco\", \"Cuchillo\"]',45,'[\"Preparamos la salsa. Picamos la cebolla, prepara el ajo y la zanahoria, y añade un chorro de aceite, y pon a pochar el ajo y la cebolla\", \"Añadimos una cucharada pequeña de harina a la cazuela \", \"Dejamos que los ingredientes suelten todo el líquido durante unos 5 minutos\", \"Haz pelotas de carne y salpimentamos la carne\", \"Añadimos  2 huevos junto el pan sin la corteza\", \"Removemos todo bien con las manos\", \"Pasa las albondigas por la harina del plato, la boleas un poco más\", \"Añadimos las albóndigas a la cazuela, y  dejamos al fuego 10 minutos\", \"Dejamos que reposen unos 5 minutos mientras freímos las patata y... ¡A disfrutar!\"]','2020-06-09 12:48:55','Adri',2618,'Si no queréis acompañarlas con patatas un buen puré de patatas es una opción. Otra alternativa es hacer un arroz blanco como guarnición, el arroz blanco es perfecto para mezclar con la salsa de las albóndigas.'),(5,'Tortilla','Española','Principiante','[\"Pelador de patatas\", \"Cuenco\", \"Cuchillo\"]',30,'[\"cortar y freír las patatas y la cebolla\", \"mezclar con los huevos\", \"cuajar la tortilla\", \"Ponemos en la sartén un par de cucharadas de aceite\", \"Ponemos de nuevo la sartén en el fuego. Espera 10 min para que se termine de hacer y ya estaría!\"]','2020-06-09 13:08:55','Adri',417,'Para que la tortilla esté jugosa es importante que las patatas se hagan bien y se confiten, porque no hay nada peor que una tortilla con las patatas medio crudas.'),(6,'Pollo a la parmesana','Italiana','Noob','[\"Olla\", \"Cacerola\", \"Licuadora\"]',65,'[\"Llevar a fuego moderado una olla y añadir el aceite y a continuación la cebolla, remueve unos 3 minutos y añade el ajo, sigue revolviendo por dos minutos para agregar luego los tomates, el azúcar, la sal y 1/3  de taza de agua. Deja que se cocine unos 20 minutos  y revuelve de vez en cuando. A continuación añade la mantequilla y revuelve para integrar. Retira y vierte en la licuadora para mezclar todo (cuidado si está caliente).\", \"Precalienta el horno a 375 °. Mientras mezcla el pan rallado con ¼ de queso parmesano y a continuación sal pimentar el pollo; una vez listo tendrás que pasar de una a una las piezas para la harina, luego por el  huevo y finalmente  por el pan rallado mezclado con el queso. Cuando lo tengas, coloca a calentar 3 cucharadas de  aceite en una sartén y coloca cada pieza para dorar de ambos lados.\", \"Transfiere a una fuente de horno y cubre  con  salsa y mozzarella y  espolvorea con lo que ha quedado del queso parmesano. Cocina aproximadamente durante 15 minutos o hasta que el pollo este cocido.\"]','2020-06-09 14:57:24','Cesar',2255,'Receta italiana muy típica y además facilísima de preparar'),(7,'Pollo marinado con vino blanco','Italiana','Principiante','[\"Cacerola\"]',770,'[\"Calentar el aceite en una cacerola a fuego alto, sazonar el pollo con sal y pimienta y espolvorear en la harina. Cocinar el pollo hasta que se dore en la cacerola por 10 minutos aproximadamente. Luego, transferir el pollo a un plato y reservar.\", \"Seguidamente, agregar el romero, el ajo, el laurel, la zanahoria, el pimiento, la cebolla y el apio a la sartén y cocinar hasta que estén bien dorados por 6 u 8 minutos.\", \"Finalmente, añadir el vino a la mezcla y cocinar revolviendo y raspando pedacitos dorados del fondo de la sartén, hasta que se reduzca a la mitad el líquido, y transcurridos unos 3 minutos regresar el pollo al sartén junto a los tomates por 30 minutos más. Destapar y agregar el perejil.\", \"Servir con generosa salsa y combinar con alguna ensalada de vegetales, puré u arroz.\"]','2020-06-09 14:42:03','Cesar',3843,'Combinar pollo y verduras en una salsa de tomate nunca había sido tan delicioso y fácil como en esta receta tradicional italiana.'),(8,'Arroz tres delicias','Asiatica','Noob','[\"Plato hondo\", \"Cuchillo\", \"Cazo\"]',25,'[\"Cocemos los langostinos durante no más de 2 minutos. Escurrimos, pelamos y reservamos las colas.Podemos emplear langostinos frescos o congelados. Hoy en día en el mercado podemos encontrar productos congelados de gran calidad.\", \"Cocemos los guisantes y las zanahorias cortadas en daditos en agua ligeramente salada. Cocinamos la verdura durante 10-12 minutos, escurrimos y reservamos.Cortamos el jamón cocido en tiras y lo doramos en una sartén con un hilo de aceite. Reservamos.\", \"En una cazuela calentamos unos 600 ml. de agua con sal. Cuando el agua comience a hervir incorporamos el arroz.\", \"Cocinamos el arroz, a fuego medio, durante 18 minutos. Nos interesa que el arroz se quede con un punto de dureza.\", \"Una vez cocido lavamos el arroz para parar el seco la cocción y dejarlo suelto, escurrimos bien.\"]','2020-06-07 22:52:26','Pablo',290,'Este arroz frito con variedad de ingredientes tiene un sabor exquisito que ademas de sirve para acompañar con varias salsas'),(9,'Pollo con almendras','Asiatica','Cocinillas','[\"Sarten\", \"Cuenco\", \"Cuchillo\"]',40,'[\"Corta el pollo en dados, del tamaño de un bocado. Ponlos en un bol y añade la salsa de soja, el azúcar y el jengibre en polvo. Mézclalo todo bien, para que el pollo se macere y aromatice. Tapa el bol e introdúcelo en la nevera, al menos durante media hora, mientras preparas el resto de la receta.\", \"En una sartén a fuego medio-fuerte, pon un poco de aceite y saltea las almendras, hasta que se doren ligeramente. Resérvalas.\", \"Corta la cebolla en tiras gruesas, y haz lo mismo con la zanahoria. Saltéalas en la misma sartén en la que has dorado las almendras, con el fuego al medio, y añade un poquito de sal. Deja que se hagan durante 5-7 minutos, y reserva.\", \"Corta el calabacín en pequeños dados y saltéalo en la sartén, hasta que se dore ligeramente. Reserva junto con el resto de verduras y las almendras.\", \"Saca el pollo de la nevera y échalo en la sartén junto con la salsa de macerado. Saltéalo durante 5 minutos, que no llegue a dorarse.\"]','2020-06-07 22:52:26','Cesar',610,'Sirve el pollo con almendras bien caliente, en un bol al centro de la mesa o incluso repartido en los platos de los comensales. La salsa desprenderá un olor increíble y tanto las verduras como el pollo estarán jugosos.'),(10,'Judias madrileñas','Española','Principiante','[\"olla\", \"cuchillos\"]',60,'[\"el dia anterior poner las judias en remojo\", \"pelar y picar todo\", \"añadir todo a la olla junto a la sal y al ajo con abundante agua,y poner a cocer\", \"cuando se ponga a hervir dejarlo a fuego medio 45 min mas\", \"disfrutar\"]','2020-06-09 15:29:14','Adri',466,'Plato tipico madrileño, perfecto para dias frios de invierno'),(11,'Solomillos con vino','Española','Noob','[\"cuchillos\", \"sarten\"]',60,'[\"cortar los solomillos en medallones y sazonarlos(ponerles sal y ajo)\", \"poner la sarten a calentar con un poco de aceite\", \"cuando este caliente poner a dorar la cebolla\", \"añadir los medallones y cuando cojan color añadir el vino blanco y un poco de agua\", \"dejar hervir a fuego lento durante 20 min o hasta que se reduzca el vino y servir\"]','2020-06-09 15:46:44','Adri',970,'Plato sencillo y delicioso, que no nos permite impresionar a nuestras visitas'),(12,'Salmorejo Cordobés','Española','Noob','[\"Cuchillo\", \"Batidora\", \"Bol\"]',15,'[\"Limpiamos bien los tomates y los cortamos en cuartos, los añadimos a un bol grande y trituramos con la batidora hasta que nos quede una salsa líquida.\", \"Cortamos el pan en trozos pequeños y los añadimos a la crema de tomate anterior. Los dejamos reposar durante unos 10 minutos.\", \"Pelamos el ajo y, como lo vamos a usar en crudo, le quitamos el centro para que no repita. Lo añadimos al tomate con el pan.\", \"Echamos el aceite de oliva virgen extra y la sal.\", \"Lo pasamos todo por la batidora hasta que nos quede lo más fino posible. Probamos si esta bien de sal y si no rectificamos con un poco más.\", \"Sólo nos queda meter el bol en la nevera y dejar que se enfríe. En un par de horas lo tendréis bien fresquito y perfecto para probar el mejor salmorejo cordobés.\", \"A la hora de la presentación, lo mejor es ponerlo en un plato hondo o cuenco.\", \"Espolvoreamos con unas virutas de jamón serrano o ibérico con un punto (10 segundos) de microondas. Así conseguiréis un toque crujiente.\", \"Además el huevo cocido y unas gotitas de aove le quedan perfecto.\"]','2020-06-09 17:59:00','Juanjo',491,'Os presento una de mis recetas de tapas, aperitivos y pinchos preferidos durante todo el año, en especial para refrescarnos en los calores del verano, el salmorejo.'),(13,'Tarta de queso al horno','Italiana','Principiante','[\"Molde de tarta de 28 cm\", \"Horno\", \"Batidora\"]',60,'[\"En un vaso de batidora, trituramos las galletas maría.\", \"Seguidamente agregamos la mantequilla pomada o derretida y seguimos mezclando. Con esto haremos la pequeña base de galleta de la tarta.\", \"Colocamos la mezcla en la base del molde.\", \"En el mismo vaso de batidora. Colocamos los huevos, la nata, el azúcar y los quesos. trituramos bien. Podemos hacerlo en thermomix o también a mano. Como mejor os venga o mejores utensilios tengáis.\", \"Precalentamos el horno a 180º e introducimos durante 49 minutos con el calor arriba abajo. No os asustéis si cuando la saquéis está totalmente líquida. Dejamos enfriar la tarta y la introducimos en la nevera unas horas para que se enfrie. Cuando la partais cuidado porque la parte interior debe quedar cremosa y así estará si seguís la receta al pie de la letra.\", \"(Opcional) Lo podéis presentar con una bola de helado de vainilla que le va genial.\"]','2020-06-09 18:15:09','Juanjo',8649,'La tarta de queso al horno es mi postre favorito con diferencia. Hay muchas recetas de tarta de queso, pero esta es una de las clásicas o tradicionales y no debe faltar en ningún recetario.'),(14,'Pesto','Italiana','Noob','[\"Batidora\"]',5,'[\"En el vaso de la batidora agregar todos los ingredientes\", \"Batir hasta que no se note ningun trozo notablemente grande de piñón.\"]','2020-06-10 03:04:16','Adri',384,'Una pasta que podrá acompañar a todas tus pastas, con una fragancia insuperable y un sabor que te dejará pasmado pese a su simplicidad.'),(15,'Pasta aglio e olio','Italiana','Noob','[\"Cuchillo\", \"Tabla\", \"Sarten\"]',15,'[\"Lo primero de todo vamos a poner a hervir la pasta\", \"Cortar en la tabla el ajo en finas láminas y picar el perejil (quitando antes los tallos y quedandonos con las ramas finas)\", \"Calentar la sarten con el aceite de oliva (es mucha cantidad, lo sé) y cuando este caliente, echar el ajo del paso anterior y freirlo hasta que quede doradito\", \"Cuando hayan pasado los 10 mins desde que pusimos a hervir la pasta, echaremos esta en la sarten con el ajo.\", \"Despues de freir unos segundos la pasta en la sarten, añadir el resto de ingredientes.\"]','2020-06-10 06:06:45','Pablo',491,'Una forma de preparar tu pasta que consigue que el resultado sea mejor que la suma de sus ingredientes y de una manera sencilla y poco costosa en tiempo.');
/*!40000 ALTER TABLE `recetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respuestas`
--

DROP TABLE IF EXISTS `respuestas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `respuestas` (
  `nick_valoracion` varchar(25) NOT NULL,
  `nick_respuesta` varchar(25) NOT NULL,
  `id_receta` int unsigned NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `respuesta` text,
  PRIMARY KEY (`nick_valoracion`,`nick_respuesta`,`id_receta`,`fecha_hora`),
  KEY `respuestas_FK` (`nick_valoracion`,`id_receta`),
  KEY `respuestas_FK_1` (`nick_respuesta`),
  CONSTRAINT `respuestas_FK` FOREIGN KEY (`nick_valoracion`, `id_receta`) REFERENCES `valoraciones` (`nick`, `id_receta`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `respuestas_FK_1` FOREIGN KEY (`nick_respuesta`) REFERENCES `usuarios` (`nick`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='En esta tabla vamos a guardar las respuestas a las valoraciones de los usuarios.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respuestas`
--

LOCK TABLES `respuestas` WRITE;
/*!40000 ALTER TABLE `respuestas` DISABLE KEYS */;
/*!40000 ALTER TABLE `respuestas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `nick` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'nick del usuario, será la clave primaria.',
  `password` varchar(25) NOT NULL COMMENT 'Contraseña del usuario, guardaremos su hash.',
  `email` varchar(100) NOT NULL COMMENT 'email del usuario, debe ser único.',
  `nombre` varchar(50) DEFAULT NULL COMMENT 'Nombre del usuario, sin apellidos.',
  `apellidos` varchar(60) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `cod_pais` varchar(10) DEFAULT NULL,
  `rol` varchar(10) DEFAULT NULL COMMENT 'Los usuarios por norma general tendrán el rol normal, pero puede haber usuarios con el rol admin.',
  PRIMARY KEY (`nick`),
  UNIQUE KEY `usuarios_email_UN` (`email`),
  KEY `cod_pais` (`cod_pais`),
  CONSTRAINT `cod_pais` FOREIGN KEY (`cod_pais`) REFERENCES `paises` (`cod_pais`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla que guardará los datos de los usuarios de la aplicación.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('Adri','adri','adri@helpfood.com','Adrian','Turiel','2000-02-19','SD','normal'),('Cesar','cesar','cesar@helpfood.com','Cesar','Godino','1997-04-04','DK','normal'),('Juanjo','juanjo','juanjo@helpfood.com','Juanjo','Plaza','1985-08-08','ES','normal'),('Pablo','pablo','pablo@helpfood.com','Pablo','Cidoncha','1997-11-27','PL','normal');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valoraciones`
--

DROP TABLE IF EXISTS `valoraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `valoraciones` (
  `nick` varchar(25) NOT NULL,
  `id_receta` int unsigned NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `comentario` text,
  `nota` tinyint unsigned NOT NULL COMMENT 'La valoración que hace el usuario de la receta en una escale de 1 a 5.',
  PRIMARY KEY (`nick`,`id_receta`),
  KEY `valoraciones_FK` (`id_receta`),
  CONSTRAINT `valoraciones_FK` FOREIGN KEY (`id_receta`) REFERENCES `recetas` (`id_receta`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `valoraciones_FK_1` FOREIGN KEY (`nick`) REFERENCES `usuarios` (`nick`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla contiene las valoraciones que hacen los usuarios de las recetas.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valoraciones`
--

LOCK TABLES `valoraciones` WRITE;
/*!40000 ALTER TABLE `valoraciones` DISABLE KEYS */;
INSERT INTO `valoraciones` VALUES ('Cesar',1,'2020-06-10 00:40:32','Igual de ricas que de calóricas jejejeje',4),('Cesar',5,'2020-06-10 00:37:30','No está mal, pero le falta un poco de magia',3),('Juanjo',5,'2020-06-10 00:44:57','Me la hago todas las mañanas, está increíble',5),('Juanjo',7,'2020-06-10 00:45:30','Coincido con Pablo, muy malas explicaciones, se me ha quemado el pollo',2),('Pablo',5,'2020-06-10 00:36:42','Me recuerda a la receta de mi abuela, muy buenas explicaciones',5),('Pablo',7,'2020-06-10 00:43:53','Ojalá saliera como en la foto, lamentables los detalles de los pasos',1),('Pablo',8,'2020-06-11 13:47:04','Una receta muy completa, casi perfecta',4),('Pablo',11,'2020-06-10 00:43:28','Sencillamente espectacular',5);
/*!40000 ALTER TABLE `valoraciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vista_calendario`
--

DROP TABLE IF EXISTS `vista_calendario`;
/*!50001 DROP VIEW IF EXISTS `vista_calendario`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vista_calendario` AS SELECT 
 1 AS `id_receta`,
 1 AS `titulo`,
 1 AS `nick`,
 1 AS `fecha_hora`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'helpfood'
--

--
-- Final view structure for view `vista_calendario`
--

/*!50001 DROP VIEW IF EXISTS `vista_calendario`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_calendario` AS select `r`.`id_receta` AS `id_receta`,`r`.`titulo` AS `titulo`,`p`.`nick` AS `nick`,`p`.`fecha_hora` AS `fecha_hora` from (`recetas` `r` join `planificaciones` `p` on((`r`.`id_receta` = `p`.`id_receta`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-11 13:53:52
