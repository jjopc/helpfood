package helpfood.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import helpfood.request.NewUsuarioRequest;
import helpfood.response.ListaUsuariosResponse;
import helpfood.response.UsuarioResponse;
import helpfood.service.UsuarioService;

@RestController
public class UsuarioRestController {
	@Autowired
	private UsuarioService service;

	@RequestMapping(path = "/usuarios", method = RequestMethod.GET)
	public ResponseEntity<ListaUsuariosResponse> getAllUsuarios() {
		return ResponseEntity.ok(service.getAllUsuarios());
	}

	@RequestMapping(path = "/usuarios/{nick}", method = RequestMethod.GET)
	public ResponseEntity<UsuarioResponse> getUsuario(@PathVariable String nick) {
		Optional<UsuarioResponse> response = service.getUsuario(nick);
		return response.isPresent() ? ResponseEntity.ok(response.get()) : ResponseEntity.notFound().build();
	}

	@RequestMapping(path = "/usuarios", method = RequestMethod.POST)
	public ResponseEntity<UsuarioResponse> createNewUsuario(@Valid @RequestBody NewUsuarioRequest usuarioRequest) {
		UsuarioResponse response = service.createUsuario(usuarioRequest);
		// ahora crea la nueva uri de este usuario /usuarios/nick_usuario
		URI uriCreated = UriComponentsBuilder.fromPath("/usuarios").buildAndExpand(usuarioRequest.getNick()).toUri();
		return ResponseEntity.created(uriCreated).body(response);
	}

	@RequestMapping(path = "/usuarios/editar", method = RequestMethod.PUT)
	public ResponseEntity<UsuarioResponse> editarUsuario(@Valid @RequestBody NewUsuarioRequest usuarioRequest) {
		 UsuarioResponse response = service.editarUsuario(usuarioRequest);
		 URI uriCreated = UriComponentsBuilder.fromPath("/usuarios").buildAndExpand(usuarioRequest.getNick()).toUri();
		 return ResponseEntity.created(uriCreated).body(response);
	}

	@RequestMapping(path = "/usuarios/delete/{nick}", method = RequestMethod.DELETE)
	public Optional<UsuarioResponse> borrarUsuario(@PathVariable String nick) {
		Optional<UsuarioResponse> response = service.borrarUsuario(nick);
		return response;
	}
}