package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.RecetaEntity;
import helpfood.response.RecetaResponse;

public class RecetaEntityToRecetaResponseConverter implements Converter<RecetaEntity, RecetaResponse> {
	@Override
	public RecetaResponse convert(RecetaEntity receta) {
		RecetaResponse recetaRes = new RecetaResponse();
		recetaRes.setTitulo(receta.getTitulo());
		recetaRes.setCultura(receta.getCultura());
		recetaRes.setDificultad(receta.getDificultad());
		recetaRes.setUtensilios(receta.getUtensilios());
		recetaRes.setNick(receta.getNick());
		recetaRes.setTiempoPreparacion(receta.getTiempoPreparacion());
		recetaRes.setPasos(receta.getPasos());
		//TODO Popularidad aún no está metido
		//recetaRes.setPopularidad(receta.getPopularidad());
		//TODO Calcular campo de calorías y meterlo
		recetaRes.setDescripcion(receta.getDescripcion());
		recetaRes.setId(receta.getId());
		recetaRes.setCaloriasTotales(receta.getCaloriasTotales());
		return recetaRes;
	}
}