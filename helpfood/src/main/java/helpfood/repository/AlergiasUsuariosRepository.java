package helpfood.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import helpfood.entity.AlergiasUsuariosEntity;

import java.util.List;


@Repository
public interface AlergiasUsuariosRepository extends CrudRepository<AlergiasUsuariosEntity, String>{
	
	public List<AlergiasUsuariosEntity> findAll();
	public List<AlergiasUsuariosEntity> findByNick(String nick);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM AlergiasUsuariosEntity e "
			+ "WHERE e.nick = :nick")
	void deleteAlergias(@Param("nick") String nick);
	
	//public List<IngRecetaEntity> findByidreceta(int id_receta);
	
	/*
	@Query("SELECT e "
			+ "FROM IngRecetaEntity e "
			+ "WHERE e.id_ingrediente = :idreceta AND e.id_ingrediente = :idingrediente")
	public List<IngRecetaEntity> findByIdRecetaAndIdIngrediente(@Param("idreceta") int idreceta, @Param("idingrediente") int idingrediente);
	
	@Query("SELECT e "
			+ "FROM IngRecetaEntity e "
			+ "WHERE e.idreceta = :idreceta")
	public List<IngRecetaEntity> findByIdReceta(@Param("idreceta") int idreceta);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM IngRecetaEntity e "
			+ "WHERE e.idreceta = :idreceta")
	void deleteIngReceta(@Param("idreceta") int idreceta);
	*/
	
}