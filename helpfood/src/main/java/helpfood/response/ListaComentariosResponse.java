package helpfood.response;

import java.util.List;

public class ListaComentariosResponse {
	private List<ComentarioResponse> ListaComentarios;

	public ListaComentariosResponse(List<ComentarioResponse> ListaComentarios) {
		this.ListaComentarios = ListaComentarios;
	}

	public ListaComentariosResponse() {
	}

	public List<ComentarioResponse> getListaComentarios() {
		return ListaComentarios;
	}

	public void setListaComentarios(List<ComentarioResponse> ListaComentarios) {
		this.ListaComentarios = ListaComentarios;
	}
}
