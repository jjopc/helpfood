$(document).ready(function() {

	var nick = getCookie("username");
	//getJsonEventos(nick);
	var calendar = $('#calendar').fullCalendar({

	    editable:false, //a true para arrastrar eventos y redimensionar el rango de tiempo que ocupan
	    header:{
	    	left:'prev,today,next', //para mostrar los botones de izq, der y hoy
	    	center:'title', //para mostrar el nombre del mes y año actual
	    	right:'month,agendaWeek,agendaDay' //para los botones del estilo de cuadricula a la derecha
		},
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    	monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
    	dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
		//allDayText: 'día entero',
		allDaySlot: false,
		//timeFormat: 'H:mm)',
		eventTimeFormat: 'H:mm',
	
		buttonText: {
			today:    'hoy',
			month:    'mes',
			week:     'semana',
			day:      'día'
		},
		views: {
			month: { // name of view
				titleFormat: 'MMMM [de] YYYY',
				timeFormat: 'H(:mm)',
				columnFormat: 'dddd'
				// other view-specific options here
			},
			week:{
				titleFormat: 'MMMM [de] YYYY',
				timeFormat: 'H(:mm)',
				columnFormat: 'ddd D',
				slotLabelFormat: 'H:mm'
			},
			day:{
				titleFormat: 'dddd D [de] MMMM',
				timeFormat: 'H(:mm)',
				slotLabelFormat: 'H:mm'
			}
		},
		firstDay: 1,
		navLinks: true,
		events: getJsonEventos(nick),
	    selectable:false,
	    selectHelper:false,
	    editable:false,
	    eventClick:function(event) {
	    	$("#quitar_receta").modal("show");
	    	$('#quitar_receta').on('shown.bs.modal', function() {
	    		$("#tituloReceta").text(event.title);
	    		$('#irReceta').on('click', function() {
	    			window.open("/receta_concreta.html?id=" + event.idReceta, "_self");
	    		});
	    		$('#eliminarReceta').on('click', function() {
					//TODO HACER QUE SE PUEDA ELIMINAR UNA RECETA
					$("#confirmacion_quitar_receta").modal("show");
					$('#confirmacion_quitar_receta').on('shown.bs.modal', function() {
						
						moment.locale('es', {
							months : 'enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre'.split('_'),
							weekdays : 'domingo_lunes_martes_miércoles_jueves_viernes_sábado'.split('_'),
							});
						var fecha = moment(event.start._i);
						var miUrl = "/planificaciones/delete/" + nick + "/" + event.idReceta + "/" + event.start._i; //pa debuguear

						$("#tituloRecetaABorrar").text(event.title + ", para el " + fecha.format('dddd DD [de] MMMM [a las] HH:mm')/*fecha.toLocaleDateString("es-ES", options)*/);
						$('#borrarSeguro').on('click', function() {
							//Quiere borrar definitivamente el evento
							$.ajax({
								
								url : miUrl,
								type : "DELETE",
								success : function() {
									
									calendar.fullCalendar('removeEvents');
									var eventos = getJsonEventos(nick);
									calendar.fullCalendar('addEventSource', eventos);
									calendar.fullCalendar('refetchEvents');
								}
							});
						});
						$('#noBorrar').on('click', function() {
							$('#confirmacion_quitar_receta').modal('hide');
							//No quiere borrar y se quita la ventanita
						});
					});
	    		});//
	    	});   	
	    }
	}); 

});

var jsonEventos;

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function getJsonEventos(nick) {
	$.ajax({
        url: "/eventos/" + nick,
		type: "GET",
		dataType: "json",
        async: false,
        success: function (data) {
			jsonEventos = data.listaEventos;
		}//,
		//error: function(XMLHttpRequest, textStatus, errorThrown) { 
		//	alert("Status: " + textStatus); alert("Error: " + errorThrown); 
		//} 
		
	});
	return jsonEventos;
}
