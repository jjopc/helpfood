package helpfood.entity;

import javax.persistence.*;

@Entity
@Table(name = "alergenos")
public class AlergenoEntity {
	
	@Id
	@Column(name="id_alergeno")
	private int id;
	@Column
	private String nombre;
	@Column
	private String observaciones;
	
	public AlergenoEntity() {}
	
	public AlergenoEntity(int id, String nombre, String observaciones) {
		this.id = id;
		this.nombre = nombre;
		this.observaciones = observaciones;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getObservaciones() {
		return this.observaciones;
	}
	
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
}