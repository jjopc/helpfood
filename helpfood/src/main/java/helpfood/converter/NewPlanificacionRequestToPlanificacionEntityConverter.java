package helpfood.converter;

import helpfood.entity.PlanificacionEntity;
import helpfood.request.NewPlanificacionRequest;

public class NewPlanificacionRequestToPlanificacionEntityConverter {

	public PlanificacionEntity convert(NewPlanificacionRequest planificacionRequest) {
		PlanificacionEntity planificacionEntity = new PlanificacionEntity();
		planificacionEntity.setnick(planificacionRequest.getnick());
		planificacionEntity.setid_receta(planificacionRequest.getid_receta());
		planificacionEntity.setfecha_hora(planificacionRequest.getfecha_hora());
		return planificacionEntity;
	}

}
