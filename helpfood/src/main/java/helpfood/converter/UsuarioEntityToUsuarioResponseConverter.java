package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.UsuarioEntity;
import helpfood.response.UsuarioResponse;

public class UsuarioEntityToUsuarioResponseConverter implements Converter<UsuarioEntity, UsuarioResponse> {
	@Override
	public UsuarioResponse convert(UsuarioEntity usuario) {
		UsuarioResponse usuarioRes = new UsuarioResponse();
		usuarioRes.setNick(usuario.getNick());
		usuarioRes.setPassword(usuario.getPassword());
		usuarioRes.setEmail(usuario.getEmail());
		usuarioRes.setNombre(usuario.getNombre());
		usuarioRes.setApellidos(usuario.getApellidos());
		usuarioRes.setFechaNacimiento(usuario.getFechaNacimiento());
		usuarioRes.setCodPais(usuario.getCodPais());
		usuarioRes.setRol(usuario.getRol());
		return usuarioRes;
	}
}