package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.ComentarioEntityToComentarioResponseConverter;
import helpfood.converter.NewComentarioRequestToComentarioEntityConverter;
import helpfood.entity.ComentarioEntity;
import helpfood.repository.ComentariosRepository;
import helpfood.request.NewComentarioRequest;
import helpfood.response.ComentarioResponse;
import helpfood.response.ListaComentariosResponse;

@Service
public class ComentarioServiceImpl implements ComentarioService{
	@Autowired
	private ComentariosRepository comentariosRepository;
	@PersistenceContext
    private EntityManager entityManager;
	
	private ComentarioEntityToComentarioResponseConverter entityToResponseConverter = new ComentarioEntityToComentarioResponseConverter();
	private NewComentarioRequestToComentarioEntityConverter requestToEntityConverter = new NewComentarioRequestToComentarioEntityConverter();

	public ListaComentariosResponse getComentariosFromReceta(int id_receta) {
		ListaComentariosResponse res = new ListaComentariosResponse();
		List<ComentarioEntity> comentarios = comentariosRepository.findByidreceta(id_receta);
		List<ComentarioResponse> responses = comentarios.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		res.setListaComentarios(responses);
		return res;
	}

	public ListaComentariosResponse getComentariosFromUsuario(String nick) {
		ListaComentariosResponse res = new ListaComentariosResponse();
		List<ComentarioEntity> comentarios = comentariosRepository.findBynickUsuario(nick);
		List<ComentarioResponse> responses = comentarios.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		res.setListaComentarios(responses);
		return res;
	}
	
	public ComentarioResponse createComentario(NewComentarioRequest comentarioRequest) {
		ComentarioResponse response;
		ComentarioEntity newEntity = requestToEntityConverter.convert(comentarioRequest);
		newEntity = comentariosRepository.save(newEntity);
		response = entityToResponseConverter.convert(newEntity);
		return response;
	}
	
	@Override
	public Optional<ComentarioResponse> borrarValoraciones(String nick){
		Optional<ComentarioResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		comentariosRepository.deleteValoraciones(nick);
		return response;
	}
	
	@Override
	public Optional<ComentarioResponse> borrarValoracionesReceta(int idreceta){
		Optional<ComentarioResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		comentariosRepository.deleteValoracionesReceta(idreceta);
		return response;
	}
}