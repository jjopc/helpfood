package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.RecetasListasDeRecetasDeUnUsuarioEntity;
import helpfood.request.NewRecetasListasDeRecetasDeUnUsuarioRequest;

public class NewRecetasListasDeUnUsuarioRequestToEntityConverter implements Converter<NewRecetasListasDeRecetasDeUnUsuarioRequest, RecetasListasDeRecetasDeUnUsuarioEntity>{

    @Override
    public RecetasListasDeRecetasDeUnUsuarioEntity convert(NewRecetasListasDeRecetasDeUnUsuarioRequest source) {
        RecetasListasDeRecetasDeUnUsuarioEntity entity = new RecetasListasDeRecetasDeUnUsuarioEntity();
        entity.setIdReceta(source.getId_receta());
        entity.setNick(source.getNick());
        entity.setTituloLista(source.getTitulo_lista());
        return entity;
    }
    
}