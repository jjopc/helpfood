package helpfood.entity;

public class PaisEntity {
	private int codigo;
	private String nombre;
	private String unidadMasa;
	private String unidadVolumen;
	
	public PaisEntity(int codigo, String nombre, String unidadMasa, String unidadVolumen) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.unidadMasa = unidadMasa;
		this.unidadVolumen = unidadVolumen;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUnidadMasa() {
		return unidadMasa;
	}

	public void setUnidadMasa(String unidadMasa) {
		this.unidadMasa = unidadMasa;
	}

	public String getUnidadVolumen() {
		return unidadVolumen;
	}

	public void setUnidadVolumen(String unidadVolumen) {
		this.unidadVolumen = unidadVolumen;
	}
	
}