package helpfood.service;
import java.util.List;
import java.util.Optional;

import helpfood.entity.RecetaEntity;
import helpfood.request.NewRecetaRequest;
import helpfood.response.ListaRecetasResponse;
import helpfood.response.RecetaResponse;
import helpfood.specifications.SearchCriteria;

//RecetaService nos proporciona cabeceras de metodos para obtener una receta por su id u obtener todas.
public interface RecetaService {
	//public Optional<RecetaEntity> getReceta(int id /*String titulo*/);
	public Optional<RecetaEntity> findById(int id);
	public ListaRecetasResponse findByCultura(String id);
	public Optional<RecetaEntity> findByFecha(String fecha);
	public Optional<RecetaEntity> findByTitulo(String id);
	public ListaRecetasResponse findByNick(String id);
	public ListaRecetasResponse getAllRecetas();
	List<RecetaEntity> searchReceta(List<SearchCriteria> params);
	public RecetaResponse createReceta(NewRecetaRequest recetaRequest);
	//public Optional<RecetaResponse> borrarRecetas(String nick);
	public Optional<RecetaResponse> borrarReceta(int idreceta);
	public ListaRecetasResponse getRecetasBuscadas(String titulo, String creador, int tiempoMinimo, int tiempoMaximo, String cultura, 
			int minCal, int maxCal, String dificultad);
}