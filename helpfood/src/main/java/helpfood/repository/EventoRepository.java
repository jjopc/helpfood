package helpfood.repository;

import org.springframework.stereotype.Repository;

import helpfood.entity.EventoEntity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

@Repository
public interface EventoRepository extends CrudRepository<EventoEntity, String>, JpaSpecificationExecutor<EventoEntity> {
	public List<EventoEntity> findByNick(String nick);
	public List<EventoEntity> findAll(); 
	public Optional<EventoEntity> findBystart(String start);
	
	//@Query("SELECT p FROM EventoEntity p WHERE LOWER(p.lastName) = LOWER(:lastName)")
	/*
	SELECT id_receta, titulo, nick, fecha_hora
	FROM helpfood.vista_calendario
	WHERE fecha_hora BETWEEN '2020-05-13' and CURRENT_TIMESTAMP()
	*/
	
	
	@Query("SELECT e "
			+ "FROM EventoEntity e "
			+ "WHERE e.nick = :nick AND e.start BETWEEN CURRENT_TIMESTAMP() and :fechamax")
	public List<EventoEntity> findByNickAndFechamax(@Param("nick") String nick, @Param("fechamax") String fechamax);
	//public List<Person> find(@Param("lastName") String lastName);
	
	
	@Query("SELECT e "
			+ "FROM EventoEntity e "
			+ "WHERE e.nick = :nick AND e.id_receta = :id_receta")
	public List<EventoEntity> findEventoEntityByNickAndId(@Param("nick") String nick, @Param("id_receta") int id_receta);
	
}