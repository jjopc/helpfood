package helpfood.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import helpfood.entity.RecetaEntity;
import java.util.List;
import java.util.Optional;

@Repository
public interface RecetaRepository extends JpaRepository<RecetaEntity, Integer>, JpaSpecificationExecutor<RecetaEntity>{
	public Optional<RecetaEntity> findById(int id);
	public List<RecetaEntity> findAll();
	public List<RecetaEntity> findBycultura(String id);
	public Optional<RecetaEntity> findByfecha(String fecha_receta);
	public Optional<RecetaEntity> findByTitulo(String id);
	public List<RecetaEntity> findByNick(String id);
	
	@Modifying
    @Transactional
    @Query("DELETE FROM RecetaEntity e "
			+ "WHERE e.id = :idreceta")
	void deleteReceta(@Param("idreceta") int idreceta);
    @Query("SELECT MAX(id) FROM RecetaEntity e WHERE e.nick = :nick")
    int buscaMaxIdDeNick(@Param("nick") String nick);
    
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.titulo LIKE :titulo "
    		+ "AND e.nick LIKE :creador "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.cultura = :cultura "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal "
			+ "AND e.dificultad = :dificultad")
    public List<RecetaEntity> findConMiFiltro(@Param("titulo") String titulo, 
    											@Param("creador") String creador, 
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("cultura") String cultura, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal, 
    											@Param("dificultad") String dificultad);
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.titulo LIKE :titulo "
    		+ "AND e.nick LIKE :creador "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.cultura = :cultura "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal ")
    public List<RecetaEntity> findConMiFiltro2(@Param("titulo") String titulo, 
												@Param("creador") String creador, 
												@Param("tiempoMinimo") int tiempoMinimo, 
												@Param("tiempoMaximo") int tiempoMaximo, 
												@Param("cultura") String cultura, 
												@Param("minCal") int minCal, 
												@Param("maxCal") int maxCal);
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.titulo LIKE :titulo "
    		+ "AND e.nick LIKE :creador "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal "
			+ "AND e.dificultad = :dificultad")
    public List<RecetaEntity> findConMiFiltro3(@Param("titulo") String titulo, 
												@Param("creador") String creador, 
												@Param("tiempoMinimo") int tiempoMinimo, 
												@Param("tiempoMaximo") int tiempoMaximo, 
												@Param("minCal") int minCal, 
												@Param("maxCal") int maxCal,
												@Param("dificultad") String dificultad);

    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.titulo LIKE :titulo "
    		+ "AND e.nick LIKE :creador "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal")
    public List<RecetaEntity> findConMiFiltro4(@Param("titulo") String titulo, 
    											@Param("creador") String creador, 
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal);
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.titulo LIKE :titulo "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.cultura = :cultura "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal "
			+ "AND e.dificultad = :dificultad")
    public List<RecetaEntity> findConMiFiltro5(@Param("titulo") String titulo,  
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("cultura") String cultura, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal, 
    											@Param("dificultad") String dificultad);
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.titulo LIKE :titulo "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.cultura = :cultura "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal")
    public List<RecetaEntity> findConMiFiltro6(@Param("titulo") String titulo, 
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("cultura") String cultura, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal);
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.titulo LIKE :titulo "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal "
			+ "AND e.dificultad = :dificultad")
    public List<RecetaEntity> findConMiFiltro7(@Param("titulo") String titulo, 	 
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal, 
    											@Param("dificultad") String dificultad);
    
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.titulo LIKE :titulo "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal")
    public List<RecetaEntity> findConMiFiltro8(@Param("titulo") String titulo,  
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal);
    
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.nick LIKE :creador "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.cultura = :cultura "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal "
			+ "AND e.dificultad = :dificultad")
    public List<RecetaEntity> findConMiFiltro9(@Param("creador") String creador, 
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("cultura") String cultura, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal, 
    											@Param("dificultad") String dificultad);
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.nick LIKE :creador "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.cultura = :cultura "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal")
    public List<RecetaEntity> findConMiFiltro10(@Param("creador") String creador, 
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("cultura") String cultura, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal);
    
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.nick LIKE :creador "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal "
			+ "AND e.dificultad = :dificultad")
    public List<RecetaEntity> findConMiFiltro11(@Param("creador") String creador, 
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo,  
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal, 
    											@Param("dificultad") String dificultad);
    
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.nick LIKE :creador "
			+ "AND e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal")
    public List<RecetaEntity> findConMiFiltro12(@Param("creador") String creador, 
    											@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal);
    
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.cultura = :cultura "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal "
			+ "AND e.dificultad = :dificultad")
    public List<RecetaEntity> findConMiFiltro13(@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("cultura") String cultura, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal, 
    											@Param("dificultad") String dificultad);
    
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.cultura = :cultura "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal")
    public List<RecetaEntity> findConMiFiltro14(@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("cultura") String cultura, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal);
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal "
			+ "AND e.dificultad = :dificultad")
    public List<RecetaEntity> findConMiFiltro15(@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal, 
    											@Param("dificultad") String dificultad);
    
    @Query("SELECT e FROM RecetaEntity e "
			+ "WHERE e.tiempoPreparacion >= :tiempoMinimo "
    		+ "AND e.tiempoPreparacion <= :tiempoMaximo "
			+ "AND e.caloriasTotales >= :minCal "
			+ "AND e.caloriasTotales <= :maxCal")
    public List<RecetaEntity> findConMiFiltro16(@Param("tiempoMinimo") int tiempoMinimo, 
    											@Param("tiempoMaximo") int tiempoMaximo, 
    											@Param("minCal") int minCal, 
    											@Param("maxCal") int maxCal);
    
}

/*
findConMiFiltro(titulo, creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
findConMiFiltro2(titulo, creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal);
findConMiFiltro3(titulo, creador, tiempoMinimo, tiempoMaximo, minCal, maxCal, dificultad);
findConMiFiltro4(titulo, creador, tiempoMinimo, tiempoMaximo, minCal, maxCal);
findConMiFiltro5(titulo, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
findConMiFiltro6(titulo, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal);
findConMiFiltro7(titulo, tiempoMinimo, tiempoMaximo, minCal, maxCal, dificultad);
findConMiFiltro8(titulo, tiempoMinimo, tiempoMaximo, minCal, maxCal);
findConMiFiltro9(creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
findConMiFiltro10(creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal);
findConMiFiltro11(creador, tiempoMinimo, tiempoMaximo, minCal, maxCal, dificultad);
findConMiFiltro12(creador, tiempoMinimo, tiempoMaximo, minCal, maxCal);
findConMiFiltro13(tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
findConMiFiltro14(tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal);
findConMiFiltro15(tiempoMinimo, tiempoMaximo, minCal, maxCal, dificultad);

findConMiFiltro16(tiempoMinimo, tiempoMaximo, minCal, maxCal);		
*/