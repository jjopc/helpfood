package helpfood.entity;

//import java.sql.Date;
import javax.persistence.*;

@Entity
@IdClass(CompositeKeyEventoEntity.class)
@Table(name = "vista_calendario")
public class EventoEntity {
	@Id
	@Column(name="id_receta")
	private int id_receta;
	
	@Column(name="titulo")
	private String title;
	
	//@Column(name="titulo")
	@Id
	private String nick;
	
	@Id
	@Column(name="fecha_hora")
	private String start;
	
	@Transient
	private String url; 
	//transient indica que url no viene de ninguna columna, 
	//lo formamos nosotros en la clase con lo que sí viene de otra columna y lo uso como otro parametro en el json
	
	public EventoEntity() {
		 
	};

	public int getid() {
		return this.id_receta;
	}
	
	public void setIdReceta(int id) {
		this.id_receta = id;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getStart() {
		return this.start;
	}
	
	public void setStart(String start) {
		this.start = start;
	}
	
	public String getNick() {
		return this.nick;
	}
	
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public String geturl() {
		return null;//"/receta_concreta.html?id=" + id_receta;
	}
	
	public void seturl(String url) {
		this.url = url;
	}

	public void setfechahora(String getfechahora) {
		// TODO Auto-generated method stub
		
	}
	
}