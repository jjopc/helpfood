$(document).ready(function() {
    obtenerListasUsuario();
});

function obtenerListasUsuario() {
    var username = getCookie('username');

	 $.ajax({
         url: "/listas_usuarios/" + username,
         type: "GET",
         success: function (data) {        	 
         	for (var i = 0; i < data.listaListasUsuario.length; i++) {         		
        		$("#contenedor_listas").append('<a href="lista_concreta.html?titulo=' + data.listaListasUsuario[i].titulo + '" style="apearance:none; color:black;" class=""><div class="card m-2" id="unaReceta"><img class="card-img-top" src="' + "img/ejemplo.jpg" 
        				+ '"><div class="card-body"><h4 class="card-title">' + data.listaListasUsuario[i].titulo);
         	}
         }
 	});
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
