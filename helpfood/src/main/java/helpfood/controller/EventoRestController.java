package helpfood.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import helpfood.entity.EventoEntity;
import helpfood.response.ListaEventosResponse;
import helpfood.service.EventoService;
import helpfood.specifications.SearchCriteria;

@RestController
@RequestMapping(path = "/eventos", method = RequestMethod.GET)
public class EventoRestController {
	@Autowired
	private EventoService service;
	
	@RequestMapping(path = "")
	public ResponseEntity<ListaEventosResponse> getAllEventos() {
		return ResponseEntity.ok(service.getAllEventos());
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/filtro")
    @ResponseBody
    public List<EventoEntity> search(@RequestParam(value = "search", required = false) String search) {
        List<SearchCriteria> params = new ArrayList<SearchCriteria>();
        if (search != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3)));
            }
        }
        return service.searchEvento(params);
    }
	
	@RequestMapping(path = "/{nick}")
	public ResponseEntity<ListaEventosResponse> getEventosUsuario(@PathVariable String nick) {
		
		return ResponseEntity.ok(service.getEventosUsuario(nick));
	}
	
	
	@RequestMapping(path = "/{nick}/hastafechamaxima/{fechamax}")
	public ResponseEntity<ListaEventosResponse> getEventosUsuarioFechaMax(@PathVariable String nick, @PathVariable String fechamax) {
		
		return ResponseEntity.ok(service.findByNickAndFechamax(nick, fechamax));
	}
	
	
	@RequestMapping(path = "/{nick}/{id_receta}")
	public ResponseEntity<ListaEventosResponse> getEventosUsuarioIdReceta(@PathVariable String nick, @PathVariable int id_receta) {
		
		return ResponseEntity.ok(service.findEventoEntityByNickAndId(nick, id_receta));
	}
	
	@RequestMapping(path = "/fecha_evento/{start}")
	public ResponseEntity<Optional<EventoEntity>> getFecha(@PathVariable String start){
		return ResponseEntity.ok(service.findByFecha(start));
	}
	

}