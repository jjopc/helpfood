package helpfood.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CompositeKeyRecetasListasDeRecetas implements Serializable{
    private int idReceta;
    private String tituloLista;
    private String nick;

    public int getIdReceta() {
        return idReceta;
    }

    public void setIdReceta(int idReceta) {
        this.idReceta = idReceta;
    }

    public String getTituloLista() {
        return tituloLista;
    }

    public void setTituloLista(String tituloLista) {
        this.tituloLista = tituloLista;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    
}