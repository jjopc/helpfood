package helpfood.converter;

import helpfood.entity.ComentarioEntity;
import helpfood.request.NewComentarioRequest;

public class NewComentarioRequestToComentarioEntityConverter {

	public ComentarioEntity convert(NewComentarioRequest usuarioRequest) {
		
		ComentarioEntity comentarioEntity = new ComentarioEntity();
		comentarioEntity.setNick(usuarioRequest.getNick());
		comentarioEntity.setId_receta(usuarioRequest.getId_receta());
		comentarioEntity.setFecha(usuarioRequest.getFecha());
		comentarioEntity.setNota(usuarioRequest.getNota());
		comentarioEntity.setTexto(usuarioRequest.getTexto());
		return comentarioEntity;
	}
}
