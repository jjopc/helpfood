package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.PlanificacionEntity;
import helpfood.response.PlanificacionResponse;

public class PlanificacionEntityToPlanificacionResponseConverter implements Converter<PlanificacionEntity, PlanificacionResponse> {
	
	@Override
	public PlanificacionResponse convert(PlanificacionEntity planificacion) {
		PlanificacionResponse planificacionRes = new PlanificacionResponse(); 
		planificacionRes.setnick(planificacion.getnick());
		planificacionRes.setid_receta(planificacion.getid_receta());
		planificacionRes.setfecha_hora(planificacion.getfecha_hora());
		return planificacionRes;
	}
	
}