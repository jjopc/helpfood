$(document).ready(function() {
	var nick = getCookie("username");
	getNotificaciones(nick); 
	
	$("#notificaciones").on("click", function() {
		mostrarToast();
	});
});

function getNotificaciones(nick) {	
	var fechaMax = moment().add(1, 'days').format('YYYY-MM-DD HH:mm:ss');
	$.ajax({
		url : '/eventos/'+ nick + '/hastafechamaxima/'+fechaMax,
		type : "GET",
		success : function(data) {

		if(!data.listaEventos.length){
			$("#area_notify")
							.append( 
									'<div role="alert" id="mitoast" aria-live="assertive" aria-atomic="true" data-delay="10000" class="toast mostrar_notificacion" >'
									+ '<div class="toast-header" data-delay="10000">'
									+ '<!-- Icono / Logo de la Aplicación -->'
									+ '<img src="img/favicon.ico" width="20" height="20" class="rounded mr-2" alt="HelpFood">'
									+ '<!-- Nombre de la Aplicación -->'
									+ '<strong class="mr-auto">HelpFood</strong>'
									+ '<!-- Tiempo del Evento realizado -->'
									+ '<small> '
									+ ' '	
									+ ' </small>'
									+ '<!-- Botón para Cerrar el Toast -->'
									+ '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Cerrar" onclick="cerrarToast()">'
									+ '<span aria-hidden="true">&times;</span>'
									+ '</button>'
									+ '</div>'
									+ '<div class="toast-body" data-delay="10000">'
									+ 'No tienes nada en tu agenda para las próximas 24h. <br>' 
									+ '<a style="color:black;" href="recetas.html"><b>Date una vuelta por las recetas'
									+ '</b></a> y encuentra algo que te guste para comer!!</div>' 
									+ '</div>'
									);
		}//if
		else{
			for (var i = 0; i < data.listaEventos.length; i++) {
							
				var hoy = moment(moment().format('YYYY-MM-DD HH:mm:ss'));
				var fechaReceta = moment(data.listaEventos[i].start);
				var tiempoRestante = fechaReceta.diff(hoy, 'hours') + "h!";
				
				if(!isToday(fechaReceta, hoy))
					tiempoRestante= "para mañana!";
				
					$("#area_notify")
							.append( 
									'<div role="alert" id="mitoast" aria-live="assertive" aria-atomic="true" class="toast mostrar_notificacion">'
									+ '<div class="toast-header">'
									+ '<!-- Icono / Logo de la Aplicación -->'
									+ '<img src="img/favicon.ico" width="20" height="20" class="rounded mr-2" alt="HelpFood">'
									+ '<!-- Nombre de la Aplicación -->'
									+ '<strong class="mr-auto">HelpFood</strong>'
									+ '<!-- Tiempo del Evento realizado -->'
									+ '<small>Quedan '
									+ tiempoRestante	
									+ ' </small>'
									+ '<!-- Botón para Cerrar el Toast -->'
									+ '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Cerrar" onclick="cerrarToast()">'
									+ '<span aria-hidden="true">&times;</span>'
									+ '</button>'
									+ '</div>'
									+ '<div class="toast-body">'
									+ 'Que no se te olvide preparar la receta: <a style="color:black;" href="receta_concreta.html?id='+data.listaEventos[i].idReceta+'"><b>'
									+ data.listaEventos[i].title
									+ '</b>!!' + '</a></div>' + '</div>'
									);					
				}
			}//for
		}//else
	});
}

function isToday(momentDate, hoy) {
    return momentDate.isSame(hoy, 'd');
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

// Con esta función se muestra el Toast / Notificacion
function mostrarToast() {
	var area = document.getElementById("area_notify");
	area.className = "ahoraSiEstoy";
	setTimeout(function() {		
		area.className = area.className.replace("ahoraSiEstoy", "area_notif");
	}, 5000);
	
	var toast = document.getElementsByClassName("mostrar_notificacion");
	Array.from(toast).forEach(
		    function(toast) {
		    	toast.className = "mostrar";
		    	
				setTimeout(function() {
					toast.className = toast.className.replace("mostrar", "toast mostrar_notificacion");
					area.className = area.className.replace("ahoraSiEstoy", "area_notif");
				}, 5000);
		    }
	);
}

// Con esta función se cierra el Toast / Notificacion
function cerrarToast() {
	$(document).on('click','#mitoast',function() {
 		 $(this).remove();
	 });	
}


