$(document).ready(function () {
	$("#pasosReceta").hide();

	//Esto recoge el id pasado como parametro a receta_concreta.html tal que .../receta_concreta.html?id=X
	//y asi se puede facilmente llamar y cargar cualquier receta cuyo id exista en la BD
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const idReceta = urlParams.get('id');
	
	getValoraciones(idReceta);
	getIngRecetas(idReceta);
	getRecetas(idReceta);
	mostrarReceta();
	activarItem();

	document.getElementById("fecha_evento").min = moment().format('YYYY-MM-DDThh:mm');
	document.getElementById("fecha_evento").value = moment().format('YYYY-MM-DDThh:mm');

	$("#agregar").click(function () {
		prueba(idReceta);
		$('#agregarEvento').modal('hide');
	});

	// Lógica para añadir recetas a listas de recetas
	$("#btnAgregarALista").click(function () {
		cargarListasSelect();
	});

	$("#btnAgregarRecetaALista").click(function() {
		agregarRecetaALista();
	});
});

var puntuaciones = 0;
var numPuntuaciones = 0;
var puntuacionTotal = 0;
function getValoraciones(idReceta) {
	$.ajax({
        url: "/comentarios/receta/" + idReceta,
        type: "GET",
        async: false,
        success: function (data) {
        	numPuntuaciones = data.listaComentarios.length;
        	for (var i = 0; i < numPuntuaciones; i++) {
        		puntuaciones += data.listaComentarios[i].nota;
			}
        	puntuacionTotal = (puntuaciones/numPuntuaciones).toFixed(2);
		}
	});
}

var pasos = "";
var numComensales = 1;
var cantidadIngredientes = [];
var ingredientesReceta = []; //id, cantidad
var datosIngredientes = []; //nombre, calorias, estado

function getIngRecetas(idReceta) {
	$.ajax({
        url: "/ingrecetas/" + idReceta,
        type: "GET",
        async: false,
        success: function (data) {
        	ingredientesReceta = data.listaIngRecetas;
		}
	});
}

function getIngredientes(idIngrediente) {
	$.ajax({
        url: "/ingredientes/" + idIngrediente,
        type: "GET",
        async: false,
        success: function (data) {
        	datosIngredientes.push(data);
    		$("#ingredientes").append('<option>' + data.nombre + '</option>');
		}
	});
}

function getRecetas(idReceta) {
	$.ajax({
        url: "/recetas/" + idReceta,
        type: "GET",
        success: function (data) {
    		$("#titulo").text(data.titulo);
    		$("#descripcion").text(data.descripcion);
    		$("#cultura").text(data.cultura);
    		$("#dificultad").text(data.dificultad);
    		$("#tiempoPreparacion").text(data.tiempoPreparacion + "'");
    		$("#caloriasTotales").text(data.caloriasTotales);
    		$("#popularidad").text(puntuacionTotal);
    		
    		//TODO METER RUTA DE LA IMAGEN
    		$('#foto_receta').attr("src", "../img/img_recetas/" + idReceta + ".jpg");
    		//TODO HACER PARA TODOS LOS QUE PUEDAN TENER UN VALOR NULO, O METER CASO POR DEFECTO
    		//data.utensilios!=null ? $("#divutensilios").append('<select id="utensilios"><option>' + JSON.parse(data.utensilios)[i] + '</option></select>') : $("#divutensilios").append('<em>sin especificar</em>');
    		if(data.utensilios!=null){
    			for (var i = 0; i < JSON.parse(data.utensilios).length; i++) {
        			$("#utensilios").append('<option>' + JSON.parse(data.utensilios)[i] + '</option>');
    			}
    		}
    		 
			data.nick != getCookie("username") ?  $("#creador").append('<a href="recetas.html?creador=' + data.nick + '">' + data.nick + '</a>') : $("#creador").append('<a href="mis_recetas.html">' + data.nick + '</a>'); 
    		
			for (var i = 0; i < ingredientesReceta.length; i++) {
				getIngredientes(ingredientesReceta[i].idIngrediente);
				cantidadIngredientes.push(ingredientesReceta[i].cantidad);
			}
			pasos = JSON.parse(data.pasos);
		}
	});
}

function mostrarReceta() {
	$("#empezarReceta").click(function () {
		$("#pasosReceta").show();
		$("#empezarReceta").prop("disabled", true);

		for (var i = 1; i <= Object.keys(pasos).length; i++) {
			$("#listaPasos").append('<li class="nav-item"><a id="paso' + i + '" class="nav-link paso">Paso ' + i + '</a></li>');
		}
		numComensales = parseInt($("#comensales").val());
		mostrarPasoGeneral(numComensales);
	});
}

function activarItem() {
	$(document).on('click', '.paso', function () {
		$(".paso").removeClass("active");
		$(this).addClass("active");

		var sizeId = $(this).attr('id').length;
		var childElement = parseInt($(this).attr('id')[sizeId - 1]);

		var i = 1;
		for (var key in pasos) {
			if (childElement == 0) {
				mostrarPasoGeneral();
				break;
			} else if (i == childElement) {
				var value = pasos[key];
				$("#mensaje").text(value);
				break;
			}
			i++;
		}
	});
}

function mostrarPasoGeneral() {
	$("#mensaje").text(" ");
	$("#mensaje").append(
		"<table class='table'><thead><tr><th scope='col'>Ingrediente</th><th scope='col'>Calorias</th>" +
		"<th scope='col'>Cantidad</th><th scope='col'>Unidad</th><th scope='col'>Calorias/100</th></tr></thead>" +
		"<tbody id='bodyMensaje'></tbody></table>"
	);
	
	var idsAlergenosUsuario;
	$.ajax({
		async: false,
		url : "alergiasusuarios/" + getCookie("username"),
		type : "GET",
		success : function(data) {
			idsAlergenosUsuario = data.listaAlergiasUsuarios;
		}
	});
	
	var caloriasTotales = 0;
	for (var i = 0; i < datosIngredientes.length; i++) {
		var ingredienteEsAlergeno = false;
		var unidad = "";
		if (datosIngredientes[i].estado == 0) {
			unidad = "gr";
		} else if (datosIngredientes[i].estado == 1) {
			unidad = "mL";
		}
		var caloriasPorAlimento = (datosIngredientes[i].calorias*cantidadIngredientes[i]*numComensales)/100;
		caloriasTotales += caloriasPorAlimento;

		//-------------------------------

		
		var nombreAlergeno;
		var observacionAlergeno;
		$.ajax({
			async: false,
			url : "alergenosingredientes/" + datosIngredientes[i].id,
			type : "GET",
			success : function(data) {
				if(data != null){ //si es null, es porque un ingrediente no está asociado con ningun alergeno y por tanto el get no ha encontrado nada
					//console.log(data.idAlergeno);
					for (var i = 0; i < idsAlergenosUsuario.length; i++){

						if(idsAlergenosUsuario[i].idAlergeno == data.idAlergeno){
							ingredienteEsAlergeno = true;
							
							//obtengo el nombre de ese alergeno
							$.ajax({
								async: false,
								url : "alergenos/" + data.idAlergeno,
								type : "GET",
								success : function(dataAlergeno) {
									
									nombreAlergeno = dataAlergeno.nombre;
									observacionAlergeno = dataAlergeno.observaciones;
								}
							});
						} 
					}
				} 
			}
		});


		//-------------------------------


		if(ingredienteEsAlergeno){
			$("#bodyMensaje").append(
				'<tr><td style="color: red;">' 
				+ datosIngredientes[i].nombre 
				+ ' <div class="midropdown"><span><em><u>(' + nombreAlergeno + ')</u></em></span><div class="midropdown-content"><p>' 
				+ observacionAlergeno 
				+ '<p></div></div>' 
				+ '</td><td>' + 
				caloriasPorAlimento + "</td><td>" +
				cantidadIngredientes[i]*numComensales + "</td><td>" + 
				unidad + "</td><td>" + datosIngredientes[i].calorias + "</td></tr>"
			);
		}
		else{
			$("#bodyMensaje").append(
			'<tr><td>' + datosIngredientes[i].nombre + "</td><td>" + 
			caloriasPorAlimento + "</td><td>" +
        	cantidadIngredientes[i]*numComensales + "</td><td>" + 
        	unidad + "</td><td>" + datosIngredientes[i].calorias + "</td></tr>"
		);
		}

		
	}//for

	$("#bodyMensaje").append("<tr><th>Calorías totales</th><td>" + caloriasTotales.toFixed(2) + "</td></tr>");
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}


function agregarEvento(id_receta) {
	var fecha_hora = $("#fecha_evento").val();
	var nick = getCookie("username");

	$.post(
		"/planificaciones",
		{
			nick: nick,
			id_receta: id_receta,
			fecha_hora: fecha_hora
		},
		function (data, status) {
			alert("¡Receta agregada a tu agenda!.\nData: " + data + "\nStatus: " + status);
			window.location.assign("/receta_concreta.html?id=" + idReceta);
			comprobacion = data;
		}
	);
}

function prueba(id_receta) {
	var fecha_hora = $("#fecha_evento").val();
	var nick = getCookie("username");
	//var Success = false;
	$.ajax({
		type: "POST",
		url: "/planificaciones",
		dataType: "text",
		async: false,
		data: JSON.stringify({ nick: nick, id_receta: id_receta, fecha_hora: fecha_hora }),
		contentType: "application/json; charset=utf-8",
		success: function (data, status) {
			//Success = true;
			alert("¡Receta agregada a tu agenda!");//.\nData: " + data + "\nStatus: " + status );
			//window.location.assign("/receta_concreta.html?id="+idReceta);
		},
		error: function (textStatus, errorThrown) {
			//Success = false;
		}
	});
}

function cargarListasSelect() {
	var username = getCookie('username');

	$.ajax({
		url: "/listas_usuarios/" + username,
		type: "GET",
		success: function (data) {
			for (var i = 0; i < data.listaListasUsuario.length; i++) {
				$("#select-listas-recetas").append(
					'<option value="'
					+ data.listaListasUsuario[i].titulo
					+ '">'
					+ data.listaListasUsuario[i].titulo
					+ '</option>'
				);
			}
		},
		error: function(){
			alert("Algo ha ido mal cargando la lista del select de listas de recetas.");
		}
	});
}

function agregarRecetaALista() {
	var username = getCookie('username');
	var titulo = $("#select-listas-recetas").val();
	var id = window.location.search.split('=')[1];

	$.ajax({
		url: "listas_recetas",
		type: "POST",
		data: JSON.stringify({
			nick: username,
			titulo_lista: titulo,
			id_receta: id
		}),
		contentType: "application/json; charset=utf-8",
		success: function (data) {
			alert("Receta añadida correctamente.");
			window.location.assign('recetas.html');
		},
		error: function (data, errorThrown) {
			alert("Ha ocudrrido un problema añadiendo la receta a la lista.")
		}
	});
}
