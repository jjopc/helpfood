package helpfood.service;

import java.util.Optional;

import helpfood.entity.PlanificacionEntity;
import helpfood.request.NewPlanificacionRequest;
import helpfood.response.ListaPlanificacionesResponse;
import helpfood.response.PlanificacionResponse;

public interface PlanificacionService {
	//public ListaEventosResponse getEventosUsuario(String nick);
	public ListaPlanificacionesResponse getAllPlanificaciones();
	//List<EventoEntity> searchEvento(List<SearchCriteria> params); //filtros 
	//public EventoResponse createEvento(NewEventoRequest eventoRequest);
	public Optional<PlanificacionEntity> findPlanificacion(String nick, int id_receta, String fecha_hora);
	//public ListaEventosResponse findByNickAndFechamax(String nick, String fechamax);
	//public ListaEventosResponse findEventoEntityByNickAndId(String nick, int id_receta);
	public PlanificacionResponse createPlanificacion(NewPlanificacionRequest planificacionRequest);
	public Optional<PlanificacionResponse> borrarPlanificacion(String nick, int id_receta, String fecha_hora);
	public Optional<PlanificacionResponse> borrarTodasPlanificaciones(String nick);
	public Optional<PlanificacionResponse> borrarPlanificacionesReceta(int idreceta);
}