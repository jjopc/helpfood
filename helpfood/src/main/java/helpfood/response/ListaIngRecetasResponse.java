package helpfood.response;

import java.util.List;

public class ListaIngRecetasResponse {
	private List<IngRecetaResponse> listaIngRecetas;

	public ListaIngRecetasResponse(List<IngRecetaResponse> listaIngRecetas) {
		this.listaIngRecetas = listaIngRecetas;
	}

	public ListaIngRecetasResponse() {
	}

	public List<IngRecetaResponse> getListaIngRecetas() {
		return listaIngRecetas;
	}

	public void setListaIngRecetas(List<IngRecetaResponse> listaIngRecetas) {
		this.listaIngRecetas = listaIngRecetas;
	}
}