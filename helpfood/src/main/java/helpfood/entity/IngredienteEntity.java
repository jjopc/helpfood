package helpfood.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ingredientes")

public class IngredienteEntity {
	@Id
	@Column(name="id_ingrediente")
	private int id;
	@Column(name="nombre")
	private String nombre;
	@Column(name="calorias")
	private int calorias;
	@Column(name="tipo")
	private String estado; //Solido o liquido
	//@Column(name="alergenos")
	//private ArrayList<AlergenoEntity> alergenos;
	
	public IngredienteEntity() {}
	
	public IngredienteEntity(int id, String nombre, int calorias, String estado/*, ArrayList<AlergenoEntity> alergenos*/) {
		this.id = id;
		this.nombre = nombre;
		this.calorias = calorias;
		this.estado = estado;
		//this.alergenos = alergenos;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}	
	
	public int getCalorias() {
		return this.calorias;
	}
	
	public void setCalorias(int calorias) {
		this.calorias = calorias;
	}
	
	public String getEstado() {
		return this.estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	/*
	public ArrayList<AlergenoEntity> getAlergenos(){
		return this.alergenos;
	}
	
	public void setAlergenos(ArrayList<AlergenoEntity> alergenos) {
		this.alergenos = alergenos;
	}
	*/
	
}