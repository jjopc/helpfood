package helpfood.entity;

import java.util.ArrayList;

public class ListaComentariosEntity {

	private ArrayList<ComentarioEntity> listaComentarios;
	
	public ListaComentariosEntity(ArrayList<ComentarioEntity> listaComentarios) {
		this.listaComentarios = listaComentarios;
	}

	public ArrayList<ComentarioEntity> getListaComentarios() {
		return listaComentarios;
	}

	public void setListaComentarios(ArrayList<ComentarioEntity> listaComentarios) {
		this.listaComentarios = listaComentarios;
	}
	
}
