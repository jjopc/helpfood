package helpfood.converter;

import helpfood.entity.EventoEntity;
import helpfood.request.NewEventoRequest;

public class NewEventoRequestToEventoEntityConverter {

	public EventoEntity convert(NewEventoRequest eventoRequest) {
		EventoEntity eventoEntity = new EventoEntity();
		eventoEntity.setNick(eventoRequest.getNick());
		eventoEntity.setIdReceta(eventoRequest.getidreceta());
		eventoEntity.setfechahora(eventoRequest.getfechahora());
		return eventoEntity;
	}
}
