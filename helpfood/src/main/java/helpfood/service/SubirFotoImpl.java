package helpfood.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class SubirFotoImpl {
	
	private static int maxId=0;
	
    public static void setMaxId(int maxId) {
		SubirFotoImpl.maxId = maxId;
	}

	public void saveFile(MultipartFile file) throws IOException {
    	
        if(!file.isEmpty()){        	
            byte[] bytes = file.getBytes();
            
            File dest = new File(System.getProperty("user.dir") + "/src/main/resources/static/img/img_recetas/" + maxId +".jpg");
            Files.write(dest.toPath(),bytes);
        }
    }
}
