package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.AlergiasUsuariosEntityToAlergiasUsuariosResponseConverter;
import helpfood.entity.AlergiasUsuariosEntity;
import helpfood.repository.AlergiasUsuariosRepository;
import helpfood.response.AlergiasUsuariosResponse;
import helpfood.response.ListaAlergiasUsuariosResponse;

@Service
public class AlergiasUsuariosServiceImpl implements AlergiasUsuariosService {
	
	private AlergiasUsuariosEntityToAlergiasUsuariosResponseConverter entityToResponseConverter = new AlergiasUsuariosEntityToAlergiasUsuariosResponseConverter();
	//private NewIngRecetaRequestToIngRecetaEntityConverter requestToEntityConverter = new NewIngRecetaRequestToIngRecetaEntityConverter();
	
	@Autowired
	private AlergiasUsuariosRepository alergiasUsuariosRepository;

	@Override
	public ListaAlergiasUsuariosResponse getAllAlergiasUsuarios() {
		ListaAlergiasUsuariosResponse response = new ListaAlergiasUsuariosResponse();
		List<AlergiasUsuariosEntity> allAlergiasUsuarios = alergiasUsuariosRepository.findAll();
		List<AlergiasUsuariosResponse> allResponses = allAlergiasUsuarios.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaAlergiasUsuarios(allResponses);
		return response;
	}
	
	@Override
	public ListaAlergiasUsuariosResponse getAlergiasUsuarioConcreto(String nick) {
		ListaAlergiasUsuariosResponse response = new ListaAlergiasUsuariosResponse();
		List<AlergiasUsuariosEntity> allAlergiasUsuarios = alergiasUsuariosRepository.findByNick(nick);
		List<AlergiasUsuariosResponse> allResponses = allAlergiasUsuarios.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaAlergiasUsuarios(allResponses);
		return response;
	}
	
	public Optional<AlergiasUsuariosResponse> borrarAlergias(String nick){
		Optional<AlergiasUsuariosResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		alergiasUsuariosRepository.deleteAlergias(nick);
		return response;
	}
	
	/*
	@Override
	public ListaIngRecetasResponse findByIdRecetaAndIdIngrediente(int idreceta, int idingrediente) {
		ListaIngRecetasResponse response = new ListaIngRecetasResponse();
		List<IngRecetaEntity> ingReceta = ingRecetaRepository.findByIdRecetaAndIdIngrediente(idreceta, idingrediente);
		List<IngRecetaResponse> allResponses = ingReceta.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaIngRecetas(allResponses);
		return response;
	}
	
	@Override
	public ListaIngRecetasResponse findByIdReceta(int idreceta) {
		ListaIngRecetasResponse response = new ListaIngRecetasResponse();
		List<IngRecetaEntity> ingReceta = ingRecetaRepository.findByIdReceta(idreceta);
		List<IngRecetaResponse> allResponses = ingReceta.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaIngRecetas(allResponses);
		return response;
	}
		
	
	//@Override
	public Optional<IngRecetaResponse> borrarIngReceta(int idreceta){
		Optional<IngRecetaResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		ingRecetaRepository.deleteIngReceta(idreceta);
		return response;
	}
	*/
}