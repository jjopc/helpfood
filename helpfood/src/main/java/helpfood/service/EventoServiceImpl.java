package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.EventoEntityToEventoResponseConverter;
import helpfood.converter.NewEventoRequestToEventoEntityConverter;
import helpfood.entity.EventoEntity;
import helpfood.repository.EventoRepository;
import helpfood.response.EventoResponse;
import helpfood.response.ListaEventosResponse;
import helpfood.specifications.SearchCriteria;
import helpfood.specifications.SearchQueryCriteriaConsumer;

@Service
public class EventoServiceImpl implements EventoService {
	private EventoEntityToEventoResponseConverter entityToResponseConverter = new EventoEntityToEventoResponseConverter();
	private NewEventoRequestToEventoEntityConverter requestToEntityConverter = new NewEventoRequestToEventoEntityConverter(); 
	@Autowired
	private EventoRepository eventoRepository;
	
	@PersistenceContext
    private EntityManager entityManager; 
	
	@Override
	public ListaEventosResponse getEventosUsuario(String nick){
		ListaEventosResponse response = new ListaEventosResponse();
		List<EventoEntity> allEventos = eventoRepository.findByNick(nick);
		List<EventoResponse> allResponses = allEventos.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaEventos(allResponses);
		return response;
	}
	

	@Override
	public ListaEventosResponse getAllEventos() {
		ListaEventosResponse response = new ListaEventosResponse();
		List<EventoEntity> allEventos = eventoRepository.findAll();
		List<EventoResponse> allResponses = allEventos.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaEventos(allResponses);
		return response;
	}
	
	@Override
    public List<EventoEntity> searchEvento(final List<SearchCriteria> params) { //para los filtros
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<EventoEntity> query = builder.createQuery(EventoEntity.class);
        final Root r = query.from(EventoEntity.class);

        Predicate predicate = builder.conjunction();
        SearchQueryCriteriaConsumer searchConsumer = new SearchQueryCriteriaConsumer(predicate, builder, r);
        params.stream().forEach(searchConsumer);
        predicate = searchConsumer.getPredicate();
        query.where(predicate);

        return entityManager.createQuery(query).getResultList();
    }
	
	/*
	@Override
	public EventoResponse createEvento(NewEventoRequest eventoRequest) {
		EventoResponse response;
		
		Optional<EventoEntity> oldEntity = eventoRepository.findByNick(usuarioRequest.getNick());
		if (oldEntity.isPresent()) {
			response = entityToResponseConverter.convert(oldEntity.get());
		} else {
			EventoEntity newEntity = requestToEntityConverter.convert(eventoRequest);
			newEntity = eventoRepository.save(newEntity);
			response = entityToResponseConverter.convert(newEntity);
		//}
		return response;
	}
	*/
	
	@Override
	public Optional<EventoEntity> findByFecha(String start) {
		return eventoRepository.findBystart(start);
	}
	
	@Override
	public ListaEventosResponse findByNickAndFechamax(String nick, String fechamax){
		ListaEventosResponse response = new ListaEventosResponse();
		List<EventoEntity> allEventos = eventoRepository.findByNickAndFechamax(nick, fechamax);
		List<EventoResponse> allResponses = allEventos.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaEventos(allResponses);
		return response;
	}
	
	@Override
	public ListaEventosResponse findEventoEntityByNickAndId(String nick, int id_receta){
		ListaEventosResponse response = new ListaEventosResponse();
		List<EventoEntity> allEventos = eventoRepository.findEventoEntityByNickAndId(nick, id_receta);
		List<EventoResponse> allResponses = allEventos.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaEventos(allResponses);
		return response;
	}
	

}