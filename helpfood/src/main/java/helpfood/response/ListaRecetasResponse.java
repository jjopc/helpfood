package helpfood.response;

import java.util.List;

public class ListaRecetasResponse {
	private List<RecetaResponse> listaRecetas;

	public ListaRecetasResponse(List<RecetaResponse> listaRecetas) {
		this.listaRecetas = listaRecetas;
	}

	public ListaRecetasResponse() {
	}

	public List<RecetaResponse> getListaRecetas() {
		return listaRecetas;
	}

	public void setListaRecetas(List<RecetaResponse> listaRecetas) {
		this.listaRecetas = listaRecetas;
	}
}