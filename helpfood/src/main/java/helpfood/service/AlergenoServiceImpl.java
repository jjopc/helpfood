package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import helpfood.converter.AlergenoEntityToAlergenoResponseConverter;
import helpfood.entity.AlergenoEntity;
import helpfood.repository.AlergenoRepository;
import helpfood.response.ListaAlergenosResponse;
import helpfood.response.AlergenoResponse;

@Service
public class AlergenoServiceImpl implements AlergenoService {
	private AlergenoEntityToAlergenoResponseConverter entityToResponseConverter = new AlergenoEntityToAlergenoResponseConverter();
	@Autowired
	private AlergenoRepository alergenoRepository;
	
	@Override
	public Optional<AlergenoEntity> getAlergeno(int idalergeno) {
		return alergenoRepository.findById(idalergeno);
	}

	@Override
	public ListaAlergenosResponse getAllAlergenos() {
		ListaAlergenosResponse response = new ListaAlergenosResponse();
		List<AlergenoEntity> allUsuarios = alergenoRepository.findAll();
		List<AlergenoResponse> allResponses = allUsuarios.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaAlergenos(allResponses);
		return response;
	}
}