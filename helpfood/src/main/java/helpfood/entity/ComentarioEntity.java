package helpfood.entity;

import javax.persistence.*;

@Entity
@IdClass(CompositeKey_comentarios.class)
@Table(name = "valoraciones")
public class ComentarioEntity {	
	@Id
	@Column(name="id_receta")
	private int idreceta;
	@Id
	@Column(name="nick")
	private String nickUsuario;
	@Column(name="comentario")
	private String texto;
	@Column
	private int nota; //Nota de un comentario
	@Column(name="fecha_hora")
	private String fecha;
	
	public ComentarioEntity() {}
	
	public ComentarioEntity(int id, String texto, int nota, String nick, String fecha) {
		this.idreceta = id;
		this.texto = texto;
		this.nickUsuario = nick;
		this.nota = nota;
		this.fecha = fecha;
	}
	
	public int getId_receta() {
		return idreceta;
	}

	public void setId_receta(int id_receta) {
		this.idreceta = id_receta;
	}

	public String getNick() {
		return nickUsuario;
	}

	public void setNick(String nick) {
		this.nickUsuario = nick;
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
