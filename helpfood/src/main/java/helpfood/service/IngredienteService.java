package helpfood.service;

import java.util.Optional;

import helpfood.response.ListaIngredientesResponse;
import helpfood.entity.IngredienteEntity;
import helpfood.response.IngredienteResponse;

public interface IngredienteService {
	public Optional<IngredienteResponse> getIngrediente(String nombre);
	public ListaIngredientesResponse getAllIngredientes();
	public Optional<IngredienteEntity> findById(int id_ingrediente);
}