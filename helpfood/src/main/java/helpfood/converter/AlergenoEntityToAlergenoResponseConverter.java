package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.AlergenoEntity;
import helpfood.response.AlergenoResponse;

public class AlergenoEntityToAlergenoResponseConverter implements Converter<AlergenoEntity, AlergenoResponse> {
	@Override
	public AlergenoResponse convert(AlergenoEntity alergeno) {
		AlergenoResponse alergenoRes = new AlergenoResponse();
		alergenoRes.setId(alergeno.getId());
		alergenoRes.setNombre(alergeno.getNombre());
		alergenoRes.setObservaciones(alergeno.getObservaciones());
		return alergenoRes;
	}
}