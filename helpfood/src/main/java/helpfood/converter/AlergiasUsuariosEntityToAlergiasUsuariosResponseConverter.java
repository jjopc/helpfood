package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.AlergiasUsuariosEntity;
import helpfood.response.AlergiasUsuariosResponse;

public class AlergiasUsuariosEntityToAlergiasUsuariosResponseConverter implements Converter<AlergiasUsuariosEntity, AlergiasUsuariosResponse> {
	@Override
	public AlergiasUsuariosResponse convert(AlergiasUsuariosEntity alergiasUsuarios) {
		AlergiasUsuariosResponse alergiasUsuariosRes = new AlergiasUsuariosResponse();
		alergiasUsuariosRes.setNick(alergiasUsuarios.getNick());
		alergiasUsuariosRes.setIdAlergeno(alergiasUsuarios.getIdAlergeno());
		return alergiasUsuariosRes;
	}
}