package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.ListasDeRecetasDeUnUsuarioEntity;
import helpfood.response.ListasDeRecetasDeUnUsuarioResponse;


public class ListasDeRecetasDeUnUsuarioEntityToResponseConverter implements Converter<ListasDeRecetasDeUnUsuarioEntity, ListasDeRecetasDeUnUsuarioResponse> {

	@Override
	public ListasDeRecetasDeUnUsuarioResponse convert(ListasDeRecetasDeUnUsuarioEntity source) {
		ListasDeRecetasDeUnUsuarioResponse res = new ListasDeRecetasDeUnUsuarioResponse();
		res.setNick(source.getNick());
		res.setTitulo(source.getTitulo());
		return res;
	}
}
