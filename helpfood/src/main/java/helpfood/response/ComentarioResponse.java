package helpfood.response;

public class ComentarioResponse {

	private int id_receta;
	private String nick;
	private String texto;
	private int nota;
	private String fecha;
	
	public ComentarioResponse(int id_receta, String nick, String texto, int nota, String fecha){
		this.id_receta = id_receta;
		this.nick = nick;
		this.texto = texto;
		this.nota = nota;
		this.fecha = fecha;
	}
	
	public ComentarioResponse() {}

	public int getId_receta() {
		return id_receta;
	}

	public void setId_receta(int id_receta) {
		this.id_receta = id_receta;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
