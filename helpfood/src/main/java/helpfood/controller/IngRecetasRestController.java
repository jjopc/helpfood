package helpfood.controller;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import helpfood.request.NewIngRecetaRequest;
import helpfood.response.IngRecetaResponse;
import helpfood.response.ListaIngRecetasResponse;
import helpfood.service.IngRecetaService;

@RestController
@RequestMapping(path = "/ingrecetas")
public class IngRecetasRestController {
	@Autowired
	private IngRecetaService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ListaIngRecetasResponse> getAllIngRecetas() {
		return ResponseEntity.ok(service.getAllIngRecetas());
	}
	
	@RequestMapping(path = "/{id_receta}", method = RequestMethod.GET)
	public ResponseEntity<ListaIngRecetasResponse> getIngReceta(@PathVariable int id_receta) {
		return ResponseEntity.ok(service.findByIdReceta(id_receta));
	}
	
	@RequestMapping(path = "/{id_receta}/{id_ingrediente}", method = RequestMethod.GET)
	public ResponseEntity<ListaIngRecetasResponse> getIngRecetas(@PathVariable int id_receta, @PathVariable int id_ingrediente) {
		return ResponseEntity.ok(service.findByIdRecetaAndIdIngrediente(id_receta, id_ingrediente));
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<IngRecetaResponse> createNewIngReceta(@Valid @RequestBody NewIngRecetaRequest ingRecetaRequest) {
		IngRecetaResponse response = service.createIngReceta(ingRecetaRequest);
		// ahora crea la nueva uri de este usuario /usuarios/nick_usuario
		URI uriCreated = UriComponentsBuilder.fromPath("/ingreceta").buildAndExpand(ingRecetaRequest.getId()).toUri();		
		return ResponseEntity.created(uriCreated).body(response);
	}
	
	@RequestMapping(path = "/delete/{idreceta}", method = RequestMethod.DELETE)
	public Optional<IngRecetaResponse> borrarIngReceta(@PathVariable int idreceta) {
		Optional<IngRecetaResponse> response = service.borrarIngReceta(idreceta);
		return response;
	}
	
}