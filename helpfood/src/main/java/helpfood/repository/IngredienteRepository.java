package helpfood.repository;

import org.springframework.stereotype.Repository;

import helpfood.entity.IngredienteEntity;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface IngredienteRepository extends CrudRepository<IngredienteEntity, String>{
	public Optional<IngredienteEntity> findByNombre(String nombre);
	public List<IngredienteEntity> findAll();
	public Optional<IngredienteEntity> findById(int id_ingrediente);
}