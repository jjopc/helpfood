$(document).ready(function () {
	getAlergenos();
	$("#mostrarClave").click(function () {
		mostrarClave();
	});

	$("#password2").blur(function () {
		comprobarClave();
	});

	$("#crear").click(function () {
		creaUsuario();
	});
});

function getAlergenos() {
	$.ajax({
		async: false,
		url: "alergenos/",
		type: "GET",
		success: function (data) {
			for (var i = 0; i < data.listaAlergenos.length; i++) {
				$("#alergias").append('<option value=' + data.listaAlergenos[i].id + '>' + data.listaAlergenos[i].nombre + '</option>');
			}
		}
	});
}

function creaUsuario() {
	var alergias = $("#alergias").attr("selected", true).val();
	var alergiasInt = [];
	
	for (var i = 0; i < alergias.length; i++) {
		alergiasInt.push(parseInt(alergias[i]));
	}

	$.ajax({
		url: "/usuarios",
		type: "POST",
		dataType: "text",
		async: false,
		data: JSON.stringify({
			nick: $("#nick").val(),
			password: $("#password").val(),
			email: $("#email").val(),
			nombre: $("#nombre").val(),
			apellidos: $("#apellidos").val(),
			fechaNacimiento: $("#fecha_nacimiento").val(),
			codPais: $("#cod_pais").val(),
			rol: $("#rol").val(),
			alergias: JSON.stringify(alergiasInt)
		}),
		contentType: "application/json; charset=utf-8",
		success: function (data, status) {
			alert("Usuario creado con éxito.");
			crearFavoritas();
		},
		error: function () {
			alert("Algo ha ido mal en la creación del usuario");
		}
	});
}

function crearFavoritas() {
	$.ajax({
		url: "/listas_usuarios",
		type: "POST",
		data:
		{
			nick: $("#nick").val(),
			titulo: 'favoritas'
		},
		success: function (data, status) {
			window.location.assign("/login.html");
		},
		error: function (data, errorThrown) {
			alert("Ha ocudrrido un problema al crear la lista favoritas.");
		}
	}
	);
}

function comprobarClave() {
	clave1 = document.f1.clave1.value;
	clave2 = document.f1.clave2.value;

	if (clave1 != clave2) {
		$('#verify').append(
			'<div class="input-group my-3"><div class="alert alert-danger">   ' +
			'<button type="button" class="close" data-dismiss="alert">&times;</button> ' +
			'<strong>¡Claves diferentes!</strong> </div></div>'
		);
	}
}

function mostrarClave() {
	var x = document.getElementById("password");
	var y = document.getElementById("password2");
	if (x.type === "password") {
		x.type = "text";
		y.type = "text";
	} else {
		x.type = "password";
		y.type = "password";
	}
}