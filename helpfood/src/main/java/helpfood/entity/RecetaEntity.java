package helpfood.entity;

import javax.persistence.*;

@Entity
@Table(name = "recetas")
public class RecetaEntity {
	@Id
	@Column(name="id_receta")
	private int id;
	
	@Column
	private String titulo;
	@Column
	private String cultura;
	@Column
	private String dificultad;
	@Column
	private String utensilios;
	@Column(name="t_preparacion")
	private int tiempoPreparacion;
	@Column
	private String pasos;
	@Column(name="fecha_receta")
	private String fecha;
	@Column
	private String nick;
	@Column(name="calorias")
	private int caloriasTotales;
	@Column
	private String descripcion;
	/* @Column
	private int popularidad; */
	
	public RecetaEntity() {};
	
	public RecetaEntity(String titulo) {
		this.titulo = titulo;
	}

	public RecetaEntity(int id, String titulo, String cultura, String dificultad, String utensilios,
			int tiempoPreparacion, String pasos, String fecha, String nick, int caloriasTotales, String descripcion) {
		this.id = id;
		this.titulo = titulo;
		this.cultura = cultura;
		this.dificultad = dificultad;
		this.utensilios = utensilios;
		this.tiempoPreparacion = tiempoPreparacion;
		this.pasos = pasos;
		this.fecha = fecha;
		this.nick = nick;
		this.caloriasTotales = caloriasTotales;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCultura() {
		return cultura;
	}

	public void setCultura(String cultura) {
		this.cultura = cultura;
	}

	public String getDificultad() {
		return dificultad;
	}

	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}

	public String getUtensilios() {
		return utensilios;
	}

	public void setUtensilios(String utensilios) {
		this.utensilios = utensilios;
	}

	public int getTiempoPreparacion() {
		return tiempoPreparacion;
	}

	public void setTiempoPreparacion(int tiempoPreparacion) {
		this.tiempoPreparacion = tiempoPreparacion;
	}

	public String getPasos() {
		return pasos;
	}

	public void setPasos(String pasos) {
		this.pasos = pasos;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getCaloriasTotales() {
		return caloriasTotales;
	}

	public void setCaloriasTotales(int caloriasTotales) {
		this.caloriasTotales = caloriasTotales;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}