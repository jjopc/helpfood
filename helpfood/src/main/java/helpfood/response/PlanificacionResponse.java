package helpfood.response;

public class PlanificacionResponse {
	
	private String nick;
	private int id_receta;
	private String fecha_hora;
	
	public PlanificacionResponse() {
		
	}

	public int getid_receta() {
		return this.id_receta; 
	}
	
	public void setid_receta(int id) {
		this.id_receta = id;
	}
	
	public String getfecha_hora() {
		return this.fecha_hora;
	}
	
	public void setfecha_hora(String fecha_hora) {
		this.fecha_hora = fecha_hora;
	}
	
	public String getnick() {
		return this.nick;
	}
	
	public void setnick(String nick) {
		this.nick = nick;
	}
	

}