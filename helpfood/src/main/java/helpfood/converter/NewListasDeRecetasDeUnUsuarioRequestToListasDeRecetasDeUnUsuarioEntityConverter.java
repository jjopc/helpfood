package helpfood.converter;

import helpfood.entity.ListasDeRecetasDeUnUsuarioEntity;
import helpfood.request.NewListasDeRecetasDeUnUsuarioRequest;

public class NewListasDeRecetasDeUnUsuarioRequestToListasDeRecetasDeUnUsuarioEntityConverter {

	public ListasDeRecetasDeUnUsuarioEntity convert(NewListasDeRecetasDeUnUsuarioRequest recetaRequest) {
		ListasDeRecetasDeUnUsuarioEntity listaEntity = new ListasDeRecetasDeUnUsuarioEntity();
		listaEntity.setNick(recetaRequest.getNick());
		listaEntity.setTitulo(recetaRequest.getTitulo());
		return listaEntity;
	}
}
