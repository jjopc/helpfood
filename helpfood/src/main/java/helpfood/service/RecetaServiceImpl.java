package helpfood.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.NewRecetaRequestToRecetaEntityConverter;
import helpfood.converter.RecetaEntityToRecetaResponseConverter;
import helpfood.entity.IngRecetaEntity;
import helpfood.entity.RecetaEntity;
import helpfood.repository.IngRecetaRepository;
import helpfood.repository.RecetaRepository;
import helpfood.request.NewRecetaRequest;
import helpfood.response.ListaRecetasResponse;
import helpfood.response.RecetaResponse;
import helpfood.specifications.SearchCriteria;
import helpfood.specifications.SearchQueryCriteriaConsumer;

@Service
public class RecetaServiceImpl implements RecetaService {
	private RecetaEntityToRecetaResponseConverter entityToResponseConverter = new RecetaEntityToRecetaResponseConverter();
	private NewRecetaRequestToRecetaEntityConverter requestToEntityConverter = new NewRecetaRequestToRecetaEntityConverter();
		
	@Autowired
	private RecetaRepository recetaRepository;
	@Autowired
	private IngRecetaRepository ingRecetaRepository;
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public ListaRecetasResponse getAllRecetas() {
		ListaRecetasResponse response = new ListaRecetasResponse();
		List<RecetaEntity> allRecetas = recetaRepository.findAll();
		List<RecetaResponse> allResponses = allRecetas.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaRecetas(allResponses);
		return response;
	}
	
	@Override
	public Optional<RecetaEntity> findById(int id){
		return recetaRepository.findById(id);
	}
	
	@Override
	public ListaRecetasResponse findByCultura(String id) {
		ListaRecetasResponse response = new ListaRecetasResponse();
		List<RecetaEntity> RecetasCultura = recetaRepository.findBycultura(id);
		List<RecetaResponse> allResponses = RecetasCultura.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaRecetas(allResponses);
		return response;
	}
	
	@Override
	public Optional<RecetaEntity> findByFecha(String fecha) {
		return recetaRepository.findByfecha(fecha);
	}
	
	@Override
	public Optional<RecetaEntity> findByTitulo(String id) {
		 return recetaRepository.findByTitulo(id);
	}
	
	public ListaRecetasResponse findByNick(String id) {
		ListaRecetasResponse response = new ListaRecetasResponse();
		List<RecetaEntity> recetasAutor = recetaRepository.findByNick(id);
		List<RecetaResponse> allResponses = recetasAutor.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaRecetas(allResponses);
		return response;
	}
	
	@Override
    public List<RecetaEntity> searchReceta(final List<SearchCriteria> params) { //para los filtros
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<RecetaEntity> query = builder.createQuery(RecetaEntity.class);
        final Root r = query.from(RecetaEntity.class);

        Predicate predicate = builder.conjunction();
        SearchQueryCriteriaConsumer searchConsumer = new SearchQueryCriteriaConsumer(predicate, builder, r);
        params.stream().forEach(searchConsumer);
        predicate = searchConsumer.getPredicate();
        query.where(predicate);

        return entityManager.createQuery(query).getResultList();
    }

	@Override
	public RecetaResponse createReceta(NewRecetaRequest recetaRequest) {
		RecetaResponse response;
		Optional<RecetaEntity> oldEntity = recetaRepository.findById(recetaRequest.getId());
		if (oldEntity.isPresent()) {
			response = entityToResponseConverter.convert(oldEntity.get());
		} else {
			RecetaEntity newEntity = requestToEntityConverter.convert(recetaRequest);
			newEntity = recetaRepository.save(newEntity);
						
			int maxId = recetaRepository.buscaMaxIdDeNick(recetaRequest.getNick());
			SubirFotoImpl.setMaxId(maxId);
			//GUARDAR LA IMG POR DEFECTO
			if(recetaRequest.getNombreArchivo()=="" || recetaRequest.getNombreArchivo()==null) {//						
				try {
					File sourc = new File(System.getProperty("user.dir") + "/src/main/resources/static/img/ejemplo.jpg");
					File desti = new File(System.getProperty("user.dir") + "/src/main/resources/static/img/img_recetas/"+ maxId+ ".jpg");
										
					Files.copy(sourc.toPath(), desti.toPath());
				}catch(IOException e){
//					
				}
//				
			}
			
			
			
			JSONObject objectListaIngredientes = new JSONObject(recetaRequest.getIngredientes());
			JSONArray listaIngredientes = objectListaIngredientes.getJSONArray("listaIngredientes");
			for (int i = 0; i < listaIngredientes.length(); i++) {
				JSONObject ingredientes = listaIngredientes.getJSONObject(i);
				int idIngrediente = ingredientes.getInt("idIngrediente");
				int cantidad = ingredientes.getInt("cantidad");
				IngRecetaEntity newIngRecetaEntity = new IngRecetaEntity(maxId, idIngrediente, cantidad);
				ingRecetaRepository.save(newIngRecetaEntity);
			}
			
			/*String rutaImg = recetaRequest.getRutaImg();
			ImgRecetaEntity imgReceta = new ImgRecetaEntity(maxId, rutaImg);
			imagenRecetaRepository.save(imgReceta);*/
			
			response = entityToResponseConverter.convert(newEntity);
		}
		return response;
	}
	
	/*
	@Override
	public Optional<RecetaResponse> borrarRecetas(String nick){
		Optional<RecetaResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		recetaRepository.deleteTodasRecetas(nick);
		return response;
	}
	*/
	
	@Override
	public Optional<RecetaResponse> borrarReceta(int idreceta){
		Optional<RecetaResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		recetaRepository.deleteReceta(idreceta);
		return response;
	}
	
	public ListaRecetasResponse getRecetasBuscadas(String titulo, String creador, int tiempoMinimo, int tiempoMaximo, String cultura, 
			int minCal, int maxCal, String dificultad) {
		
		//Si uno o todos los parametros llegan desde la petición get (de recetas.js) con un valor null 
		//(o sea, que no están en la URL formada en la petición en el .js), adoptarán aquí su defaultValue especificado
		//Tambien necesito preparar titulo y creador para que tengan al comienzo y final los % para la query con el like (spring requiere que eso se lo demos ya preparado)
		System.out.println(titulo);
		System.out.println(creador);
		if(tiempoMinimo == -1) tiempoMinimo = 0;
		if(tiempoMaximo == -1) tiempoMaximo = Integer.MAX_VALUE;
		if(minCal == -1) minCal = 0;
		if(maxCal == -1) maxCal = Integer.MAX_VALUE;
		if(!titulo.equals("")) titulo = '%' + titulo + '%';
		if(!creador.equals("")) creador = '%' + creador+ '%';
		
		ListaRecetasResponse response = new ListaRecetasResponse();
		List<RecetaEntity> allRecetas = null;//List<RecetaEntity>();
		if(!titulo.equals("") && !creador.equals("") && !cultura.equals("") && !dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro(titulo, creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
		if(!titulo.equals("") && !creador.equals("") && !cultura.equals("") && dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro2(titulo, creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal);
		if(!titulo.equals("") && !creador.equals("") && cultura.equals("") && !dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro3(titulo, creador, tiempoMinimo, tiempoMaximo, minCal, maxCal, dificultad);
		if(!titulo.equals("") && !creador.equals("") && cultura.equals("") && dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro4(titulo, creador, tiempoMinimo, tiempoMaximo, minCal, maxCal);
		if(!titulo.equals("") && creador.equals("") && !cultura.equals("") && !dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro5(titulo, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
		if(!titulo.equals("") && creador.equals("") && !cultura.equals("") && dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro6(titulo, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal);
		if(!titulo.equals("") && creador.equals("") && cultura.equals("") && !dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro7(titulo, tiempoMinimo, tiempoMaximo, minCal, maxCal, dificultad);
		if(!titulo.equals("") && creador.equals("") && cultura.equals("") && dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro8(titulo, tiempoMinimo, tiempoMaximo, minCal, maxCal);
		if(titulo.equals("") && !creador.equals("") && !cultura.equals("") && !dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro9(creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
		if(titulo.equals("") && !creador.equals("") && !cultura.equals("") && dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro10(creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal);
		if(titulo.equals("") && !creador.equals("") && cultura.equals("") && !dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro11(creador, tiempoMinimo, tiempoMaximo, minCal, maxCal, dificultad);
		if(titulo.equals("") && !creador.equals("") && cultura.equals("") && dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro12(creador, tiempoMinimo, tiempoMaximo, minCal, maxCal);
		if(titulo.equals("") && creador.equals("") && !cultura.equals("") && !dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro13(tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
		if(titulo.equals("") && creador.equals("") && !cultura.equals("") && dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro14(tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal);
		if(titulo.equals("") && creador.equals("") && cultura.equals("") && !dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro15(tiempoMinimo, tiempoMaximo, minCal, maxCal, dificultad);
		if(titulo.equals("") && creador.equals("") && cultura.equals("") && dificultad.equals("")) allRecetas = recetaRepository.findConMiFiltro16(tiempoMinimo, tiempoMaximo, minCal, maxCal);
		
		//List<RecetaEntity> allRecetas = recetaRepository.findConMiFiltro(cultura);
		//List<RecetaEntity> allRecetas = recetaRepository.findConMiFiltro2(titulo, creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad);
		List<RecetaResponse> allResponses = allRecetas.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaRecetas(allResponses);
		return response;
	}
	
}