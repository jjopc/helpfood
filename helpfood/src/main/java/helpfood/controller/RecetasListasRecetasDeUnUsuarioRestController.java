package helpfood.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import helpfood.request.NewRecetasListasDeRecetasDeUnUsuarioRequest;
import helpfood.response.RecetasListasDeRecetasDeUnUsuarioResponse;
import helpfood.service.RecetasListasDeRecetasDeUnUsuarioService;

@RestController
@RequestMapping(path = "/listas_recetas")
public class RecetasListasRecetasDeUnUsuarioRestController {
    
    @Autowired
    private RecetasListasDeRecetasDeUnUsuarioService service;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<List<RecetasListasDeRecetasDeUnUsuarioResponse>> findAllRecetasListasDeRecetasDeUnUsuario() {
        return ResponseEntity.ok(service.findAllRecetasListasDeRecetasDeUnUsuario());
    }

    @RequestMapping(path = "/{nick}/{titulo}", method = RequestMethod.GET)
    public ResponseEntity<List<RecetasListasDeRecetasDeUnUsuarioResponse>> findByTituloListaAndNick(@PathVariable String titulo, @PathVariable String nick) {
        return ResponseEntity.ok(service.findByTituloListaAndNick(titulo, nick));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<RecetasListasDeRecetasDeUnUsuarioResponse>> findByIdReceta(@PathVariable int id) {
        return ResponseEntity.ok(service.findByIdReceta(id));
    }

    @RequestMapping(path = "/{nick}/{titulo}/{id}", method = RequestMethod.GET)
    public ResponseEntity<RecetasListasDeRecetasDeUnUsuarioResponse> findByTituloListaAndNickAndIdReceta(@PathVariable String titulo, @PathVariable String nick,@PathVariable int id) {
        return ResponseEntity.ok(service.findByTituloListaAndNickAndIdReceta(titulo, nick, id));
    }


    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<RecetasListasDeRecetasDeUnUsuarioResponse> createRecetasListasDeUnUsuario(@Valid @RequestBody NewRecetasListasDeRecetasDeUnUsuarioRequest request) {
        RecetasListasDeRecetasDeUnUsuarioResponse response = service.createRecetasListasDeUnUsuario(request);
        URI uriCreated = UriComponentsBuilder.fromPath("/listas_recetas").buildAndExpand(request.getTitulo_lista()).toUri(); 
        return ResponseEntity.created(uriCreated).body(response);
    }

    @RequestMapping(path = "/delete/{nick}/{titulo}", method = RequestMethod.DELETE)
    public void deleteTodaLaListaDeRecetasListasDeRecetasDeUnUsuario(@PathVariable String titulo, @PathVariable String nick) {
        service.deleteTodaLaListaDeRecetasListasDeRecetasDeUnUsuario(titulo, nick);
    }

    @RequestMapping(path = "/delete/{nick}/{titulo}/{id}", method = RequestMethod.DELETE)
    public void deleteUnaRecetaDeUnaListaDeRecetasListasDeRecetasDeUnUsuario(@PathVariable String titulo, @PathVariable String nick, @PathVariable int id) {
        service.deleteUnaRecetaDeUnaListaDeRecetasListasDeRecetasDeUnUsuario(titulo, nick, id);
    }
    
    @RequestMapping(path = "/delete/{nick}", method = RequestMethod.DELETE)
    public void deleteTodaLaListaDeRecetasListasDeRecetas(@PathVariable String nick) {
        service.deleteTodaLaListaDeRecetasListasDeRecetas(nick);
    }
    
    @RequestMapping(path = "/delete/apariciones/{id}", method = RequestMethod.DELETE)
    public void deleteListaApareceReceta(@PathVariable int id) {
        service.deleteListaApareceReceta(id);
    }
}