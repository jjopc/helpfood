# Tutorial para unirse al proyecto en gitlab:

## 1. Instalar git en el ordenador:
```
https://git-scm.com/downloads
```

## 2. Crear una cuenta en gitlab (en caso de no tenerla):
```
https://git-scm.com/downloads
```

## 3. Configuramos git:
	a) Consola de git en Windows o terminal de Linux/macOS:
	git config --global user.name "Vuestro_nombre_en_gitlab"
	git config --global user.email "vuestro_email_en_gitlab@example.com"
	
## 4. Crear un par de claves SSH para poder conectar con el repositorio y firmar los commits, en este caso, dependiendo del SO anfitri�n podemos distinguir (web oficial de tutorial):
	a) Windows: Abrimos la consola de git y escribimos:
		ssh-keygen -t ed25519 -C "email@example.com"
	b) Linux/macOS: Abrimos una terminal y escribimos:
		ssh-keygen -t ed25519 -C "email@example.com"
	
## 5. A�adir la clave P�BLICA a nuestro perfil de gitlab:
	a) PRIMERO: Copiamos la clave p�blica al portapapeles
		i. Windows: en la consola de git
			cat ~/.ssh/id_ed25519.pub | clip
		ii. Linux:
			xclip -sel clip < ~/.ssh/id_ed25519.pub
		iii. macOS:
		pbcopy < ~/.ssh/id_ed25519.pub
	b) SEGUNDO: En la p�gina de gitlab:
		i. Clicamos la imagen de nuestro avatar (en la esquina superior derecha) y vamos a
			configuraci�n.
		ii. En el panel de navegaci�n a la izquierda vamos a �Claves SSH�, aqu� pegamos el
			contenido del portapapeles en el recuadro y le damos a a�adir clave.
	
## 6. Ahora ya podemos clonar el repositorio a nuestro ordenador y trabajar con normalidad. Para clonar el repositorio usamos:
```
git@gitlab.com:jjopc/helpfood.git
```


