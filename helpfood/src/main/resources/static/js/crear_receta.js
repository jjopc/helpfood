$(document).ready(function(){
	$("#sumaIngrediente").on("click", function() {
		sumaIngrediente();
	});
  	 
	$("#sumaPaso").on("click", function() {
		sumaPaso();
	});
  	 
  	$(document).on('click','#btn_close_ingre',function() {
  		$(this).parent().parent().parent().remove();
	});
  	 
  	$(document).on('click','#btn_close_paso',function() {
  		$(this).parent().parent().parent().remove();
  	});
  	
  	var nombreArchivo = "";
  	 
	$("#crearReceta").click(function() {
		crearReceta(nombreArchivo);
	});	
});

var caloriasTotales = 0;
var cantidadesIngredientes = [];
function getIngredientes(idIngrediente, i) {
	$.ajax({
        url: "/ingredientes/" + idIngrediente,
        type: "GET",
        async: false,
        success: function (data) {
        	caloriasTotales += data.calorias*cantidadesIngredientes[i]/100;
		}
	});
}

function sumaIngrediente() {
	$.ajax({
        url: "/ingredientes",
        type: "GET",
        success: function (data) {
        	$("#ingrediente").append(
    			'<tr class="addedIngrediente" id=""><td>'+
    				'<div class="row my-4 ">'+
    				'<div class="col-sm  mx-3 mt-3">'+
    					'<div class="d-flex justify-content-center form_container" style="text-align:left;">'+
    						'<form>		'+
    							'<span class="titulo_menor">Ingrediente</span>'+
    							'<select class="selectIngredientes form-control selectRecetaConcreta"></select>'
    						+'</form>'+
    					'</div>	' +
    				'</div>'+
    				
    				'<div class="col-sm  mx-2 mt-3">'+
    					'<div class="d-flex justify-content-center form_container" style="text-align:left;">'+
    						'<form>	'	+
    							'<span class="titulo_menor">Cantidad</span>'+
    							'<input type="number" min="1" name="" class="form-control inputs_receta cantidades" value="1" placeholder="Ingrediente">'
    						+'</form>'+
    					'</div>' +
    				'</div>' +
    				
    				'<a class="mr-5 mt-3" id="btn_close_ingre" type="button"  style= "color:#fff; transform: scale(2);">'+
    					'<i class="fas fa-trash " ></i>	'+
    				'</a>'+
    				
    				'</div>'+
    				'</div>'+
    			"</td></tr>"
    		);
        	for (var i = 0; i < data.listaIngredientes.length; i++) {
        		$(".selectIngredientes").append('<option value=' + data.listaIngredientes[i].id + '>' + data.listaIngredientes[i].nombre + '</option>');
			}
		}
	});
}

var i = 0;
function sumaPaso() {       	
	$("#paso").append(
		'<tr class="addedPaso"><td style= "color:#fff">'+ 
		'<span class="tituloPaso">Paso '+ i+
		'<a class="ml-5" id="btn_close_paso" type="button"  style= "color:#fff;">'+
		'<i class="fas fa-trash " ></i>	'+										
		'</a>'+
		'</span>'+
		'<textarea class="form-control detallePasos" rows="3" placeholder=" Detalla el paso:"></textarea>'+	
		"</td></tr>"
	);	
	i++;
}

function crearReceta(nombreArchivo) {
	var tpHoras = parseInt($("#tiempoPreparacion").val().substr(0,2));
	var tpMinutos = parseInt($("#tiempoPreparacion").val().substr(3,5));
	var tiempoPreparacion =  tpHoras*60 + tpMinutos;
	var idsIngredientes = [];
	
	$("#ingrediente").find(".selectIngredientes").each(function(){
		idsIngredientes.push($(this).prop("value"));
	});
	$("#ingrediente").find(".cantidades").each(function(){
		cantidadesIngredientes.push($(this).val());
	});
	
	var listaIngredientes = {};
	var arrayIngredientes = [];
	
	for (var i = 0; i < idsIngredientes.length; i++) {
		getIngredientes(idsIngredientes[i], i);
		var objectIngrediente = {};
		objectIngrediente["idIngrediente"] = idsIngredientes[i];
		objectIngrediente["cantidad"] = cantidadesIngredientes[i];
		arrayIngredientes.push(objectIngrediente);
	}
	listaIngredientes["listaIngredientes"] = arrayIngredientes;
	
	var utensilios = [];
	var pasos = [];
		
	$("#opcionesUtensilios li").each(function(){
		utensilios.push($(this).text());
	});
	
	$(".addedPaso .detallePasos").each(function(){
		pasos.push($(this).val());
	});	
	
	$.post(
		"/recetas", {
			titulo: $("#titulo").val(),
			cultura: $("#cultura").val(),
			dificultad: $("#dificultad").val(),
			utensilios: JSON.stringify(utensilios),
			tiempoPreparacion: tiempoPreparacion,
			pasos: JSON.stringify(pasos),
			fecha: moment().format('YYYY-MM-DD HH:mm:ss'),
			nick: getCookie("username"),
			descripcion: $("#descripcion").val(),
			nombreArchivo: nombreArchivo,//JSON.stringify(urlImg)
			caloriasTotales: parseInt(caloriasTotales),
			//Campos del ingReceta (idIngrediente, cantidad)
			ingredientes: JSON.stringify(listaIngredientes)
		},
		function (data, status) {
			alert("Receta creada con éxito");
			window.location.assign("/subirFoto.html");
	     }
	);	
}
