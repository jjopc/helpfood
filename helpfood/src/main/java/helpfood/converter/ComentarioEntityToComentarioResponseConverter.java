package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.ComentarioEntity;
import helpfood.response.ComentarioResponse;

public class ComentarioEntityToComentarioResponseConverter implements Converter<ComentarioEntity, ComentarioResponse> {

	public ComentarioResponse convert(ComentarioEntity source) {

		ComentarioResponse res = new ComentarioResponse();
		
		res.setFecha(source.getFecha());
		res.setId_receta(source.getId_receta());
		res.setNick(source.getNick());
		res.setNota(source.getNota());
		res.setTexto(source.getTexto());
		
		return res;
	}


}
