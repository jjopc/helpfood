package helpfood.entity;

import java.io.Serializable;

//Nos permite tener para IngRecetaEntity que las 2 columnas especificadas aqui sean ambas clave primaria
@SuppressWarnings("serial")
public class CompositeKeyEventoEntity implements Serializable {
    private int id_receta;
    private String nick;
    private String start;

    public int getId_receta() {
        return id_receta;
    }

    public void setId_receta(int id_receta) {
        this.id_receta = id_receta;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }
    
}