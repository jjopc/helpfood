package helpfood.converter;

import org.springframework.core.convert.converter.Converter;

import helpfood.entity.RecetasListasDeRecetasDeUnUsuarioEntity;
import helpfood.response.RecetasListasDeRecetasDeUnUsuarioResponse;

public class RecetasListasDerecetasDeUnUsuarioEntityToResponseConverter implements Converter<RecetasListasDeRecetasDeUnUsuarioEntity, RecetasListasDeRecetasDeUnUsuarioResponse> {

    @Override
    public RecetasListasDeRecetasDeUnUsuarioResponse convert(RecetasListasDeRecetasDeUnUsuarioEntity source) {
        RecetasListasDeRecetasDeUnUsuarioResponse response = new RecetasListasDeRecetasDeUnUsuarioResponse();
        response.setIdReceta(source.getIdReceta());
        response.setTituloLista(source.getTituloLista());
        response.setNick(source.getNick());
        return response;
    }
    
}