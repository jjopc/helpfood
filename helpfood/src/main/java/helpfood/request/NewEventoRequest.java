package helpfood.request;

import javax.validation.constraints.NotNull;

public class NewEventoRequest {
	
	@NotNull
	private String nick;
	@NotNull
	private int id_receta;
	@NotNull
	private String fecha_hora;
	

	
	public NewEventoRequest() {
		super();
	}
	
	public NewEventoRequest(@NotNull String nick, @NotNull int id_receta, @NotNull String fecha_hora) {
		super();
		this.nick = nick;
		this.id_receta = id_receta;
		this.fecha_hora = fecha_hora;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getidreceta() {
		return id_receta;
	}

	public void setidreceta(int id_receta) {
		this.id_receta = id_receta;
	}

	public String getfechahora() {
		return fecha_hora;
	}

	public void setfechahora(String fecha_hora) {
		this.fecha_hora = fecha_hora;
	}

}
