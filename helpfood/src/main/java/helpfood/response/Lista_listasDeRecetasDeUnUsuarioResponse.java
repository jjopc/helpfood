package helpfood.response;

import java.util.List;

public class Lista_listasDeRecetasDeUnUsuarioResponse {
	
	private List<ListasDeRecetasDeUnUsuarioResponse> listaListasUsuario;

	public Lista_listasDeRecetasDeUnUsuarioResponse(List<ListasDeRecetasDeUnUsuarioResponse> listaListasUsuario) {
		this.listaListasUsuario = listaListasUsuario;
	}

	public Lista_listasDeRecetasDeUnUsuarioResponse() {}
	
	public List<ListasDeRecetasDeUnUsuarioResponse> getListaListasUsuario() {
		return listaListasUsuario;
	}

	public void setListaListasUsuario(List<ListasDeRecetasDeUnUsuarioResponse> listaListasUsuario) {
		this.listaListasUsuario = listaListasUsuario;
	}
}
