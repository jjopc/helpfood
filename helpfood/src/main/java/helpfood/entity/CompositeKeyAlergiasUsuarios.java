package helpfood.entity;

import java.io.Serializable;

//Nos permite tener para IngRecetaEntity que las 2 columnas especificadas aqui sean ambas clave primaria
@SuppressWarnings("serial")
public class CompositeKeyAlergiasUsuarios implements Serializable {
   
    private int idalergeno;
    private String nick;

    public int getIdalergeno() {
        return idalergeno;
    }

    public void setIdalergeno(int idalergeno) {
        this.idalergeno = idalergeno;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    
}