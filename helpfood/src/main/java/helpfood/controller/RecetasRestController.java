package helpfood.controller;


import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import helpfood.entity.RecetaEntity;
import helpfood.request.NewRecetaRequest;
import helpfood.response.ListaRecetasResponse;
import helpfood.response.RecetaResponse;
import helpfood.service.RecetaService;
import helpfood.specifications.SearchCriteria;

@RestController
@RequestMapping(path = "/recetas")
public class RecetasRestController {
	@Autowired
	private RecetaService service;
	
	//@RequestMapping(path = "/recetas", method = RequestMethod.GET)
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ListaRecetasResponse> getAllRecetas() {
		return ResponseEntity.ok(service.getAllRecetas());
	}
	
	@RequestMapping(path = "/{id_receta}", method = RequestMethod.GET)
	public ResponseEntity<Optional<RecetaEntity>> getReceta(@PathVariable int id_receta){
		return ResponseEntity.ok(service.findById(id_receta));
	}
	@RequestMapping(path = "/cultura/{cultura}", method = RequestMethod.GET)
	public ResponseEntity<ListaRecetasResponse> getCultura(@PathVariable String cultura){
		return ResponseEntity.ok(service.findByCultura(cultura));
	}
	@RequestMapping(path = "/titulo/{titulo}", method = RequestMethod.GET)
	public ResponseEntity<Optional<RecetaEntity>> getTitulo(@PathVariable String titulo){
		return ResponseEntity.ok(service.findByTitulo(titulo));
	}
	@RequestMapping(path = "/nick/{nick}", method = RequestMethod.GET)
	public ResponseEntity<ListaRecetasResponse> getNick(@PathVariable String nick){
		return ResponseEntity.ok(service.findByNick(nick));
	}
	@RequestMapping(path = "/fecha_receta/{fecha_receta}", method = RequestMethod.GET)
	public ResponseEntity<Optional<RecetaEntity>> getFecha(@PathVariable String fecha_receta){
		return ResponseEntity.ok(service.findByFecha(fecha_receta));
	}
	@RequestMapping(value = "/busqueda", method = RequestMethod.GET)
	public ResponseEntity<ListaRecetasResponse> getRecetasBuscadas(@RequestParam(value="titulo", required=false, defaultValue = "") String titulo, 
																	@RequestParam(value="creador", required=false, defaultValue = "") String creador, 
																	@RequestParam(value="tiempoMinimo", required=false, defaultValue = "-1") int tiempoMinimo, 
																	@RequestParam(value="tiempoMaximo", required=false, defaultValue = "-1") int tiempoMaximo,
																	@RequestParam(value="cultura", required=false, defaultValue = "") String cultura,
																	@RequestParam(value="minCal", required=false, defaultValue = "-1") int minCal,
																	@RequestParam(value="maxCal", required=false, defaultValue = "-1") int maxCal,
																	@RequestParam(value="dificultad", required=false, defaultValue = "") String dificultad){
		
		
		return ResponseEntity.ok(service.getRecetasBuscadas(titulo, creador, tiempoMinimo, tiempoMaximo, cultura, minCal, maxCal, dificultad));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/filtro")
    @ResponseBody
    public List<RecetaEntity> search(@RequestParam(value = "search", required = false) String search) {
        List<SearchCriteria> params = new ArrayList<SearchCriteria>();
        if (search != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3)));
            }
        }
        return service.searchReceta(params);
    }
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<RecetaResponse> createNewReceta(@Valid NewRecetaRequest recetaRequest) {
		RecetaResponse response = service.createReceta(recetaRequest);
		// ahora crea la nueva uri de este usuario /usuarios/nick_usuario
		URI uriCreated = UriComponentsBuilder.fromPath("/recetas").buildAndExpand(recetaRequest.getNick()).toUri();		
		return ResponseEntity.created(uriCreated).body(response);
	}	
	/*
	@RequestMapping(path = "/delete/{nick}/all", method = RequestMethod.DELETE)
	public Optional<RecetaResponse> borrarRecetas(@PathVariable String nick) {
		Optional<RecetaResponse> response = service.borrarRecetas(nick);
		return response;
	}
	*/
	
	@RequestMapping(path = "/delete/{idreceta}", method = RequestMethod.DELETE)
	public Optional<RecetaResponse> borrarRecetas(@PathVariable int idreceta) {
		Optional<RecetaResponse> response = service.borrarReceta(idreceta);
		return response;
	}
}