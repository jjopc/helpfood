package helpfood.repository;

import org.springframework.stereotype.Repository;

import helpfood.entity.AlergenoEntity;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface AlergenoRepository extends CrudRepository<AlergenoEntity, String>{
	public Optional<AlergenoEntity> findById(int idalergeno);
	public List<AlergenoEntity> findAll();
}