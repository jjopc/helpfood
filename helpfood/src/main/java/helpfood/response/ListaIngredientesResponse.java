package helpfood.response;

import java.util.List;

public class ListaIngredientesResponse {
	private List<IngredienteResponse> listaIngredientes;

	public ListaIngredientesResponse(List<IngredienteResponse> listaIngredientes) {
		this.listaIngredientes = listaIngredientes;
	}

	public ListaIngredientesResponse() {
	}

	public List<IngredienteResponse> getListaIngredientes() {
		return listaIngredientes;
	}

	public void setListaIngredientes(List<IngredienteResponse> listaIngredientes) {
		this.listaIngredientes = listaIngredientes;
	}
}