package helpfood.response;

import java.util.List;

public class ListaPlanificacionesResponse {
	private List<PlanificacionResponse> listaPlanificaciones;

	public ListaPlanificacionesResponse(List<PlanificacionResponse> listaPlanificaciones) {
		this.listaPlanificaciones = listaPlanificaciones;
	}

	public ListaPlanificacionesResponse() {
	}

	public List<PlanificacionResponse> getListaPlanificaciones() {
		return listaPlanificaciones;
	}

	public void setListaPlanificaciones(List<PlanificacionResponse> listaPlanificaciones) {
		this.listaPlanificaciones = listaPlanificaciones; 
	}
}