package helpfood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import helpfood.converter.NewPlanificacionRequestToPlanificacionEntityConverter;
import helpfood.converter.PlanificacionEntityToPlanificacionResponseConverter;
import helpfood.entity.PlanificacionEntity;
import helpfood.repository.PlanificacionRepository;
import helpfood.request.NewPlanificacionRequest;
import helpfood.response.ListaPlanificacionesResponse;
import helpfood.response.PlanificacionResponse;

@Service
public class PlanificacionServiceImpl implements PlanificacionService {
	//private PlanificacionEntityToPlanificacionResponseConverter entityToResponseConverter = new PlanificacionEntityToPlanificacionResponseConverter();
	//private NewPlanificacionRequestToPlanificacionEntityConverter requestToEntityConverter = new NewPlanificacionRequestToPlanificacionEntityConverter(); 
	
	@Autowired
	private PlanificacionRepository planificacionRepository;
	
	@PersistenceContext
    private EntityManager entityManager; 
	
	private PlanificacionEntityToPlanificacionResponseConverter entityToResponseConverter = new PlanificacionEntityToPlanificacionResponseConverter();
	private NewPlanificacionRequestToPlanificacionEntityConverter requestToEntityConverter = new NewPlanificacionRequestToPlanificacionEntityConverter();
	
	/*
	@Override
	public ListaEventosResponse getEventosUsuario(String nick){
		ListaEventosResponse response = new ListaEventosResponse();
		List<EventoEntity> allEventos = eventoRepository.findByNick(nick);
		List<EventoResponse> allResponses = allEventos.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaEventos(allResponses);
		return response;
	}
	*/
	

	@Override
	public ListaPlanificacionesResponse getAllPlanificaciones() {
		ListaPlanificacionesResponse response = new ListaPlanificacionesResponse();
		List<PlanificacionEntity> allPlanificaciones = planificacionRepository.findAll();
		List<PlanificacionResponse> allResponses = allPlanificaciones.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaPlanificaciones(allResponses);
		return response;
	}
	
	/*
	@Override
    public List<EventoEntity> searchEvento(final List<SearchCriteria> params) { //para los filtros
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<EventoEntity> query = builder.createQuery(EventoEntity.class);
        final Root r = query.from(EventoEntity.class);

        Predicate predicate = builder.conjunction();
        SearchQueryCriteriaConsumer searchConsumer = new SearchQueryCriteriaConsumer(predicate, builder, r);
        params.stream().forEach(searchConsumer);
        predicate = searchConsumer.getPredicate();
        query.where(predicate);

        return entityManager.createQuery(query).getResultList();
    }
    */
	
	/*
	@Override
	public EventoResponse createEvento(NewEventoRequest eventoRequest) {
		EventoResponse response;
		
		Optional<EventoEntity> oldEntity = eventoRepository.findByNick(usuarioRequest.getNick());
		if (oldEntity.isPresent()) {
			response = entityToResponseConverter.convert(oldEntity.get());
		} else {
			EventoEntity newEntity = requestToEntityConverter.convert(eventoRequest);
			newEntity = eventoRepository.save(newEntity);
			response = entityToResponseConverter.convert(newEntity);
		//}
		return response;
	}
	*/
	
	/*
	@Override
	public Optional<EventoEntity> findByFecha(String start) {
		return eventoRepository.findBystart(start);
	}
	
	@Override
	public ListaEventosResponse findByNickAndFechamax(String nick, String fechamax){
		ListaEventosResponse response = new ListaEventosResponse();
		List<EventoEntity> allEventos = eventoRepository.findByNickAndFechamax(nick, fechamax);
		List<EventoResponse> allResponses = allEventos.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaEventos(allResponses);
		return response;
	}
	
	@Override
	public ListaEventosResponse findEventoEntityByNickAndId(String nick, int id_receta){
		ListaEventosResponse response = new ListaEventosResponse();
		List<EventoEntity> allEventos = eventoRepository.findEventoEntityByNickAndId(nick, id_receta);
		List<EventoResponse> allResponses = allEventos.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setListaEventos(allResponses);
		return response;
	}
	*/
	
	@Override
	public Optional<PlanificacionEntity> findPlanificacion(String nick, int id_receta, String fecha_hora){
		return planificacionRepository.findPlanificacion(nick, id_receta, fecha_hora);
	}

	@Override
	public PlanificacionResponse createPlanificacion(NewPlanificacionRequest planificacionRequest) {
		PlanificacionResponse response;
		Optional<PlanificacionEntity> oldEntity = planificacionRepository.findPlanificacion(planificacionRequest.getnick(), planificacionRequest.getid_receta(), planificacionRequest.getfecha_hora());
		if (oldEntity.isPresent()) {
			response = entityToResponseConverter.convert(oldEntity.get());
		} else {
			PlanificacionEntity newEntity = requestToEntityConverter.convert(planificacionRequest);
			newEntity = planificacionRepository.save(newEntity);
			response = entityToResponseConverter.convert(newEntity);
		}
		return response;
	}
	
	@Override
	public Optional<PlanificacionResponse> borrarPlanificacion(String nick, int id_receta, String fecha_hora) {
		Optional<PlanificacionResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		planificacionRepository.deletePlanificacion(nick, id_receta, fecha_hora);
		return response;
	}
	
	@Override
	public Optional<PlanificacionResponse> borrarTodasPlanificaciones(String nick){
		Optional<PlanificacionResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		planificacionRepository.deleteTodasPlanificaciones(nick);
		return response;
	}
	
	@Override
	public Optional<PlanificacionResponse> borrarPlanificacionesReceta(int idreceta){
		Optional<PlanificacionResponse> response = null;
		//Optional<UsuarioEntity> oldEntity = usuarioRepository.findByNick(nick);
		planificacionRepository.deletePlanificacionesReceta(idreceta);
		return response;
	}
}