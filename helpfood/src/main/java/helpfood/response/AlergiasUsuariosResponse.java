package helpfood.response;

public class AlergiasUsuariosResponse {
	
	private int id_alergeno;
	private String nick;
	
	public AlergiasUsuariosResponse() {}
	

	public int getIdAlergeno() {
		return this.id_alergeno;
	}

	public void setIdAlergeno(int id) {
		this.id_alergeno = id;
	}
	
	public String getNick() {
		return this.nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
	
}